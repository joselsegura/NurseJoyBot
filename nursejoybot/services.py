#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import os
import re
import logging
import emoji
import random
import time
import pickle
import urllib.request
from threading import Thread

import telegram
from telegram.ext.dispatcher import run_async

from nursejoybot.config import get_config
from nursejoybot.supportmethods import (
    extract_update_info,
    ensure_escaped,
    delete_message,
    is_admin
)
from nursejoybot.storagemethods import (
    are_banned,
    rm_news_provider,
    set_news_provider,
    get_news_provider,
    get_verified_providers,
    rm_all_news_subscription,
    is_news_provider,
    set_news_subscription,
    rm_news_subscription,
    get_news_subscribed,
    is_news_subscribed,
    get_news_consumers
)
from nursejoybot.model import (
    Group,
    User,
    UserGroup,
    Team,
    Validation,
    ValidationType,
    ValidationStep,
    News,
    NewsSubs,
    ValidationRequiered
)
#OLD UP/NEW DOWN
@run_async
def init_news(bot, update, args=None):
    logging.debug("%s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)

    if chat_type != "channel":
        return

    if hasattr(message.chat, 'username') and message.chat.username is not None:
        alias = message.chat.username
    elif args is not None and len(args)!=0:
        alias = args[0]
    else:
        output = (
            "❌ He reconocido este canal como un canal privado y no me has"
            " especificado un nombre para el canal."
        )
        bot.sendMessage(
            chat_id=chat_id,
            text=output
        )
        return

    if not is_news_provider(chat_id):
        set_news_provider(chat_id, alias)

    output = (
        "📰 Bienvenido al sistema de noticias de @NurseJoyBot.\nYa tan solo "
        "queda el último paso. Ejecuta `/add_news {}` en los grupos que quie"
        "ras recibir las noticias de este canal.".format(chat_id)
    )
    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )

@run_async
def stop_news(bot, update):
    logging.debug("%s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)

    if chat_type != "channel":
        return

    output = "❌ No he reconocido este canal como proveedor de noticias."

    if is_news_provider(chat_id):
        rm_news_provider(chat_id)
        output = "✅ ¡Canal eliminado correctamente!"

    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )

@run_async
def add_news(bot, update, args=None):
    logging.debug("%s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id, chat_id):
        return

    if args is None or len(args)!=1:
        logging.debug("Fuck")
        return

    output = "❌ No he reconocido este canal como proveedor de noticias."

    if is_news_provider(args[0]) and not is_news_subscribed(chat_id, args[0]):
        set_news_subscription(chat_id, args[0])
        output = "✅ ¡Suscripción realizada correctamente!"

    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )

@run_async
def rm_news(bot, update, args=None):
    logging.debug("%s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id, chat_id):
        return

    if args is None or len(args)!=1:
        logging.debug("Fuck")
        return

    output = "❌ No he reconocido este canal como proveedor de noticias."

    if is_news_provider(args[0]) and is_news_subscribed(chat_id, args[0]):
        rm_news_subscription(chat_id, args[0])
        output = "✅ ¡Suscripción eliminada correctamente!"

    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )

@run_async
def list_news(bot, update):
    logging.debug("%s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id, chat_id):
        return

    active_news = get_news_subscribed(chat_id)
    verified_out = ""
    def_out = ""

    if active_news is None:
        output = "❌ No hay suscrpiciones activas en este grupo."

    else:
        for k in active_news:
            provider = get_news_provider(k.news_id)
            if provider.active:
                verified_out = "🔰 @{}\n".format(provider.alias) + verified_out
            else:
                def_out = "📰 {} - `{}`\n".format(provider.alias, provider.id) + def_out

        output = "Listado de canales activos:\n" + verified_out + def_out

    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN

    )

@run_async
def send_news(bot, update):
    logging.debug("%s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    if chat_type != "channel":
        return

    if not is_news_provider(chat_id):
        logging.debug("Not provider")
        return

    time.sleep(60)

    groups = get_news_consumers(chat_id)
    if groups is None:
        logging.debug("Without consumers %s", groups)
        return

    exceptions_users = []

    for k in groups:
        time.sleep(0.2)
        try:
            bot.forwardMessage(
                chat_id=k.user_id,
                from_chat_id=chat_id, 
                message_id=message.message_id
            )
        except Exception:
            exceptions_users.append(k.user_id)
            logging.info('sendingMessageTo:FAILURE->: %s', k.user_id)

    for k in exceptions_users:
        rm_all_news_subscription(k.user_id)
