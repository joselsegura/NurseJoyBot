#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

from enum import Enum

import requests


class GorochuError(Exception):
    pass


class AuthorizedStatus(Enum):
    DENIED = 'denied'
    ACEPTED = 'accepted'
    WAITING = 'waiting'
    UNKNOWN = 'unknown'


class GorochuAPI:
    def __init__(self, endpoint, auth_key):
        self.endpoint = endpoint
        self.__headers = {
            'X-GOROCHU-API': auth_key
        }

    def is_registered(self, user_id):
        resp = requests.get(
            '{}/isRegistered/{}'.format(self.endpoint, user_id),
            headers=self.__headers
        )

        if resp.status_code != 200:
            raise GorochuError('Error {}'.format(resp))

        return resp.json().get('registered', False)

    def is_authorized(self, user_id):
        resp = requests.get(
            '{}/isAuthorized/{}'.format(self.endpoint, user_id),
            headers=self.__headers
        )

        if resp.status_code != 200:
            raise GorochuError('Error {}'.format(resp))

        return AuthorizedStatus(resp.json().get('authorized', 'unknown'))

    def request_authorization(self, user_id):
        resp = requests.get(
            '{}/requestAuthorization/{}'.format(self.endpoint, user_id),
            headers=self.__headers
        )

        if resp.status_code != 200:
            raise GorochuError('Error {}'.format(resp))

        return resp.json().get('requested', False)

    def get_data(self, user_id):
        resp = requests.get(
            '{}/getData/{}'.format(self.endpoint, user_id),
            headers=self.__headers
        )

        if resp.status_code == 403:
            raise GorochuError(
                'Error. User has not authorized data sharing with you')

        elif resp.status_code != 200:
            raise GorochuError('Error {}'.format(resp))

        data = resp.json()

        return data
