#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import os
import re
import sys
import time
import random
import signal
import logging
import argparse

import nursejoybot.model as model
import nursejoybot.staff as staff
import nursejoybot.tablas as tablas

from nursejoybot.gorochu import GorochuAPI
from nursejoybot.nanny import process_cmd, set_nanny
from logging.handlers import  TimedRotatingFileHandler

from telegram.ext import (
    Updater,
    CommandHandler,
    ConversationHandler,
    MessageHandler,
    InlineQueryHandler,
    CallbackQueryHandler,
    RegexHandler,
    Filters
)

from nursejoybot.nursejoybot import (
    whois,
    whois_id,
    tabla,
    start,
    joyping,
    profile,
    info_rgpd,
    fuck_rgpd,
    max_cp,
    min_cp,
    pkdx,
    fc_list
)
from nursejoybot.lists import (
    create_list,
    list_btn,
    refloat,
    joy_close,
    joy_reopen
)
from nursejoybot.register import (
    register,
    set_friend_id,
    user_privacity,
    privacity_btn,
    set_switch_id,
    set_ds_id,
    process_photo_private,
    process_private_message
)
from nursejoybot.settings import (
    set_zone,   
    set_welcome,   
    settings,
    set_maxmembers,
    settingsbutton,
    miscbuttons,
    set_cooldown
)
from nursejoybot.admin import (
    ban,
    kick,
    unban,
    warn,
    settings_admin,
    create_adm,
    rm_adm,
    groups,
    add_url,
    link,
    tag,
    unlink,
    request_list_uv,
    request_kick_uv,
    kick_lvl,
    kick_msg,
    kick_old
)
from nursejoybot.group import (
    joined_chat,
    left_group,
    process_group_message
)
from nursejoybot.rules import (
    rules,
    set_rules,
    clear_rules
)
from nursejoybot.custom_commands import (
    list_commands,
    delete_commands,
    create_command,
    get_cmd,
    get_reply,
    cancel_command,
    GET_CMD,
    GET_REPLY
)
from nursejoybot.nests import (
    new_nest,
    erase_nest,
    erase_group_nests,
    list_nests,
    find_nest
)
from nursejoybot.services import (
    send_news,
    init_news,
    stop_news,
    add_news,
    rm_news,
    list_news
)
from nursejoybot.supportmethods import (
    error_callback,
    cleanup,
    parse_profile_image,
    callback_update_validations_status,
    create_needed_paths
)
from nursejoybot.config import (
    get_default_config_path,
    create_default_config,
    get_config
)
from nursejoybot.welcome import (
    test_welcome
)


def start_bot():
    signal.signal(signal.SIGINT, cleanup)
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-c', '--cfg', default=get_default_config_path(),
        type=str,
        help='path to the nursejoy bot config file (Default: %(default)s'
    )

    parser.add_argument(
        '--create-config', default=False,
        action='store_true',
        help=''
    )

    args = parser.parse_args()

    if args.create_config:
        create_default_config(args.cfg)
        sys.exit(0)

    config = get_config(args.cfg)
    create_needed_paths()
    model.create_databases()

    log = (
        os.path.expanduser(config['general'].get('log')) or
        os.path.join(os.getcwd(), 'debug.log')
    )

    log_handler = TimedRotatingFileHandler(
        log, when='d', interval=1, backupCount=5
    )

    logging.basicConfig(
        format="%(asctime)s %(name)s %(module)s:%(funcName)s:%(lineno)s - %(message)s",
        handlers=[log_handler],
        level=logging.DEBUG
    )

    logging.info("--------------------- Starting bot! -----------------------")

    updater = Updater(token=config["telegram"]["token"], workers=36, request_kwargs={'read_timeout': 15, 'connect_timeout': 15})
    dispatcher = updater.dispatcher
    dispatcher.add_error_handler(error_callback)

    #Custom commands commands xD
    dispatcher.add_handler(CommandHandler('new_cmd', create_command, Filters.group, pass_args=True))
    dispatcher.add_handler(RegexHandler(r'^[Ll]istado de [Cc]omandos$', list_commands))
    dispatcher.add_handler(RegexHandler(r'^[Ee]liminar [Cc]omando ([0-9]{1,10})$', delete_commands))

    # Admin group settings cmds
    dispatcher.add_handler(CommandHandler('create_admin', create_adm, Filters.group))
    dispatcher.add_handler(CommandHandler('settings_admin', settings_admin, Filters.group))
    dispatcher.add_handler(CommandHandler('rm_admin', rm_adm, Filters.group))
    dispatcher.add_handler(CommandHandler('create_link', link, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('rm_link', unlink, Filters.group))
    dispatcher.add_handler(CommandHandler('add_url', add_url, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('add_tag', tag, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler(['groups','group'], groups, Filters.group))

    # Admins commands
    dispatcher.add_handler(CommandHandler('unban', unban, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('kick', kick, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('warn', warn, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('ban', ban, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler(['joykicklvl','kicklvl'], kick_lvl, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('joykickuv', request_kick_uv, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('joykickmsg', kick_msg, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('joykickold', kick_old, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('joyuv', request_list_uv, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('test_welcome', test_welcome, Filters.group))

    # Basic commands
    dispatcher.add_handler(CommandHandler('profile', profile))
    dispatcher.add_handler(CommandHandler('pkdx', pkdx, Filters.chat(529767166), pass_args=True))
    dispatcher.add_handler(CommandHandler('joyping', joyping))  
    dispatcher.add_handler(CommandHandler(['help','start'], start, pass_args=True))
    dispatcher.add_handler(CommandHandler(['fclist','fc'], fc_list))
    dispatcher.add_handler(CommandHandler(['privacity'], user_privacity, Filters.private))
    dispatcher.add_handler(CommandHandler(['quienes','whois'], whois, pass_args=True))
    dispatcher.add_handler(CommandHandler('id', whois_id, pass_args=True))
    dispatcher.add_handler(CommandHandler(['tabla','table'], tabla, pass_args=True))
    dispatcher.add_handler(CommandHandler(['max','maxiv','ivmax'], max_cp, pass_args=True))
    dispatcher.add_handler(CommandHandler(['min','miniv','ivmin'], min_cp, pass_args=True))
    dispatcher.add_handler(RegexHandler(r'^([Qq][uU][iI][eéEÉ][nN] [eE][sS])$', whois))
    dispatcher.add_handler(RegexHandler(r'^([Qq][uU][iI][eéEÉ][nN] [eE][sS] +[a-zA-Z0-9_]{3,15})$', whois))
    dispatcher.add_handler(RegexHandler(r'^RGPD$', info_rgpd))
    dispatcher.add_handler(RegexHandler(r'^B0rr4r t0d05 m15 d4t05$', fuck_rgpd))

    # Register Commands
    dispatcher.add_handler(CommandHandler('register', register, Filters.private))
    dispatcher.add_handler(CommandHandler(['set_friendid','set_friendID'], set_friend_id, Filters.private, pass_args=True))
    dispatcher.add_handler(CommandHandler('set_switch', set_switch_id, Filters.private, pass_args=True))
    dispatcher.add_handler(CommandHandler('set_ds', set_ds_id, Filters.private, pass_args=True))
    dispatcher.add_handler(MessageHandler(Filters.private & Filters.photo, process_photo_private))
    dispatcher.add_handler(MessageHandler(Filters.private & (~ Filters.photo), process_private_message))

    # Lists Commands
    dispatcher.add_handler(CommandHandler('rules', rules, Filters.group))   
    dispatcher.add_handler(CommandHandler('set_rules', set_rules, Filters.group))  
    dispatcher.add_handler(CommandHandler('clear_rules', clear_rules, Filters.group))

    # Lists Commands
    dispatcher.add_handler(CommandHandler(['listrefloat','joyrefloat'], refloat, Filters.group))   
    dispatcher.add_handler(CommandHandler(['listclose','joyclose'], joy_close, Filters.group))  
    dispatcher.add_handler(CommandHandler(['listopen','joyopen'], joy_reopen, Filters.group)) 
    dispatcher.add_handler(CommandHandler('joylist', create_list, Filters.group))  

    # Group config commands
    # dispatcher.add_handler(CommandHandler('update', refresh))
    dispatcher.add_handler(CommandHandler('settings', settings, Filters.group))
    dispatcher.add_handler(CommandHandler('set_zone', set_zone, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('set_maxmembers', set_maxmembers, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('set_cooldown', set_cooldown, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('set_welcome', set_welcome, Filters.group))

    # Staff commands
    dispatcher.add_handler(CommandHandler('c', staff.edit_cmd, Filters.chat([-1001330396711]), pass_args=True))
    dispatcher.add_handler(CommandHandler('v', staff.validate, Filters.chat([-1001283985028])))
    dispatcher.add_handler(CommandHandler('reset', staff.reset_cmd, Filters.chat([-1001283985028]), pass_args=True))
    dispatcher.add_handler(CommandHandler('b', staff.ban_cmd, Filters.chat([-1001382078354]),pass_args=True))
    dispatcher.add_handler(CommandHandler('ub', staff.unban_cmd, Filters.chat([-1001382078354]),pass_args=True))
    dispatcher.add_handler(CommandHandler(['a','ch'], staff.ch_staff, Filters.chat([-1001283985028]),pass_args=True))

    #Tablas cmds
    dispatcher.add_handler(CommandHandler('tablas', tablas.list_pics, Filters.chat([-1001293393047, -1001284943779])))
    dispatcher.add_handler(CommandHandler('upload', tablas.upload_pictures, Filters.chat([-1001293393047, -1001284943779]),pass_args=True))
    dispatcher.add_handler(CommandHandler('nueva_tabla', tablas.new_pic, Filters.chat([-1001293393047, -1001284943779]),pass_args=True))
    dispatcher.add_handler(CommandHandler('editar_tabla', tablas.edit_pic, Filters.chat([-1001293393047, -1001284943779]),pass_args=True))
    dispatcher.add_handler(CommandHandler('borrar_tabla', tablas.rm_pic, Filters.chat([-1001293393047, -1001284943779]),pass_args=True))


    # Nests commands
    dispatcher.add_handler(RegexHandler(r'^[Rr]egistr(ar|o) (nido|spawn) de ([a-zA-Z0-9\- ]{3,10}) en ([a-zA-ZÀ-Üà-ü0-9\- \/.•()·\']{3,50})$', new_nest))
    dispatcher.add_handler(RegexHandler(r'^[Ee]liminar (nido|spawn) n[uú]mero ([0-9]{1,5})$', erase_nest))
    dispatcher.add_handler(RegexHandler(r'^[Bb]uscar (nido|spawn) (de|en) ([a-zA-ZÀ-Üà-ü0-9\- ()·\'•]{3,50})$', find_nest))
    dispatcher.add_handler(RegexHandler(r'^[Ee]liminar todos los nidos de este grupo$', erase_group_nests))
    dispatcher.add_handler(RegexHandler(r'^[Ll]istado de [Nn]idos$', list_nests)) 

    # Me & Services commands
    #dispatcher.add_handler(CommandHandler('emoji', emoji, Filters.chat(529767166), pass_args=True))

    #News services
    dispatcher.add_handler(CommandHandler('add_news', add_news, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('rm_news', rm_news, Filters.group, pass_args=True))
    dispatcher.add_handler(CommandHandler('list_news', list_news, Filters.group))

    # User moves messages
    dispatcher.add_handler(MessageHandler(Filters.group & Filters.status_update.new_chat_members, joined_chat, pass_job_queue=True)) 
    dispatcher.add_handler(MessageHandler(Filters.group & Filters.status_update.left_chat_member, left_group))   

    #Nanny messages
    dispatcher.add_handler(CommandHandler('set_nanny', set_nanny, Filters.group))  
    dispatcher.add_handler(MessageHandler(Filters.command, process_cmd))
    dispatcher.add_handler(MessageHandler(Filters.group & Filters.all, process_group_message))
    dispatcher.add_handler(MessageHandler(Filters.all, send_news))

    #Callback handlers
    dispatcher.add_handler(CallbackQueryHandler(tablas.tablas_btn, pattern=r"^tabla_"))
    dispatcher.add_handler(CallbackQueryHandler(privacity_btn, pattern=r"^privacity_"))
    dispatcher.add_handler(CallbackQueryHandler(list_btn, pattern=r"^list_"))
    dispatcher.add_handler(CallbackQueryHandler(settingsbutton, pattern=r"^settings_"))
    dispatcher.add_handler(CallbackQueryHandler(miscbuttons))

    dispatcher.add_handler(InlineQueryHandler(tablas.inline_tablas))
    
    
    job_queue = updater.job_queue
    job1 = job_queue.run_repeating(
        callback_update_validations_status, interval=300, first=60
    )

    updater.start_polling(timeout=25)
    
    sys.exit(0)


def generate_models():
    '''
    Method to create the models for Tesseract OCR
    '''

    parser = argparse.ArgumentParser(
        description='''Generate model images

The images will be stored on the "inspectdir" directory
defined in the configuration file
'''
    )

    parser.add_argument(
        '-c', '--cfg', default=get_default_config_path(),
        type=str,
        help='path to the nursejoy bot config file (Default: %(default)s'
    )

    parser.add_argument(
        "directory", type=str,
        help="create the models for the images in this directory"
    )

    args = parser.parse_args()
    config = get_config(args.cfg)
    create_needed_paths()

    log = (
        os.path.expanduser(config['general'].get('log')) or
        os.path.join(os.getcwd(), 'debug.log')
    )

    logging.basicConfig(
        filename=log,
        format='%(asctime)s %(name)s %(message)s', level=logging.DEBUG
    )

    logging.info(
        "--------------------- Starting {}! -----------------------".format(
            sys.argv[0])
    )

    directory = os.path.join(os.getcwd(), args.directory)
    print(directory)

    for root, dirs, filenames in os.walk(directory):
        for filename in filenames:
            m = re.match(
                    r'(Rojo|Amarillo|Azul)_([0-9]{2})_([A-Za-z0-9]{3,15})_'
                    r'([A-Za-z]{3,12})_([A-Za-z0-9]{3,12})\.jpg', filename)

            if m is None:
                continue

            expected_pokemon = m.group(4)

            inspect_file_name = '{}_{}'.format(
                str(time.time()), str(random.randrange(100, 999))
            )

            print("Parsing {}".format(filename))
            parse_profile_image(
                os.path.join(root, filename),
                expected_pokemon,
                inspect=True,
                inspect_base_name=inspect_file_name
            )

    sys.exit(0)


def test_gorochu():
    '''
    This method is not linked on setup.py

    It is only an usage example on Gorochu API.

    Gorochu provider should be defined in the configuration file, under the new
    "gorochu" section.

    It should be a pair of name_endpoint and name_token keys on the configuration file on
    that section to being able to use them.

    In this caase, we only rely on Pikachu, so I named the endpoint as "pikachu_endpoint"
    and "pikachu_token".

    TODO: This method won't ever reach Prerelease or master
    '''
    config = get_config(get_default_config_path())

    endpoint = config['gorochu']['pikachu_endpoint']
    token = config['gorochu']['pikachu_token']
 
    gorochu = GorochuAPI(endpoint, token)
    user_id = sys.argv[1]

    if gorochu.is_registered(user_id):
        status = gorochu.is_authorized()

        if status.value == 'accepted':
            data = gorochu.get_data(user_id)
            print(data)
            sys.exit(0)

        else:
            print("Nos authorized {}".format(status))
            sys.exit(1)

    else:
        print("Not user {}".format(user_id))
        sys.exit(1)

    sys.exit(0)
