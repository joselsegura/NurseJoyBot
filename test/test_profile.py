# -*- coding: utf-8 -*-

from unittest import TestCase

from nursejoybot.profile_parser import Profile
from nursejoybot.model import Team


class ProfileTestCase(TestCase):
    def test_create_profile(self):
        profile = Profile(
            'Name', 'N@me', 30, Team.BLUE, 'Growlithe', 'BuddyName',
            'BuddyN4me', None)

        self.assertEqual(profile.names, ['name', 'n@me'])
        self.assertEqual(profile.buddy_names, ['buddyname', 'buddyn4me'])

    def test_check_name(self):
        profile = Profile(
            'Name', 'N@me', 30, Team.BLUE, 'Growlithe', 'BuddyName',
            'BuddyN4me', None)

        self.assertTrue(profile.check_name('Name'))
        self.assertTrue(profile.check_name('N4me'))
        self.assertTrue(profile.check_name('N4m3'))

        self.assertFalse(profile.check_name("NotMyName"))

    def test_check_buddy_name(self):
        profile = Profile(
            'Name', 'N@me', 30, Team.BLUE, 'Growlithe', 'BuddyName',
            'BuddyN4me', None)

        self.assertTrue(profile.check_buddy_name('BuddyName'))
        self.assertTrue(profile.check_buddy_name('BuddyN4me'))
        self.assertTrue(profile.check_buddy_name('BuddyN4m3'))

        self.assertFalse(profile.check_name("NotMyName"))
