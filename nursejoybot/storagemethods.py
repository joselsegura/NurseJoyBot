#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import logging
import random
from datetime import datetime
import threading

from sqlalchemy.sql.expression import and_, or_

from nursejoybot.db import get_session
from nursejoybot.model import (
    ValidationRequiered,
    WarnLimit,
    Team,
    Rules,
    News,
    NewsSubs,
    Types,
    ValidationType,
    ValidationStep,
    GroupType,
    MinLevel,
    MinDays,
    MinMessages,
    Verified,
    Group,
    User,
    UserGroup,
    Validation,
    LinkedGroups,
    AdminGroups,
    SettingsNurse,
    SettingsNests,
    SettingsJoin,
    Nests,
    Welcome,
    WelcomeButtons,
    CustomCommands,
    CommandsButtons,
    SettingsPole,
    SettingsSafari,
    SettingsAdmin,
    SettingsGroup,
    SafariBag
)


validation_pokemons = ["larvitar", "whismur", "growlithe", "diglett", "marill", "houndour", "nosepass", "dunsparce"]
validation_profiles = ["model1", "model2", "model3", "model4", "model5", "model6"]
validation_names = ["Calabaza", "Puerro", "Cebolleta", "Remolacha", "Aceituna", "Pimiento", "Zanahoria", "Tomate", "Guisante", "Coliflor", "Pepino", "Berenjena", "Perejil", "Batata", "Aguacate", "Alcaparra", "Escarola", "Lechuga", "Hinojo"]

# Define Locks
INSERTION_USER_LOCK = threading.RLock()
INSERTION_USERGROUP_LOCK = threading.RLock()
INSERTION_VALIDATION_LOCK = threading.RLock()
INSERTION_GROUP_LOCK = threading.RLock()
INSERTION_WELCOME_LOCK = threading.RLock()
WELC_BTN_LOCK = threading.RLock()
INSERTION_SNESTS_LOCK = threading.RLock()
INSERTION_JOIN_LOCK = threading.RLock()
INSERTION_NEWS_LOCK = threading.RLock()
INSERTION_NESTS_LOCK = threading.RLock()
INSERTION_ADM_LOCK = threading.RLock()
INSERTION_LINK_LOCK = threading.RLock()
INSERTION_CMD_LOCK = threading.RLock()
INSERTION_RULES_LOCK = threading.RLock()

#--------------------------------------------------------------------------#
#GET
def get_unique_from_query(query):
    '''
    Handly method to retrieve an element from a query when it should be unique.

    If no element matches the query or there are several ones, return None
    '''

    return query[0] if query.count() == 1 else None

#--------------------------------------------------------------------------#
# GLOBAL READ ONLY
def are_banned(user_id, group_id):  # If user/group banned
    try:
        session = get_session()
        if user_id != group_id:
            group = get_unique_from_query(
                session.query(Group).filter(Group.id == group_id)
            )
        else:
            group = None
        user = get_unique_from_query(
            session.query(User).filter(User.id == user_id)
        )
        if (user is not None and user.banned) or (group is not None and group.banned):
            return True
        else:
            return False
    finally:
        session.close()

#GLOBAL R/W
def set_group(group_id, group_title): #Create basic group -> void
    session = get_session()
    group = Group(id=group_id, title=group_title)
    session.add(group)
    session.commit()
    set_nur = SettingsNurse(id=group_id)
    set_nes = SettingsNests(id=group_id)
    set_j = SettingsJoin(id=group_id)
    set_w = Welcome(chat_id=group_id)
    set_pole = SettingsPole(id=group_id)
    set_safari = SettingsSafari(id=group_id)
    set_adm = SettingsAdmin(id=group_id)
    set_group = SettingsGroup(id=group_id)
    session.add(set_nur)
    session.add(set_nes)
    session.add(set_j)
    session.add(set_w)
    session.add(set_pole)
    session.add(set_adm)
    session.add(set_safari)
    session.add(set_group)
    session.commit()
    session.close()
    return

#--------------------------------------------------------------------------#
#USER READ ONLY
def get_user(user_id): #Filter user by id -> user
    try:
        session = get_session()
        user = get_unique_from_query(
            session.query(User).filter(User.id == user_id)
        )
        return user
    finally:
        session.close()

def get_user_by_name(username): #Filter user by name -> bool
    try:    
        session = get_session()
        user = get_unique_from_query(
            session.query(User).filter(or_(
                User.trainer_name == username,
                User.username == username))
        )
        return user
    finally:
        session.close()

def owned_trainername(username, user_id):
    try:    
        session = get_session()
        user = get_unique_from_query(
                session.query(User).filter(
                    and_(
                        User.trainer_name == username,
                        User.id != user_id
                    )
                )
            )   
        if user is not None:
            return True
        else:
            return False
    finally:
        session.close()

def has_fc(user_id): #Check if user has FC -> bool
    try:        
        session = get_session()
        fc = get_unique_from_query(
            session.query(User.friend_id).filter(User.id == user_id)
        )
        if fc is not None:
            return True
        else:
            return False
    finally:
        session.close()

def has_ds_fc(user_id): #Check if user has FC -> bool
    try:        
        session = get_session()
        fc = get_unique_from_query(
            session.query(User.ds_id).filter(User.id == user_id)
        )
        if fc is not None:
            return True
        else:
            return False
    finally:
        session.close()

def has_switch_fc(user_id): #Check if user has FC -> bool
    try:        
        session = get_session()
        fc = get_unique_from_query(
            session.query(User.switch_id).filter(User.id == user_id)
        )
        if fc is not None:
            return True
        else:
            return False
    finally:
        session.close()

#USER R/W
def set_user(user_id, user_username):
    with INSERTION_USER_LOCK:
        session = get_session()
        user = User(id=user_id, username=user_username)
        session.add(user)
        session.commit()
        session.close()
        return

def update_fclist(user_id):
    with INSERTION_USER_LOCK:
        session = get_session()
        user = get_unique_from_query(
            session.query(User).filter(User.id == user_id)
        )
        user.fclists = not user.fclists
        session.commit()
        session.close()

def update_mentions(user_id, admin):
    with INSERTION_USER_LOCK:
        session = get_session()
        user = get_unique_from_query(
            session.query(User).filter(User.id == user_id)
        )
        if admin:
            user.adm_alerts = not user.adm_alerts
        else:
            user.usr_alerts = not user.usr_alerts
        session.commit()
        session.close()

def del_user(user_id):
    with INSERTION_USER_LOCK:
        session = get_session()
        session.query(Validation).filter(Validation.user_id==user_id).delete()
        session.query(SafariBag).filter(SafariBag.id == user_id).delete()
        session.query(User).filter(User.id == user_id).delete()
        session.commit()
        session.close()
        return


def ban_user(user_id, flag):
    with INSERTION_USER_LOCK:
        session = get_session()
        user = get_unique_from_query(
            session.query(User).filter(User.id == user_id)
        )
        user.flag = flag
        user.banned = True
        session.commit()
        session.close()


def unban_user(user_id):
    with INSERTION_USER_LOCK:
        session = get_session()
        user = get_unique_from_query(
            session.query(User).filter(User.id == user_id)
        )
        user.flag = ""
        user.banned = False
        session.commit()
        session.close()


def rgpd_user(user_id):
    with INSERTION_USER_LOCK:
        session = get_session()
        user = get_unique_from_query(
            session.query(User).filter(User.id == user_id)
        )
        user.validation_type = ValidationType.NONE
        user.friend_id = None
        user.team = Team.NONE
        user.level = 0
        user.flag = "🗑"
        user.banned = True
        session.commit()

    with INSERTION_USERGROUP_LOCK:
        guser = session.query(UserGroup).filter(
                UserGroup.user_id == user_id)
        data = guser
        session.query(UserGroup).filter(
                UserGroup.user_id == user_id).delete()

    with INSERTION_NESTS_LOCK:
        session.query(Nests).filter(
            Nests.user_id==user_id
        ).delete()

    session.close()
    return data

def commit_user(user_id, nick=None, team=None, validated=None, level=None):
    with INSERTION_USER_LOCK:
        session = get_session()
        user = get_unique_from_query(
            session.query(User).filter(User.id == user_id)
        )
        if nick is not None:
            user.trainer_name = nick
        if validated is not None:
            user.validation_type = validated
        if team is not None:
            user.team = team
        if level is not None:
            user.level = level
        session.commit()
        session.close()

def update_level(level, user_id):
    with INSERTION_USER_LOCK:
        session = get_session()
        user = get_unique_from_query(
            session.query(User).filter(User.id == user_id)
        )
        user.level = level
        session.commit()
        session.close()

def set_fc(fc, user_id):
    with INSERTION_USER_LOCK:
        session = get_session()
        user = get_unique_from_query(
            session.query(User).filter(User.id == user_id)
        )
        user.friend_id = fc
        session.commit()
        session.close()

def set_dsfc(fc, user_id):
    with INSERTION_USER_LOCK:
        session = get_session()
        user = get_unique_from_query(
            session.query(User).filter(User.id == user_id)
        )
        user.ds_id = fc
        session.commit()
        session.close()

def set_swichfc(fc, user_id):
    with INSERTION_USER_LOCK:
        session = get_session()
        user = get_unique_from_query(
            session.query(User).filter(User.id == user_id)
        )
        user.switch_id = fc
        session.commit()
        session.close()
#--------------------------------------------------------------------------#
#USERGROUP READ ONLY
def exists_user_group(user_id, chat_id): #Check usergroup on BBDD -> bool
    try:
        session = get_session()
        guser = get_unique_from_query(
            session.query(UserGroup).filter(and_(
                UserGroup.group_id == chat_id,
                UserGroup.user_id == user_id))
        )
        if guser is None:
            return False
        else:
            return True
    finally:
        session.close()

def get_user_group(user_id, chat_id): #Check usergroup on BBDD -> bool
    try:
        session = get_session()
        guser = get_unique_from_query(
            session.query(UserGroup).filter(and_(
                UserGroup.group_id == chat_id,
                UserGroup.user_id == user_id))
        )
        return guser
    finally:
        session.close()

#USERGROUP R/W
def warn_user(chat_id, user_id, warning): #Update warns -> int
    with INSERTION_USERGROUP_LOCK:
        session = get_session()
        guser = get_unique_from_query(
            session.query(UserGroup).filter(and_(
                UserGroup.group_id == chat_id,
                UserGroup.user_id == user_id))
        )
        if warning == 0:
            guser.warn = 0
        elif warning > 0 or (warning < 0 and guser.warn != 0):
            guser.warn = guser.warn + warning
            warning = guser.warn
        else:
            warning = 999
        session.commit()
        session.close()
        return warning

def remove_warn(user_id, chat_id):#edit
    with INSERTION_USERGROUP_LOCK:
        session = get_session()
        removed = False
        warned_user = get_unique_from_query(
            session.query(UserGroup).filter(and_(
                UserGroup.group_id == chat_id,
                UserGroup.user_id == user_id))
        )
        if warned_user and warned_user.warn > 0:
            warned_user.warn -= 1
            session.commit()
            removed = True

        session.close()
        return removed

def set_user_group(user_id, chat_id): #Create basic user in group -> void
    with INSERTION_USERGROUP_LOCK:
        session = get_session()
        guser = UserGroup(user_id = user_id, group_id = chat_id)
        session.add(guser)
        session.commit()
        session.close()
        return

def join_group(user_id, chat_id, value):
    with INSERTION_USERGROUP_LOCK:
        session = get_session()
        guser = get_unique_from_query(
            session.query(UserGroup).filter(and_(
                UserGroup.group_id == chat_id,
                UserGroup.user_id == user_id))
        )
        if guser is None:
            return
        guser.left_group = value
        guser.join_date = datetime.utcnow()
        session.commit()
        session.close()
        return

def message_counter(user_id, chat_id):
    with INSERTION_USERGROUP_LOCK:
        session = get_session()
        guser = get_unique_from_query(
            session.query(UserGroup).filter(and_(
                UserGroup.group_id == chat_id,
                UserGroup.user_id == user_id))
        )
        guser.total_messages += 1
        guser.last_message = datetime.utcnow()
        session.commit()
        session.close()
        return

#--------------------------------------------------------------------------#
#GROUP READ ONLY
def get_warn_limit(group_id): #enum 2 int -> int
    try:    
        session = get_session()
        group = get_unique_from_query(
            session.query(SettingsGroup).filter(SettingsGroup.id == group_id)
        )
        warn = group.warn
        if warn == WarnLimit.EXTRICT:
            warns = 5
        elif warn == WarnLimit.LOW_PERMISIVE:
            warns = 10
        elif warn == WarnLimit.MED_PERMISIVE:
            warns = 25
        elif warn == WarnLimit.HIGH_PERMISIVE:
            warns = 50
        elif warn == WarnLimit.SO_TOLERANT:
            warns = 100
        return warns  
    finally:
        session.close()

def get_group(group_id):
    try:
        session = get_session()
        group = get_unique_from_query(
            session.query(Group).filter(Group.id == group_id)
        )
        return group
    finally:
        session.close()

def get_group_settings(group_id):
    try:
        session = get_session()
        group = get_unique_from_query(
            session.query(SettingsGroup).filter(SettingsGroup.id == group_id)
        )
        return group
    finally:
        session.close()

def get_users_from_group(group_id):
    try:
        session = get_session()
        gusers = session.query(UserGroup).filter(
                UserGroup.group_id == group_id)
        return gusers
    finally:
        session.close()
#GROUP R/W
def set_general_settings(chat_id, settings_str):
    with INSERTION_GROUP_LOCK:
        session = get_session()
        group = get_unique_from_query(
            session.query(SettingsGroup).filter(SettingsGroup.id == chat_id)
        )
        group.set_settings_from_str(settings_str)
        session.commit()
        session.close()
        return

def set_group_info(chat_id, alias, title):
    with INSERTION_GROUP_LOCK:
        session = get_session()
        group = get_unique_from_query(
            session.query(Group).filter(Group.id == chat_id)
        )
        group.title = alias
        group.alias = title
        session.close()
    return

#--------------------------------------------------------------------------#
#VALIDATION READ ONLY
def active_validation(user_id):
    try:
        session = get_session()
        validation = get_unique_from_query(
            session.query(Validation).filter(
                and_(
                    Validation.user_id == user_id,
                    or_(
                        Validation.step == ValidationStep.WAITING_NAME,
                        Validation.step == ValidationStep.WAITING_SCREENSHOT,
                        Validation.step == ValidationStep.FAILED
                    )
                )
            )
        )
        return validation
    finally:
        session.close()

def waiting_trainer(user_id):
    try:
        session = get_session()
        validation = get_unique_from_query(
            session.query(Validation).filter(
                and_(
                    Validation.user_id == user_id,
                    Validation.step == ValidationStep.WAITING_NAME
                    )
                )
            )
        if validation is None:
            return False
        else:
            return True
    finally:
        session.close()

def waiting_picture(user_id):
    try:
        session = get_session()
        validation = get_unique_from_query(
            session.query(Validation).filter(
                and_(
                    or_(
                        Validation.step == ValidationStep.WAITING_SCREENSHOT,
                        Validation.step == ValidationStep.FAILED
                    ),
                    Validation.user_id == user_id
                )
            )
        )
        return validation
    finally:
        session.close()

#VALIDATION R/W
def expire_validation(user_id): #Expires Current Validation -> void
    with INSERTION_VALIDATION_LOCK:
        session = get_session()
        validation = get_unique_from_query(
            session.query(Validation).filter(
                and_(
                    Validation.user_id == user_id,
                    or_(
                        Validation.step == ValidationStep.WAITING_NAME,
                        Validation.step == ValidationStep.WAITING_SCREENSHOT,
                        Validation.step == ValidationStep.FAILED
                    )
                )
            )
        )
        Validation.step == ValidationStep.EXPIRED
        session.commit()
        session.close()
        return

def expire_validations(time):
    with INSERTION_VALIDATION_LOCK:
        session = get_session()
        validations = session.query(Validation).filter(
            and_(
                or_(
                    Validation.step == ValidationStep.WAITING_NAME,
                    Validation.step == ValidationStep.WAITING_SCREENSHOT,
                    Validation.step == ValidationStep.FAILED
                ),
                Validation.started_time < time
            )
        )
        for v in validations:
            v.step = ValidationStep.EXPIRED
        session.commit()
        session.close()
        return validations

def new_validation(chat_id):
    with INSERTION_VALIDATION_LOCK:
        pokemon = random.choice(validation_pokemons)
        name = random.choice(validation_names)
        session = get_session()
        validation = Validation(
            user_id=chat_id,
            step=ValidationStep.WAITING_NAME,
            pokemon=pokemon,
            tries=0,
            pokemon_name=name
        )
        session.add(validation)
        session.commit()

def add_trainername(user_id, name): #Expires Current Validation -> void
    with INSERTION_VALIDATION_LOCK:
        session = get_session()
        validation = get_unique_from_query(
            session.query(Validation).filter(
                and_(
                    Validation.user_id == user_id,
                    Validation.step == ValidationStep.WAITING_NAME
                )
            )
        )
        validation.step = ValidationStep.WAITING_SCREENSHOT
        validation.trainer_name = name
        session.commit()
        session.close()
        return validation

def increase_validation(user_id):
    with INSERTION_VALIDATION_LOCK:
        session = get_session()
        validation = get_unique_from_query(
            session.query(Validation).filter(
                and_(
                    or_(
                        Validation.step == ValidationStep.WAITING_SCREENSHOT,
                        Validation.step == ValidationStep.FAILED
                    ),
                    Validation.user_id == user_id
                )
            )
        )
        validation.tries += 1
        if validation.tries == 3:
            validation.step = ValidationStep.FAILED
            session.commit()
            session.close()
            return True
        session.commit()
        session.close()
        return False

def complete_validation(level, team, user_id):
    with INSERTION_VALIDATION_LOCK:
        session = get_session()
        validation = get_unique_from_query(
            session.query(Validation).filter(
                and_(
                    or_(
                        Validation.step == ValidationStep.WAITING_SCREENSHOT,
                        Validation.step == ValidationStep.FAILED
                    ),
                    Validation.user_id == user_id
                )
            )
        )
        validation.level = level
        validation.team = team
        validation.step = ValidationStep.COMPLETED
        session.commit()
        session.close()
        return

#--------------------------------------------------------------------------#
#WELCOME READ ONLY
def get_welc_pref(chat_id):
    try:
        session = get_session()
        welc = session.query(Welcome).get(str(chat_id))
        if welc:
            return welc.should_welcome, welc.custom_welcome, welc.welcome_type
        else:
            # Welcome by default.
            return False, None, Types.TEXT
    finally:
        session.close()

def get_custom_welcome(chat_id):
    try:
        session = get_session()
        welcome_settings = session.query(Welcome).get(str(chat_id))
        ret = DEFAULT_WELCOME
        if welcome_settings and welcome_settings.custom_welcome:
            ret = welcome_settings.custom_welcome
        return ret
    finally:
        session.close()

def get_welc_buttons(chat_id):
    session = get_session()
    try:
        return session.query(WelcomeButtons).filter(WelcomeButtons.chat_id == str(chat_id)).order_by(
            WelcomeButtons.id).all()
    finally:
        session.close()

def get_welcome_settings(chat_id):
    try:    
        session = get_session()
        group = get_unique_from_query(
            session.query(Welcome).filter(Welcome.chat_id == chat_id)
        )
        return group
    finally:
        session.close()

#WELCOME R/W
def set_welc_preference(chat_id, should_welcome):
    with INSERTION_WELCOME_LOCK:
        session = get_session()
        curr = session.query(Welcome).get(str(chat_id))
        if not curr:
            curr = Welcome(str(chat_id), should_welcome=should_welcome)
        else:
            curr.should_welcome = should_welcome

        session.add(curr)
        session.commit()

def set_custom_welcome(chat_id, custom_welcome, welcome_type, buttons=None):
    if buttons is None:
        buttons = []

    with INSERTION_WELCOME_LOCK:
        session = get_session()
        welcome_settings = session.query(Welcome).get(str(chat_id))
        if not welcome_settings:
            welcome_settings = Welcome(str(chat_id), True)

        if custom_welcome:
            welcome_settings.custom_welcome = custom_welcome
            welcome_settings.welcome_type = welcome_type.value

        else:
            welcome_settings.custom_welcome = DEFAULT_GOODBYE
            welcome_settings.welcome_type = Types.TEXT.value

        session.add(welcome_settings)

        with WELC_BTN_LOCK:
            prev_buttons = session.query(WelcomeButtons).filter(WelcomeButtons.chat_id == str(chat_id)).all()
            for btn in prev_buttons:
                session.delete(btn)

            for b_name, url, same_line in buttons:
                button = WelcomeButtons(chat_id, b_name, url, same_line)
                session.add(button)

        session.commit()

def set_welcome_settings(chat_id, settings_str):
    with INSERTION_WELCOME_LOCK:
        session = get_session()
        group = get_unique_from_query(
            session.query(Welcome).filter(Welcome.chat_id == chat_id)
        )
        group.set_welcomeset_from_str()
        session.commit()
        session.close()
        return

#--------------------------------------------------------------------------#
#JOIN READ ONLY
def get_join_settings(chat_id):
    try:
        session = get_session()
        group = get_unique_from_query(
            session.query(SettingsJoin).filter(SettingsJoin.id == chat_id)
        )
        return group
    finally:
        session.close()

#JOIN R/W
def set_join_settings(chat_id, settings_str):
    with INSERTION_JOIN_LOCK:
        session = get_session()
        group = get_unique_from_query(
            session.query(SettingsJoin).filter(SettingsJoin.id == chat_id)
        )
        group.set_joinset_from_str(settings_str)
        session.commit()
        session.close()
        return

def set_max_members(chat_id, max_members):
    with INSERTION_JOIN_LOCK:
        session = get_session()
        group = get_unique_from_query(
            session.query(SettingsJoin).filter(SettingsJoin.id == chat_id)
        )
        group.max_members = max_members
        session.commit()
        session.close()
        return

def set_welcome_cooldown(chat_id, delete_cooldown):
    with INSERTION_JOIN_LOCK:
        session = get_session()
        group = get_unique_from_query(
            session.query(SettingsJoin).filter(SettingsJoin.id == chat_id)
        )
        group.delete_cooldown = delete_cooldown
        session.commit()
        session.close()
        return

#--------------------------------------------------------------------------#
#SETTINGS NESTS READ ONLY
def get_nests_settings(chat_id):
    try:
        session = get_session()
        group = get_unique_from_query(
            session.query(SettingsNests).filter(SettingsNests.id == chat_id)
        )
        return group
    finally:
        session.close()

#SETTINGS NESTS R/W
def set_nests_settings(chat_id, settings_str):
    with INSERTION_SNESTS_LOCK: 
        session = get_session()
        group = get_unique_from_query(
            session.query(SettingsNests).filter(SettingsNests.id == chat_id)
        )
        group.set_nestset_from_str(settings_str)
        session.commit()
        session.close()
        return

def nest_link(chat_id, linked_id, delete=False):
    with INSERTION_LINK_LOCK:
        session = get_session()
        if delete:
            session.query(LinkedNests).filter(
                and_(
                    LinkedNests.group_id== chat_id,
                    LinkedNests.linked_id == linked_id
            )).delete()
        else:
            nest = LinkedNests(group_id=chat_id, linked_id=linked_id)
            session.add(linked)
        session.commit()
        session.close()
        return 
#--------------------------------------------------------------------------#
#PROVIDERS READ ONLY
def is_news_provider(news_id):
    try:
        session = get_session()
        provider = get_unique_from_query(
            session.query(News).filter(
                News.id == news_id
            )
        )
        if provider is None:
            return False
        return True
    finally:
        session.close()

def get_verified_providers():
    try:
        session = get_session()
        providers = session.query(News).filter(News.active == True)
        return providers
    finally:
        session.close()

def get_news_provider(news_id):
    try:
        session = get_session()
        provider = get_unique_from_query(
            session.query(News).filter(
                News.id == news_id
            )
        )
        return provider
    finally:
        session.close()

#PROVIDERS NEWS R/W
def set_news_provider(news_id, alias):
    with INSERTION_NEWS_LOCK:
        session = get_session()
        news = News(id=news_id, alias=alias, active=False)
        session.add(news)
        session.commit()
        session.close()
        return
    
def rm_news_provider(news_id):
    with INSERTION_NEWS_LOCK:
        session = get_session()
        session.query(NewsSubs).filter(NewsSubs.news_id == news_id).delete()
        session.query(News).filter(News.id == news_id).delete()
        session.commit()
        session.close()
        return


#NEWS SUBS READ ONLY
def is_news_subscribed(chat_id, news_id):
    try:
        session = get_session()
        group = get_unique_from_query(
            session.query(NewsSubs).filter(and_(
                NewsSubs.news_id == news_id,
                NewsSubs.user_id == chat_id
                )
            )
        )
        if group is None:
            return False
        return True
    finally:
        session.close()

def get_news_consumers(news_id):
    try:
        session = get_session()
        group = session.query(NewsSubs).filter(NewsSubs.news_id == news_id)
        return group
    finally:
        session.close()

def get_news_subscribed(chat_id):
    try:
        session = get_session()
        group = session.query(NewsSubs).filter(NewsSubs.user_id == chat_id)
        return group
    finally:
        session.close()

#NEWS SUBS R/W
def set_news_subscription(chat_id, news_id):
    with INSERTION_NEWS_LOCK:
        session = get_session()
        news = NewsSubs(
            user_id=chat_id,
            news_id=news_id
        )
        session.add(news)
        session.commit()
        session.close()
        return
    
def rm_news_subscription(chat_id, news_id):
    with INSERTION_NEWS_LOCK:
        session = get_session()
        session.query(NewsSubs).filter(and_(
            NewsSubs.user_id == chat_id,
            NewsSubs.news_id == news_id)
        ).delete()
        session.commit()
        session.close()
        return

def rm_all_news_subscription(chat_id):
    with INSERTION_NEWS_LOCK:
        session = get_session()
        session.query(NewsSubs).filter(NewsSubs.user_id == chat_id).delete()
        session.commit()
        session.close()
        return

#--------------------------------------------------------------------------#
#NESTS READ ONLY
def get_group_nest(chat_id):
    try:
        session = get_session()
        nests = session.query(Nests).filter(Nests.group_id == chat_id)
        return nests
    finally:
        session.close()

def search_pokemon(chat_id, arg):
    try:
        session = get_session()
        nests = session.query(Nests).filter(
            and_(
                Nests.group_id == chat_id,
                str(Nests.pokemon).lower() == arg.lower()
            )
        )
        return nests
    finally:
        session.close()

def search_place(chat_id, arg):
    try:
        session = get_session()
        nests = session.query(Nests).filter(
            and_(
                Nests.group_id == chat_id,
                str(Nests.place).lower() == arg.lower()
            )
        )
        return nests
    finally:
        session.close()

#NESTS R/W
def add_nest(user_id, chat_id, pokemon, place, low):
    with INSERTION_NESTS_LOCK:
        session = get_session()
        nest = Nests(
            user_id=user_id,
            group_id=chat_id,
            pokemon=pokemon,
            place=place,
            spawn=low
        )
        session.add(nest)
        session.commit()
        return

def delete_nest(chat_id, numer):
    with INSERTION_NESTS_LOCK:
        session = get_session()
        try:
            nest = session.query(Nests).filter(
                and_(
                    Nests.group_id==chat_id,
                    Nests.id == numer
            ))
            if nest is None:
                return False
            session.query(Nests).filter(
                and_(
                    Nests.group_id==chat_id,
                    Nests.id == numer
            )).delete()
            session.commit()
            return True
        except:
            session.rollback()
            return False
        finally:
            session.close()

def delete_group_nest(chat_id):
    with INSERTION_NESTS_LOCK:
        session = get_session()
        try:
            session.query(Nests).filter(
                Nests.group_id==chat_id
            ).delete()
            session.commit()
            out = True
        except:
            session.rollback()
            out = False
        finally:
            session.close()
            return out

def delete_all_nest():
    with INSERTION_NESTS_LOCK:
        session = get_session()
        try:
            session.query(Nests).delete()
            session.commit()
            out = True
        except:
            session.rollback()
            out = False
        finally:
            session.close()
            return out

#--------------------------------------------------------------------------#
#ADMIN READ ONLY
def get_admin(chat_id):
    try:
        session = get_session()
        admin = get_unique_from_query(
            session.query(AdminGroups).filter(
                AdminGroups.id == chat_id)
        )
        return admin
    finally:
        session.close()

def get_particular_admin(chat_id):
    try:
        session = get_session()
        admin = get_unique_from_query(
            session.query(SettingsAdmin).filter(
                SettingsAdmin.id == chat_id)
        )
        return admin
    finally:
        session.close()

def get_admin_from_linked(chat_id):
    try:
        session = get_session()
        linked = get_unique_from_query(
            session.query(LinkedGroups).filter(
                LinkedGroups.linked_id == chat_id)
        )
        if linked is None:
            session.close()
            return None
        admin = get_unique_from_query(
            session.query(AdminGroups).filter(
                AdminGroups.id == linked.admin_id)
        )
        return admin
    finally:
        session.close()

#ADMIN R/W
def set_admin_settings(chat_id, settings_str):
    with INSERTION_ADM_LOCK:
        session = get_session() 
        admin = get_unique_from_query(
            session.query(AdminGroups).filter(
                AdminGroups.id == chat_id)
        )
        admin.set_admset_from_str(settings_str)
        session.commit()
        session.close()
        return

def set_ladmin_settings(chat_id, settings_str):
    with INSERTION_ADM_LOCK:
        session = get_session() 
        admin = get_unique_from_query(
            session.query(SettingsAdmin).filter(
                SettingsAdmin.id == chat_id)
        )
        admin.set_setadm_from_str(settings_str)
        session.commit()
        session.close()
        return

def set_admin(chat_id):
    with INSERTION_ADM_LOCK:
        session = get_session()
        admin = AdminGroups(id=chat_id)
        session.add(admin)
        session.commit()
        session.close()
        return  

def rm_admin(chat_id):
    with INSERTION_ADM_LOCK:
        session = get_session()
        try:
            session.query(LinkedGroups).filter(LinkedGroups.admin_id==chat_id).delete()
            session.query(AdminGroups).filter(AdminGroups.id==chat_id).delete()
            session.commit()
            out = True
        except:
            session.rollback()
            out = False
        finally:
            session.close()
            return out
#--------------------------------------------------------------------------#
#LINKED READ ONLY
def get_team_linked(chat_id, team):
    try:
        session = get_session()
        
        linked = get_unique_from_query(
            session.query(LinkedGroups).filter(and_(
                LinkedGroups.admin_id == chat_id,
                LinkedGroups.group_type == team))
        )
        if linked is None:
            return False
        else:
            return True
    finally:
        session.close()

def get_linked_admin(chat_id, args):
    try:
        session = get_session()
        
        linked = get_unique_from_query(
            session.query(LinkedGroups).filter(and_(
                LinkedGroups.admin_id == chat_id,
                LinkedGroups.linked_id == args))
        )
        if linked is None:
            return False
        else:
            return True
    finally:
        session.close()

def get_all_groups(chat_id):
    try:
        session = get_session()
        
        linked = get_unique_from_query(
            session.query(LinkedGroups).filter(
                LinkedGroups.linked_id == chat_id)
        )
        if linked is None:
            linked = get_unique_from_query(
                session.query(AdminGroups).filter(
                    AdminGroups.id == chat_id))
            if linked is None:
                session.close()
                return None
            else:
                admin_id = linked.id
        else:
            admin_id = linked.admin_id

        groups = session.query(LinkedGroups).filter(
            LinkedGroups.admin_id == admin_id)
        return groups
    finally:
        session.close()

def get_groups(chat_id):
    try:
        session = get_session()
        linked = get_unique_from_query(
            session.query(LinkedGroups).filter(
                LinkedGroups.linked_id == chat_id)
        )
        return linked
    finally:
        session.close()

def get_linked(group_id):
    try:
        session = get_session()
        linked = get_unique_from_query(
            session.query(LinkedGroups).filter(
                LinkedGroups.linked_id == group_id)
        )
        if linked is not None:
            return linked.admin_id, linked.group_type
        else:
            return None, None
    finally:
        session.close()

def get_team_group(group_id, gtype):
    try:
        session = get_session()
        linked = get_unique_from_query(
            session.query(LinkedGroups).filter(and_(
                LinkedGroups.admin_id == group_id,
                LinkedGroups.group_type == gtype))
        )
        return linked.admin_id, linked.group_type
    finally:
        session.close()
#LINKED R/W
def new_link(admin, chat, title):
    with INSERTION_LINK_LOCK:
        session = get_session()
        linked = LinkedGroups(admin_id=admin, linked_id=chat, title=title)
        session.add(linked)
        session.commit()
        session.close()
        return  

def mock_team_link(chat_id, group_id, team, link):
    if group_id is None:
        with INSERTION_LINK_LOCK:
            session = get_session()
            session.query(LinkedGroups).filter(and_(
                LinkedGroups.admin_id == chat_id,
                LinkedGroups.group_type == team)).delete()
            session.commit()
            session.close()
            return 
    else:
        with INSERTION_LINK_LOCK:
            with INSERTION_GROUP_LOCK:
                session = get_session()
                group = Group(id=group_id)
                linked = LinkedGroups(
                    admin_id=chat_id,
                    linked_id=group_id,
                    group_type=team,
                    link=link
                )
                session.add(group)
                session.add(linked)
                session.commit()
                session.close()
                return  

def rv_link(group_id):
    with INSERTION_LINK_LOCK:
        session = get_session()
        session.query(LinkedGroups).filter(
            LinkedGroups.linked_id == group_id).delete()
        session.commit()
        session.close()
        return 

def rm_link(group_id):
    with INSERTION_LINK_LOCK:
        session = get_session()
        session.query(LinkedGroups).filter(and_(
            LinkedGroups.linked_id == group_id
            )).delete()
        session.commit()
        session.close()
        return 

def add_tlink(chat_id, arg):
    with INSERTION_LINK_LOCK:
        session = get_session()
        linked = get_unique_from_query(
            session.query(LinkedGroups).filter(
                LinkedGroups.linked_id == chat_id)
        )
        if linked is None:
            session.close()
            return None
        linked.link = arg
        session.commit()
        session.close()
        return

def add_type(chat_id, arg):
    with INSERTION_LINK_LOCK:
        session = get_session()
        linked = get_unique_from_query(
            session.query(LinkedGroups).filter(
                LinkedGroups.linked_id == chat_id)
        )
        if linked is None:
            session.close()
            return
        linked.group_type = arg
        session.commit()
        session.close()
        return

def set_tag(chat_id, arg):
    with INSERTION_LINK_LOCK:
        session = get_session()
        linked = get_unique_from_query(
            session.query(LinkedGroups).filter(
                LinkedGroups.linked_id == chat_id)
        )
        if linked is None:
            session.close()
            return
        linked.label = arg
        session.commit()
        session.close()
        return

#--------------------------------------------------------------------------#
#COMMANDS READ ONLY
def get_active_command(chat_id):
    try:
        session = get_session()
        cmd = get_unique_from_query(session.query(CustomCommands).filter(and_(
            CustomCommands.chat_id==chat_id,
            CustomCommands.media=="{}".format(chat_id)
        )))
        return cmd
    finally:
        session.close()

def get_commands(chat_id):
    try:
        session = get_session()        
        cmd = session.query(CustomCommands).filter(and_(
            CustomCommands.chat_id==chat_id,
            CustomCommands.media!="{}".format(chat_id)
        ))
        return cmd
    finally:
        session.close()

def get_cmd_buttons(cmd_id):
    session = get_session()
    try:
        return session.query(CommandsButtons).filter(CommandsButtons.cmd_id == str(cmd_id)).order_by(CommandsButtons.id).all()
    finally:
        session.close()

#COMMANDS R/W
def set_custom_command(chat_id, arg):
    with INSERTION_CMD_LOCK:
        session = get_session()
        cmd = get_unique_from_query(session.query(CustomCommands).filter(and_(
            CustomCommands.chat_id==chat_id,
            CustomCommands.command=="{}".format(arg.lower())
        )))
        if cmd is None:
            cmd = CustomCommands(chat_id=chat_id, command=arg.lower(), media="{}".format(chat_id))
            session.add(cmd)
        else:
            cmd.media="{}".format(chat_id)
        session.commit()
        session.close()
        return

def set_custom_command_reply(chat_id, arg, ctype, buttons=None, retry=True):
    logging.debug("%s, %s, %s, %s, %s", chat_id, arg, ctype, buttons, retry)
    if buttons is None:
        buttons = []

    with INSERTION_CMD_LOCK:
        session = get_session()
        cmd = get_unique_from_query(
            session.query(CustomCommands).filter(and_(
                CustomCommands.chat_id == chat_id,
                CustomCommands.media == "{}".format(chat_id)
            )))

        if cmd is None:
            cmd = get_unique_from_query(
                session.query(CustomCommands).filter(and_(
                    CustomCommands.chat_id == chat_id,
                    CustomCommands.media == chat_id
                )))

        if cmd is None and retry:
            return set_custom_command_reply(chat_id, arg, ctype, None, False)

        if arg:
            cmd.media = arg
            cmd.command_type = ctype.value
        else:
            return
            
        session.add(cmd)
        with WELC_BTN_LOCK:
            prev_buttons = session.query(CommandsButtons).filter(CommandsButtons.cmd_id == str(cmd.id)).all()
            for btn in prev_buttons:
                session.delete(btn)

            for b_name, url, same_line in buttons:
                button = CommandsButtons(cmd.id, b_name, url, same_line)
                session.add(button)

        session.commit()
        return

def set_custom_welcome(chat_id, custom_welcome, welcome_type, buttons=None):
    if buttons is None:
        buttons = []

    with INSERTION_WELCOME_LOCK:
        session = get_session()
        welcome_settings = session.query(Welcome).get(str(chat_id))
        if not welcome_settings:
            welcome_settings = Welcome(str(chat_id), True)

        if custom_welcome:
            welcome_settings.custom_welcome = custom_welcome
            welcome_settings.welcome_type = welcome_type.value

        else:
            welcome_settings.custom_welcome = DEFAULT_GOODBYE
            welcome_settings.welcome_type = Types.TEXT.value

        session.add(welcome_settings)

        with WELC_BTN_LOCK:
            prev_buttons = session.query(WelcomeButtons).filter(WelcomeButtons.chat_id == str(chat_id)).all()
            for btn in prev_buttons:
                session.delete(btn)

            for b_name, url, same_line in buttons:
                button = WelcomeButtons(chat_id, b_name, url, same_line)
                session.add(button)

        session.commit()


def rm_command(chat_id, command):
    with INSERTION_CMD_LOCK:
        session = get_session()
        try:
            session.query(CustomCommands).filter(and_(
                CustomCommands.chat_id==chat_id,
                CustomCommands.id==command
            )).delete()
            session.commit()
            out = True
        except:
            session.rollback()
            out = False
        finally:
            session.close()
            return out
            
#--------------------------------------------------------------------------#
#NANNY READ ONLY
def get_nanny_settings(chat_id):
    try:
        session = get_session()
        nanny = get_unique_from_query(
            session.query(SettingsNurse).filter(
            SettingsNurse.id==chat_id)
        )
        return nanny
    finally:
        session.close()

#NANNY R/W
def set_nanny_reply(chat_id, text):
    with INSERTION_JOIN_LOCK:
        session = get_session()
        nanny = get_unique_from_query(
            session.query(SettingsNurse).filter(
            SettingsNurse.id==chat_id)
        )
        nanny.reply = text
        session.commit()
        session.close()
        return

def set_nanny_settings(chat_id, settings_str):
    with INSERTION_JOIN_LOCK:
        session = get_session()
        nanny = get_unique_from_query(
            session.query(SettingsNurse).filter(
            SettingsNurse.id==chat_id)
        )
        nanny.set_nurseset_from_str(settings_str)
        session.commit()
        session.close()
        return

#--------------------------------------------------------------------------#
#NANNY READ ONLY
def get_rules(chat_id):
    try:
        session = get_session()
        rules = session.query(Rules).get(str(chat_id))
        ret = ""
        if rules:
            ret = rules.rules
        return ret
    finally:
        session.close()

def has_rules(chat_id):
    try:
        session = get_session()
        rules = session.query(Rules).get(str(chat_id))
        if rules is not None and rules.rules != "":
            return True
        return False
    finally:
        session.close()

#RULES R/W
def add_rules(chat_id, rules_text):
    with INSERTION_RULES_LOCK:
        session = get_session()
        rules = session.query(Rules).get(str(chat_id))
        if not rules:
            rules = Rules(str(chat_id))
        rules.rules = rules_text
        session.add(rules)
        session.commit()
        session.close()
        return
