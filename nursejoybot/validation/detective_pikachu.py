#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import re

from nursejoybot.validation import Validator, ValidationData


class PikachuValidator(Validator):
    '''
    Validator for Detective Pikachu (@detectivepikachubot).

    The expected format for a valid profile is the following:

    ID de Telegram: User telegram ID (as str representing an int)
    Alias de Telegram: Telegram alias of the user (it can be empty)
    Nombre de entrenador: Trainer name
    Estado cuenta: Validation status
    Equipo: Team in Spanish
    Nivel: Level (as str representing an int)
    '''

    REGESP = re.compile(
        r'ID de Telegram: (\d+)\n'
        r'Alias de Telegram: (Desconocido|[a-zA-Z0-9_]{5,32})\n'
        r'Nombre de entrenador: ([a-zA-Z0-9_]{3,15})\n'
        r'Estado cuenta: (Validada|.*)\n'
        r'Equipo: (Valor|Sabiduría|Instinto)\n'
        r'Nivel: ([0-9]{1,2})',
        flags=re.IGNORECASE | re.MULTILINE
    )
    REGFR = re.compile(
        r'ID dans Telegram: (\d+)\n'
        r'Pseudonyme dans Telegram: ([a-zA-Z0-9_]{2,16})\n'
        r'Nom de dresseur: ([a-zA-Z0-9_]{2,16})\n'
        r'État de la compte: (Validée|.*)\n'
        r'Équipe: (Bravoure|Sagesse|Intuition)\n'
        r'Niveau: ([0-9]{1,2})',
        flags=re.IGNORECASE | re.MULTILINE
    )
    REGGAL = re.compile(
        r'ID de Telegram: (\d+)\n'
        r'Alias de Telegram: ([a-zA-Z0-9_]{2,16})\n'
        r'Nome de adestrador: ([a-zA-Z0-9_]{2,16})\n'
        r'Estado da conta: (Validada|.*)\n'
        r'Equipo: (Valor|Sabedoría|Instinto)\n'
        r'Nivel: ([0-9]{1,2})',
        flags=re.IGNORECASE | re.MULTILINE
    )

    BOT_ID = 409242898
    BOT_NAME = 'Detective Pikachu'

    def validate(self, msg_text):
        '''
        This method will get a multiline string with the forwarded
        message from Detective Pikachu bot.

        This method will check if it has the right format and will return
        a ValidationData object or None, if the message doesn't have the
        expected format
        '''

        match = PikachuValidator.REGESP.match(msg_text)

        if match is not None:
            telegram_id = int(match.group(1))
            if match.group(2) is "Desconocido":
                telegram_alias = None
            else:
                telegram_alias = match.group(2)
            trainer_name = match.group(3)
            valid_account = match.group(4) == 'Validada'
            trainer_team = match.group(5)
            trainer_level = int(match.group(6))

            return ValidationData(
                telegram_id,
                telegram_alias,
                trainer_name,
                valid_account,
                trainer_team,
                trainer_level
            )

        return None
