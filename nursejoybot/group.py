#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import os
import re
import logging
import time
from Levenshtein import distance
from threading import Thread

import telegram
from telegram.ext.dispatcher import run_async

from nursejoybot.config import get_config
from nursejoybot.gorochu import GorochuAPI, GorochuError
from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup
)

from nursejoybot.supportmethods import (
    is_admin,
    build_keyboard,
    extract_update_info,
    delete_message_timed,
    delete_message,
    send_message_timed,
    ensure_escaped,
    markdown_parser
)
from nursejoybot.model import (
    ValidationRequiered,
    ValidationType,
    Team,
    Types,
    MinLevel,
    GroupType
)
from nursejoybot.storagemethods import (
    get_admin,
    get_group,
    set_group,
    get_user,
    get_admin_from_linked,
    exists_user_group,
    set_user_group,
    are_banned,
    get_welc_pref,
    join_group,
    get_join_settings,
    message_counter,
    get_commands,
    get_linked,
    get_team_group,
    has_rules,
    get_particular_admin,
    get_cmd_buttons
)
from nursejoybot.nanny import (
    nanny_text,
    process_gif,
    process_cmd,
    process_contact,
    process_file,
    process_game,
    process_ubi,
    process_pic,
    process_sticker,
    process_voice,
    process_video,
    process_url
)
from nursejoybot.welcome import send_welcome

class DeleteContext:
  def __init__(self, chat_id, message_id):
    self.chat_id = chat_id
    self.message_id = message_id

@run_async
def joined_chat(bot, update, job_queue):
    '''
    Method to handle update status. It will be used mainly to check users
    that enter or leave the group
    '''

    logging.debug("%s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    new_members = update.effective_message.new_chat_members
    new_chat_member = message.new_chat_members[0] \
        if message.new_chat_members else None

    config = get_config()
    bot_alias = config['telegram']['botalias']
    gvalidation_id = int(config['telegram'].get('group_validation', 0))

    if new_chat_member.username == bot_alias:
        if are_banned(user_id, chat_id):
            bot.leaveChat(chat_id=chat_id)
            return
        chat_title = message.chat.title
        chat_id = message.chat.id
        logging.debug("Getting Group")
        group = get_group(chat_id)
        logging.debug("Group: %s", group)
        if group is None:
            logging.debug("Creating Group")
            set_group(chat_id, message.chat.title)
            logging.debug("DONE!")

        if gvalidation_id != 0:
            message_text=(
                "Nuevo grupo registrado:\n"
                "ID de grupo: *{}*\n"
                "Nombre del grupo: *{}*").format(message.chat.id, message.chat.title)
            bot.sendMessage(
                chat_id=gvalidation_id,
                text=message_text,
                parse_mode=telegram.ParseMode.MARKDOWN
            )

        message_text = (
            "Entrenadores de *{}*, sed bienvenidos al Centro P"
            "okémon de la región de Telegram.\nAntes de poder "
            "utilizarme, un administrador tiene que configurar"
            " algunas cosas. Comenzad viendo la ayuda con el c"
            "omando `/help` para conocer todas las funciones. "
            "Aseguraos de ver la *ayuda para administradores*,"
            " donde se explica en detalle todos los pasos que "
            "se deben seguir.".format(ensure_escaped(chat_title))
        )

        Thread(
            target=send_message_timed,
            args=(chat_id, message_text, 3, bot)
        ).start()

    elif new_chat_member.username != bot_alias:
        chat_id = message.chat.id
        user_id = update.effective_message.new_chat_members[0].id

        group = get_join_settings(chat_id)
        if group is not None:
            if group.silence:
                delete_message(chat_id, message.message_id, bot)

            user = get_user(user_id)       
            if (user is None or user.level is None) and (group.validationrequired is not ValidationRequiered.NO_VALIDATION or group.levelrequired is not MinLevel.NO_LEVEL):
                bot.kickChatMember(chat_id=chat_id, user_id=user_id, until_date=time.time()+31)
                if group.mute is False:
                    output = "👌 Entrenador sin registrarse expulsado!"
                    bot.sendMessage(
                        chat_id=chat_id, 
                        text=output, 
                        parse_mode=telegram.ParseMode.MARKDOWN)
                good_luck(bot, update, "Not registered")
                return

            min_level = get_min_level(group)
            if min_level is not 0 and (user is None or user.level is None or user.level < min_level):
                bot.kickChatMember(chat_id=chat_id, user_id=user_id, until_date=time.time()+31)
                if group.mute is False:
                    output = "👌 Entrenador novel expulsado"
                    bot.sendMessage(
                        chat_id=chat_id, 
                        text=output, 
                        parse_mode=telegram.ParseMode.MARKDOWN)           
                good_luck(bot, update, "Low level")
                try:
                    bot.sendMessage(
                        chat_id=user_id, 
                        text="❌ Debes alcanzar el nivel {} para entrar en este grupo".format(min_level), 
                        parse_mode=telegram.ParseMode.MARKDOWN)
                except Exception:
                    pass
                return

            if are_banned(user_id, user_id) and user.level is not 0:
                bot.kickChatMember(chat_id, user_id)
                if group.mute is False:
                    path = os.path.expanduser(config['general']['tablesdir'])
                    dirs = os.listdir(path)
                    output = "💢 El Team Rocket despega de nuevo"
                    sticker_path = '{}/team_rocket.webp'.format(path)
                    bot.send_sticker(
                        chat_id=chat_id,
                        sticker=open(sticker_path, 'rb'))
                    bot.sendMessage(
                        chat_id=chat_id, 
                        text=output, 
                        parse_mode=telegram.ParseMode.MARKDOWN)
                good_luck(bot, update, "Banned")
                return

            if group.validationrequired is ValidationRequiered.VALIDATION and user.validation_type is ValidationType.NONE:
                bot.kickChatMember(chat_id=chat_id, user_id=user_id, until_date=time.time()+31)
                if group.mute is False:
                    output = "👌 Entrenador sin validarse expulsado!"
                    bot.sendMessage(
                        chat_id=chat_id, 
                        text=output, 
                        parse_mode=telegram.ParseMode.MARKDOWN)           
                good_luck(bot, update, "Not registered")
                try:
                    bot.sendMessage(
                        chat_id=user_id, 
                        text="❌ Debes validarte para entrar en este grupo", 
                        parse_mode=telegram.ParseMode.MARKDOWN)
                except Exception:
                    pass
                return
        
            if group.validationrequired is ValidationRequiered.BLUE_EXCLUSIVE and user.team is not Team.BLUE:
                bot.kickChatMember(chat_id, user_id)
                if group.mute is False:
                    output = "👌 Infiltrado expulsado!"
                    bot.sendMessage(
                        chat_id=chat_id, 
                        text=output, 
                        parse_mode=telegram.ParseMode.MARKDOWN)
                good_luck(bot, update, "Other team")
                try:
                    bot.sendMessage(
                        chat_id=user_id, 
                        text="❌ No puedes entrar a este grupo", 
                        parse_mode=telegram.ParseMode.MARKDOWN)

                except Exception:
                    pass
                return
        
            if group.validationrequired is ValidationRequiered.YELLOW_EXCLUSIVE and user.team is not Team.YELLOW:
                bot.kickChatMember(chat_id, user_id)
                if group.mute is False:
                    output = "👌 Infiltrado expulsado!"
                    bot.sendMessage(
                        chat_id=chat_id, 
                        text=output, 
                        parse_mode=telegram.ParseMode.MARKDOWN)
                good_luck(bot, update, "Other team")
                try:
                    bot.sendMessage(
                        chat_id=user_id, 
                        text="❌ No puedes entrar a este grupo", 
                        parse_mode=telegram.ParseMode.MARKDOWN)
                except Exception:
                    pass
                return
        
            if group.validationrequired is ValidationRequiered.RED_EXCLUSIVE and user.team is not Team.RED:
                bot.kickChatMember(chat_id, user_id)
                if group.mute is False:
                    output = "👌 Infiltrado expulsado!"
                    bot.sendMessage(
                        chat_id=chat_id, 
                        text=output, 
                        parse_mode=telegram.ParseMode.MARKDOWN)
                good_luck(bot, update, "Other team")
                try:
                    bot.sendMessage(
                        chat_id=user_id, 
                        text="❌ No puedes entrar a este grupo", 
                        parse_mode=telegram.ParseMode.MARKDOWN)
                except Exception:
                    pass
                return

            if group.max_members is not None and group.max_members > 0 and bot.get_chat_members_count(chat_id) >= group.max_members:
                if group.mute is False:
                    output = "❌ El número máximo de integrantes en el grupo ha sido alcanzado"
                    sent = bot.sendMessage(
                        chat_id=chat_id, 
                        text=output, 
                        parse_mode=telegram.ParseMode.MARKDOWN)
                    delete_object = DeleteContext(chat_id, sent.message_id)
                    job_queue.run_once(
                        callback_delete, 
                        10,
                        context=delete_object
                    )
                time.sleep(2)
                bot.kickChatMember(chat_id=chat_id, user_id=user_id, until_date=time.time()+31)
                return

            if group.valpikachu and (user is None or user.validation_type in [ValidationType.NONE, ValidationType.INTERNAL]):
                if not_registered_pika(user_id):
                    bot.kickChatMember(chat_id=chat_id, user_id=user_id, until_date=time.time()+31)
                    if group.mute is False:
                        output = "👌 Entrenador sin registrarse expulsado! (Pikachu)"
                        bot.sendMessage(
                            chat_id=chat_id, 
                            text=output, 
                            parse_mode=telegram.ParseMode.MARKDOWN)
                    good_luck(bot, update, "Other team")
                    return

            if (not exists_user_group(user_id, chat_id)):
                set_user_group(user_id, chat_id)
            else:
                join_group(user_id, chat_id, False)

            logging.debug("HAS RULES %s", has_rules(chat_id))
            if has_rules(chat_id):
                bot.restrict_chat_member(
                    chat_id,
                    user_id,
                    until_date=0,
                    can_send_messages=False,
                    can_send_media_messages=False,
                    can_send_other_messages=False,
                    can_add_web_page_previews=False
                )

            if get_welc_pref(chat_id):
                sent = send_welcome(bot, update)
                if sent is not None and group.delete_cooldown is not None and group.delete_cooldown > 0:
                    delete_object = DeleteContext(chat_id, sent.message_id)
                    job_queue.run_once(
                        callback_delete, 
                        group.delete_cooldown,
                        context=delete_object
                    )

            if group.pikachu and (user is None or user.validation_type in [ValidationType.NONE, ValidationType.INTERNAL]):
                sent_message = alert_pika(bot, chat_id, user_id)
                if sent_message is not None:
                    delete_object = DeleteContext(chat_id, sent_message.message_id)
                    job_queue.run_once(
                        callback_delete, 
                        30,
                        context=delete_object
                    )

            if group.mute_pikachu and (user is None or user.validation_type in [ValidationType.NONE, ValidationType.INTERNAL]):
                sent_message = mute_pika(bot, chat_id, user_id)
                if sent_message is not None:
                    bot.restrict_chat_member(
                        chat_id,
                        user_id,
                        until_date=0,
                        can_send_messages=False,
                        can_send_media_messages=False,
                        can_send_other_messages=False,
                        can_add_web_page_previews=False
                    )

            if group.joy and (user is None or user.level is None):
                sent_message = alert_joy(bot, chat_id, user_id)
                if sent_message is not None:
                    delete_object = DeleteContext(chat_id, sent_message.message_id)
                    job_queue.run_once(
                        callback_delete, 
                        30,
                        context=delete_object
                    )
            
            ladmin = get_particular_admin(chat_id)
            if ladmin is not None and ladmin.welcome:
                admin = get_admin_from_linked(chat_id)
                if admin is not None and admin.welcome is True:
                    replace_pogo = replace(user_id, message.from_user.first_name)
                    message_text = ("ℹ️ {}\n👤 {} ha entrado en el grupo").format(message.chat.title, replace_pogo)
                    bot.sendMessage(chat_id=admin.id, text=message_text,
                                    parse_mode=telegram.ParseMode.MARKDOWN)

            if user and user.team is not Team.NONE and user.level is not 0:
                send_team_link(bot, user, chat_id)

        else:
            logging.info("CREATING GROUP")
            set_group(chat_id, message.chat.title)

            if gvalidation_id != 0:
                message_text=(
                "Nuevo grupo registrado:\n"
                "ID de grupo: *{}*\n"
                "Nombre del grupo: *{}*").format(message.chat.id, message.chat.title)
                bot.sendMessage(
                    chat_id=gvalidation_id,
                    text=message_text,
                    parse_mode=telegram.ParseMode.MARKDOWN
                )

            message_text = (
                "Entrenadores de *{}*, sed bienvenidos al Centro P"
                "okémon de la región de Telegram.\nAntes de poder "
                "utilizarme, un administrador tiene que configurar"
                " algunas cosas. Comenzad viendo la ayuda con el c"
                "omando `/help` para conocer todas las funciones. "
                "Aseguraos de ver la *ayuda para administradores*,"
                " donde se explica en detalle todos los pasos que "
                "se deben seguir.\n\n <Si este mensaje ha aparecido"
                "y no has promocionado el grupo a supergrupo, pide "
                "ayuda en @enfermerajoyayuda >".format(ensure_escaped(chat_title))
            )

            Thread(
                target=send_message_timed,
                args=(chat_id, message_text, 3, bot)
            ).start()

@run_async
def left_group(bot, update):
    logging.debug("%s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    user_id = message.left_chat_member.id
    join_group(user_id, chat_id, True)
            
    ladmin = get_particular_admin(chat_id)
    if ladmin is not None and ladmin.goodbye:
        admin = get_admin_from_linked(chat_id)
        if admin is not None and admin.goodbye is True:
            replace_pogo = replace(user_id, message.left_chat_member.first_name)
            message_text=("ℹ️ {}\n👤 {} ha abandonado el grupo").format(message.chat.title, replace_pogo)
            bot.sendMessage(
                chat_id=admin.id,
                text=message_text,
                parse_mode=telegram.ParseMode.MARKDOWN
            )

@run_async
def process_group_message(bot, update):
    '''
    Function that handles a message received in a group
    '''
    logging.debug("nursejoybot.process_group_message")
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    msg = update.effective_message
    
    if are_banned(user_id, chat_id):
        return
  
    group = get_group(chat_id)

    if group is None:
        set_group(chat_id, message.chat.title)
        logging.debug("GROUP created")
    if (not exists_user_group(user_id, chat_id)):
        set_user_group(user_id, chat_id)
        logging.debug("USER GROUP created")
    message_counter(user_id, chat_id)
    if text is None or msg.photo is None:
        if msg and msg.document:
            process_gif(bot, update)
            return
        elif msg and msg.contact:
            process_contact(bot, update)
            return
        elif msg and msg.game:
            process_game(bot, update)
            return
        elif msg and msg.location or msg.venue:
            process_ubi(bot, update)
            return
        elif msg and msg.photo:
            process_pic(bot, update)
            return
        elif msg and msg.sticker:
            process_sticker(bot, update)
            return
        elif msg and msg.voice or msg.audio:
            process_voice(bot, update)
            return
        elif msg and msg.video or msg.video_note:
            process_video(bot, update)
            return

    if msg and msg.entities and process_url(bot, update):
        return

    if nanny_text(bot, user_id, chat_id, message):
        return

    if text is not None and re.search("@admin(?!\w)", text) is not None:
        replace_pogo = replace(user_id, message.from_user.first_name)
        message_text=("ℹ️ {}\n👤 {} ha enviado una alerta a los administradores\n\nMensaje: {}").format(
            message.chat.title,
            replace_pogo,
            text
        )
        for admin in bot.get_chat_administrators(chat_id):
            user = get_user(admin.user.id)
            if user is not None and user.adm_alerts:
                bot.sendMessage(
                    chat_id=admin.user.id,
                    text=message_text,
                    parse_mode=telegram.ParseMode.MARKDOWN
                )
        ladmin = get_particular_admin(chat_id)
        if ladmin is not None and ladmin.admin:
            admin = get_admin_from_linked(chat_id)
            if admin is not None and admin.admin is True:
                bot.sendMessage(
                    chat_id=admin.id,
                    text=message_text,
                    parse_mode=telegram.ParseMode.MARKDOWN
                )
                return

    if text and len(text) < 31:
        commands = get_commands(chat_id)
        if commands is None:
            return
        for command in commands:
            logging.debug("%s %s", text, command)
            if distance(text.lower(), command.command.lower()) < 1:
                logging.debug("%s %s", text, command.command)
                ENUM_FUNC_MAP = {
                    Types.TEXT.value: bot.sendMessage,
                    Types.BUTTON_TEXT.value: bot.sendMessage,
                    Types.STICKER.value: bot.sendSticker,
                    Types.DOCUMENT.value: bot.sendDocument,
                    Types.PHOTO.value: bot.sendPhoto,
                    Types.AUDIO.value: bot.sendAudio,
                    Types.VOICE.value: bot.sendVoice,
                    Types.VIDEO.value: bot.sendVideo
                }
                if command.command_type != Types.TEXT and command.command_type != Types.BUTTON_TEXT:
                    ENUM_FUNC_MAP[command.command_type](chat_id, command.media)
                    return

                if command.command_type == Types.BUTTON_TEXT:
                    buttons = get_cmd_buttons(command.id)
                    keyb = build_keyboard(buttons)
                    keyboard = InlineKeyboardMarkup(keyb)
                else:
                    keyboard = None
                try:
                    msg = update.effective_message.reply_text(
                        command.media,
                        reply_markup=keyboard,
                        parse_mode=telegram.ParseMode.MARKDOWN,
                        disable_web_page_preview=True, 
                        disable_notification=False
                        )

                except IndexError:
                    msg = update.effective_message.reply_text(
                            markdown_parser(
                                "\nBip bop bip: El mensaje tiene errores de"
                                "Markdown, revisalo y configuralo de nuevo."),
                        parse_mode=telegram.ParseMode.MARKDOWN)
                except KeyError:
                    msg = update.effective_message.reply_text(
                            markdown_parser(
                                "\nBip bop bip: El mensaje tiene errores con"
                                "las llaves, revisalo y configuralo de nuevo."),
                        parse_mode=telegram.ParseMode.MARKDOWN)
                return

def send_team_link(bot, user, chat_id):
    group_type, admin_id = get_linked(chat_id)

    if group_type is None or group_type in [GroupType.RED, GroupType.BLUE, GroupType.YELLOW]:
        return

    if user.team is Team.RED:
        group = get_team_group(admin_id, GroupType.RED)
        equipo = "Valor"

    elif user.team is Team.BLUE:
        group = get_team_group(admin_id, GroupType.BLUE)
        equipo = "Sabiduría"

    elif user.team is Team.YELLOW:
        group = get_team_group(admin_id, GroupType.YELLOW)
        equipo = "Instinto"

    if group and group.link is not None:
        output = ( 
            "Hola! Has sido invitado al grupo del equipo {}.\n"
            "Mantén este enlace en secreto. {}".format(equipo, group.link)
        )
        try:
            bot.sendMessage(
                chat_id=user.id,
                text=output,
                parse_mode=telegram.ParseMode.MARKDOWN
            )
        except Exception:
            pass

    return

def good_luck(bot, update, reason):
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    ladmin = get_particular_admin(chat_id)
    if ladmin is not None and ladmin.welcome:
        admin = get_admin_from_linked(chat_id)
        if admin is not None and admin.welcome is True:
            replace_pogo = replace(user_id, message.from_user.first_name)
            message_text=("ℹ️ {}\n👤 {} ha intentado entrar.\nExpulsado por: *{}*").format(message.chat.title, replace_pogo, reason)
            bot.sendMessage(
                chat_id=admin.id,
                text=message_text,
                parse_mode=telegram.ParseMode.MARKDOWN
            )

def replace(user_id, first_name):
    user = get_user(user_id)
    logging.info('%s', user)

    if user is None or user.team is Team.NONE:
        text_team = "_Desconocido_"
    elif user.team is Team.BLUE:
        text_team = "💙"
    elif user.team is Team.RED:
        text_team = "❤️"
    elif user.team is Team.YELLOW:
        text_team = "💛"

    text_trainername = ("{}".format(user.trainer_name)
                        if user is not None and user.trainer_name is not None
                        else "{}".format(first_name))
    
    text_level = ("*{}*".format(user.level)
                  if user is not None and user.level is not None
                  else "??")

    if user is not None and user.banned == 1:
        text_validationstatus = "⛔️"

    elif user is not None and user.validation_type in [
            ValidationType.INTERNAL, ValidationType.PIKACHU, ValidationType.GOROCHU]:
        text_validationstatus = "✅"

    else:
        text_validationstatus = "⚠️"

    replace_pogo = "[{0}](tg://user?id={1}) `{1}` - *L*{2} {3} {4}".format(
        text_trainername,
        user_id,
        text_level,
        text_team,
        text_validationstatus)

    if user is not None and user.level is 0:
        return "[{0}](tg://user?id={1}) `{1}` - *L??* ⚠️".format(first_name, user_id)
    
    return replace_pogo

def alert_pika(bot, chat_id, user_id):
    config = get_config()
    endpoint = config['gorochu']['pikachu_endpoint']
    token = config['gorochu']['pikachu_token']
    gorochu = GorochuAPI(endpoint, token)
    try:
        if not gorochu.is_registered(user_id):
            button_list = [[
                InlineKeyboardButton(text="Empezar!", url="https://t.me/detectivepikachubot")
            ]]
            reply_markup = InlineKeyboardMarkup(button_list)
            sent_message = bot.send_message(
                chat_id=chat_id,
                text=(
                    "¡Hola entrenador/a!\n\nPara permanecer en este grupo, debes registra"
                    "rte conmigo: @detectivepikachubot.\nPara hacerlo, pulsa *Empezar*, i"
                    "nicia el chat privado y escríbele `/register`.\n\n👉 Siguiendo esos "
                    "sencillos pasos estarás validado/a."
                ),
                parse_mode=telegram.ParseMode.MARKDOWN,
                reply_markup=reply_markup
            )
            return sent_message
        return None
    except GorochuError:
        return None
    except ConnectionError:
        return None   


def mute_pika(bot, chat_id, user_id):
    config = get_config()
    endpoint = config['gorochu']['pikachu_endpoint']
    token = config['gorochu']['pikachu_token']
    gorochu = GorochuAPI(endpoint, token)
    try:
        if not gorochu.is_registered(user_id):
            button_list = [[
                InlineKeyboardButton(text="Empezar!", url="https://t.me/detectivepikachubot")
            ],[
                InlineKeyboardButton(text="Quitar el mute", url="pikamute_({})".format(user_id))
            ]]
            reply_markup = InlineKeyboardMarkup(button_list)
            sent_message = bot.send_message(
                chat_id=chat_id,
                text=(
                    "¡Hola entrenador/a!\n\nPara permanecer y hablar en este grupo, debes registra"
                    "rte conmigo: @detectivepikachubot.\nPara hacerlo, pulsa *Empezar*, i"
                    "nicia el chat privado y escríbele `/register`.\n\n👉 Siguiendo esos "
                    "sencillos pasos estarás validado/a.\nPara finalizar, pulsa el botón *Quitar el mute*"
                    " para poder hablar en el grupo"
                ),
                parse_mode=telegram.ParseMode.MARKDOWN,
                reply_markup=reply_markup
            )
            return sent_message
        return None
    except GorochuError:
        return None
    except ConnectionError:
        return None    

def pikamute_btn(bot, update):
    query = update.callback_query
    data = query.data
    user = update.effective_user
    username = query.from_user.username
    user_id = query.from_user.id
    text = query.message.text
    chat_id = query.message.chat.id
    message_id = query.message.message_id

    if are_banned(user_id, chat_id):
        return   

    match = re.match(r"pikamute_\((.+?)\)", query.data)
    btn_user_id = match.group(1)

    if btn_user_id == user_id:
        config = get_config()
        endpoint = config['gorochu']['pikachu_endpoint']
        token = config['gorochu']['pikachu_token']
        gorochu = GorochuAPI(endpoint, token)
        try:
            if gorochu.is_registered(user_id):
                bot.restrict_chat_member(
                    chat_id,
                    user_id,
                    can_send_messages=True,
                    can_send_media_messages=True,
                    can_send_other_messages=True,
                    can_add_web_page_previews=True
                )
                delete_message(chat_id, message_id, bot)
            return None
        except GorochuError:
            return None
        except ConnectionError:
            return None  

def alert_joy(bot, chat_id, user_id):
    button_list = [[
        InlineKeyboardButton(text="Empezar!", url="https://t.me/NurseJoyBot")
    ]]
    reply_markup = InlineKeyboardMarkup(button_list)
    sent_message = bot.send_message(
        chat_id=chat_id,
        text=(
            "¡Hola entrenador/a!\n\nPara permanecer en este grupo, debes registra"
            "rte conmigo: @NurseJoyBot.\nPara hacerlo, pulsa *Empezar*, inicia el"
            " chat privado y escríbeme `/register`.\n\n👉 Siguiendo esos sencillo"
            "s pasos estarás validado/a."
        ),
        parse_mode=telegram.ParseMode.MARKDOWN,
        reply_markup=reply_markup
    )
    return sent_message

def get_min_level(group):
    if group.levelrequired is MinLevel.NOOB:
        min_level = 15
    elif group.levelrequired is MinLevel.CASUAL:
        min_level = 20
    elif group.levelrequired is MinLevel.LOW_PLAYER:
        min_level = 25
    elif group.levelrequired is MinLevel.MED_PLAYER:
        min_level = 30
    elif group.levelrequired is MinLevel.HIGH_PLAYER:
        min_level = 35
    elif group.levelrequired is MinLevel.HARD_PLAYER:
        min_level = 40
    else:
        min_level = 0
    return min_level

def callback_delete(bot, job):
    try:
        bot.deleteMessage(
            chat_id=job.context.chat_id, 
            message_id=job.context.message_id
        )
        return
    except:
        return

def not_registered_pika(user_id):
    config = get_config()
    endpoint = config['gorochu']['pikachu_endpoint']
    token = config['gorochu']['pikachu_token']
    gorochu = GorochuAPI(endpoint, token)
    try:
        if gorochu.is_registered(user_id):
            return False
        else:
            return True
    except GorochuError:
        return True
    except ConnectionError:
        return True    