#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import os
import re
import json
import logging
import telegram
import urllib.request

from uuid import uuid4
from telegram.ext.dispatcher import run_async
from nursejoybot.supportmethods import extract_update_info
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, InlineQueryResultCachedPhoto

try:
    with open('/var/local/nursejoy/tablas.json') as f:
        __TABLAS_JSON = json.load(f)
except:
    __TABLAS_JSON = None


@run_async
def list_pics(bot, update, args=None):
    logging.debug("%s %s" % (bot, update))
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    output = "Listado de tablas:"
    count = 0

    if args is None or len(args)!=1:
        for k in __TABLAS_JSON["tablas"]:
            count = count + 1
            output = output + "\n\nTitle: {1}\nKeywords: {2}\nID: `{0}`".format(
                k["id"],
                k["name"],
                k["keywords"]
            )
            if count == 5:  
                bot.sendMessage(
                    chat_id=chat_id,
                    text=output,
                    parse_mode=telegram.ParseMode.MARKDOWN)
                count = 0
                output = ""
    else:
        for k in __TABLAS_JSON["tablas"]:
            if args[0] in k["keywords"]:
                count = count + 1
                output = output + "\n\nTitle: {1}\nKeywords: {2}\nID: `{0}`".format(
                    k["id"],
                    k["name"],
                    k["keywords"]
                )
                if count == 5:  
                    bot.sendMessage(
                        chat_id=chat_id,
                        text=output,
                        parse_mode=telegram.ParseMode.MARKDOWN)
                    count = 0
                    output = ""

    if count != 0:
        bot.sendMessage(
            chat_id=chat_id,
            text=output,
            parse_mode=telegram.ParseMode.MARKDOWN)


@run_async
def new_pic(bot, update, args=None):
    logging.debug("%s %s" % (bot, update))
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    if args is None or len(args)<3:
        return

    if message.reply_to_message is None or message.reply_to_message.photo is None:
        return

    with open('/var/local/nursejoy/tablas.json') as f:
        data = json.load(f)

    name = ""
    while args[0] != "-":
        name = name + args[0]
        del args[0]
    
    del args[0]

    tabla_id = "{}".format(uuid4())
    data['tablas'].insert(0, {  
        "id":tabla_id,
        "name":name,
        "file_id":update.message.reply_to_message.photo[-1]["file_id"],
        "keywords":args
    })

    with open('/var/local/nursejoy/tablas.json', 'w') as outfile:  
        json.dump(data, outfile)

    reload_tablas()

    output = "Nueva tabla añadida.\nID: `{}`\nNombre: *{}*".format(
        tabla_id,
        name
    )
    txt = name.replace(" ", "_")
    keyboard = [[
        InlineKeyboardButton(text="✅ Si", callback_data='tabla_new_{}'.format(txt)),
        InlineKeyboardButton(text="❌ No", callback_data='tabla_rm')
    ]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        reply_markup=reply_markup,
        parse_mode=telegram.ParseMode.MARKDOWN
    )


@run_async
def edit_pic(bot, update, args=None):
    logging.debug("%s %s" % (bot, update))
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    if args is None or len(args)!=1 or len(args[0])!=36:
        return

    if message.reply_to_message is None or message.reply_to_message.photo is None:
        return

    with open('/var/local/nursejoy/tablas.json') as f:
        data = json.load(f)

    for k in data["tablas"]:
        if k["id"] == args[0]:
            k["file_id"] = update.message.reply_to_message.photo[-1]["file_id"]
            break

    with open('/var/local/nursejoy/tablas.json', 'w') as outfile:  
        json.dump(data, outfile)

    reload_tablas()

    output = "Tabla editada.\nID: `{}`\nNombre: *{}*".format(
        k["id"],
        k["name"]
    )
    txt = k["name"].replace(" ", "_")
    keyboard = [[
        InlineKeyboardButton(text="✅ Si", callback_data='tabla_edit_{}'.format(txt)),
        InlineKeyboardButton(text="❌ No", callback_data='tabla_rm')
    ]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        reply_markup=reply_markup,
        parse_mode=telegram.ParseMode.MARKDOWN
    )


@run_async
def rm_pic(bot, update, args=None):
    logging.debug("%s %s" % (bot, update))
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    if args is None or len(args)!=1 or len(args[0])!=36:
        return
        
    with open('/var/local/nursejoy/tablas.json') as f:
        data = json.load(f)

    for k in data["tablas"]:
        if k["id"] == args[0]:
            data["tablas"].remove(k)
            break

    with open('/var/local/nursejoy/tablas.json', 'w') as outfile:  
        json.dump(data, outfile)

    reload_tablas()

    output = "Tabla eliminada."
    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )


@run_async
def upload_pictures(bot, update, args=None):
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    if args is None or len(args) == 0:
        return

    if message.reply_to_message is None or message.reply_to_message.photo is None:
        return

    photo = bot.get_file(update.message.reply_to_message.photo[-1]["file_id"])
    logging.debug("Downloading file %s", photo)

    for arg in args:
        filename = os.path.expanduser(
                "/var/local/nursejoy/tablas/{}.jpg".format(arg)
            )
        urllib.request.urlretrieve(photo["file_path"], filename)
        logging.debug("Storing received file in %s", filename)

    output = "Hecho!"

    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )


@run_async
def inline_tablas(bot, update):
    logging.debug("%s %s" % (bot, update))
    query = update.inline_query.query
    max_range = 25
    results = []

    if len(__TABLAS_JSON["tablas"]) < 25:
        max_range = len(__TABLAS_JSON["tablas"])

    if len(query)>2:
        count = 0
        for i in __TABLAS_JSON["tablas"]:
            if re.search(query.lower(), i["name"].lower()):
                count = count + 1
                results.append(
                    InlineQueryResultCachedPhoto(
                    id=uuid4(),
                    photo_file_id=i["file_id"],
                    title=i["name"],
                    caption="@NurseJoyBot {}".format(i["name"]))
                )

            if count == max_range:
                update.inline_query.answer(results=results, cache_time=0)
                return

        for i in __TABLAS_JSON["tablas"]:
            count = count + 1
            in_keywords = False
            for k in i["keywords"]:
                if re.search(query.lower(), k.lower()):
                    results.append(
                        InlineQueryResultCachedPhoto(
                        id=uuid4(),
                        photo_file_id=i["file_id"],
                        title=i["name"],
                        caption="@NurseJoyBot {}".format(i["name"]))
                    )
                    break

        update.inline_query.answer(results=results, cache_time=0)

    else:
        for i in range(max_range):
            results.append(InlineQueryResultCachedPhoto(
                id=uuid4(),
                photo_file_id=__TABLAS_JSON["tablas"][i]["file_id"],
                title=__TABLAS_JSON["tablas"][i]["name"],
                caption="@NurseJoyBot {}".format(__TABLAS_JSON["tablas"][i]["name"])))

        update.inline_query.answer(results=results, cache_time=0)


def reload_tablas():
    global __TABLAS_JSON

    with open('/var/local/nursejoy/tablas.json') as f:
        __TABLAS_JSON = json.load(f)


def tablas_btn(bot, update):
    logging.debug("%s %s" % (bot, update))

    query = update.callback_query
    data = query.data
    chat_id = query.message.chat.id
    news_id = int(-1001290515565)
    message_id = query.message.message_id
    
    match_new = re.match(r"tabla_new_(.*)", query.data)
    if match_new:
        text = "Ha sido añadida la tabla {0} a nuestros archivos. Solicítala ya mediante `@nursejoybot {0}`\n\n¡Un saludo y buena caza!".format(match_new.group(1))
        bot.sendMessage(
            chat_id=news_id,
            text=text,
            parse_mode=telegram.ParseMode.MARKDOWN)

    match_edit = re.match(r"tabla_edit_(.*)", query.data)
    if match_new is None and match_edit:
        text = "Ha sido modificada la tabla {0}. Recuerda que puedes solicitarla mediante `@nursejoybot {0}`\n\n¡Un saludo y buena caza!".format(match_edit.group(1))
        bot.sendMessage(
            chat_id=news_id,
            text=text,
            parse_mode=telegram.ParseMode.MARKDOWN)

    bot.edit_message_reply_markup(
        chat_id=chat_id,
        message_id=message_id,
        reply_markup=None
    )
    return
