# -*- coding: utf-8 -*-

import os
import re
import unittest
import warnings

from nursejoybot.config import get_config
from nursejoybot.supportmethods import parse_profile_image

from test import get_base_path


class TestParseProfileImageTC(unittest.TestCase):
    def setUp(self):
        get_config(os.path.join(get_base_path(), 'config/basic_config.ini'))

    @unittest.skip(
        'This test should be mocked to the infinite to avoid internal '
        'usage of 3pp libs')
    def test_stored_images(self):
        testing_imgs_dir = os.path.join(get_base_path(), 'testingimgs')
        warnings.simplefilter("ignore", ResourceWarning)

        for root, dirs, filenames in os.walk(testing_imgs_dir):
            for filename in filenames:
                m = re.match(
                    r'(Rojo|Amarillo|Azul)_([0-9]{2})_([A-Za-z0-9]{3,15})_'
                    r'([A-Za-z]{3,12})_([A-Za-z0-9]{3,12})\.jpg', filename)

                if m is None:
                    continue

                expected_color = m.group(1)
                expected_level = m.group(2)
                expected_trainer_name = m.group(3)
                expected_pokemon = m.group(4)
                expected_pokemon_name = m.group(5)

                inspect_result = parse_profile_image(
                    os.path.join(root, filename),
                    expected_pokemon,
                    inspect=False
                )

                print(inspect_result)
                self.assertIsNotNone(inspect_result, filename)
                trainer_name = inspect_result[0]
                trainer_name_grey = inspect_result[1]
                level = inspect_result[2]
                chosen_color = inspect_result[3]
                chosen_pokemon = inspect_result[4]
                pokemon_name = inspect_result[5]
                pokemon_name_grey = inspect_result[6]
                chosen_profile = inspect_result[7]

                self.assertEqual(chosen_color, expected_color)
                self.assertEqual(level, expected_level)
                self.assertEqual(chosen_pokemon, expected_pokemon)
                self.assertIsNotNone(chosen_profile)
                self.assertIn(
                    expected_trainer_name, [trainer_name, trainer_name_grey]
                )
                self.assertIn(
                    expected_pokemon_name,
                    [pokemon_name, pokemon_name_grey]
                )
