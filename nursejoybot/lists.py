import os
import re
import logging
import time

import telegram
from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup
)
from telegram.ext.dispatcher import run_async
from telegram.utils.helpers import escape_markdown

from nursejoybot.supportmethods import (
    is_admin,
    extract_update_info,
    delete_message_timed,
    send_message_timed,
    ensure_escaped,
    delete_message
)
from nursejoybot.model import (
    User,
    UserGroup,
    ValidationType,
    Team
)
from nursejoybot.storagemethods import (
    get_admin,
    get_group,
    get_user,
    exists_user_group,
    set_user_group,
    are_banned
)

REGLIST = re.compile(
    r'Apuntados:'
)

@run_async
def create_list(bot, update):
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)

    if are_banned(user_id, chat_id):
        return

    button_list = [[
        InlineKeyboardButton(text="Me apunto!", callback_data='list_join'),
        InlineKeyboardButton(text="Paso...", callback_data='list_left')
    ]]

    output = text.split(None, 1)
    out = escape_markdown(output[1]) + "\n\nApuntados:"

    bot.send_message(
        chat_id=chat_id,
        text=out,
        reply_markup=InlineKeyboardMarkup(button_list),
        disable_web_page_preview=True
    )

def list_btn(bot, update):
    query = update.callback_query
    data = query.data
    user = update.effective_user
    username = query.from_user.username
    user_id = query.from_user.id
    text = query.message.text
    chat_id = query.message.chat.id
    message_id = query.message.message_id

    if are_banned(user_id, chat_id):
        return   

    user = get_user(user_id)
    if user is None or user.validation_type == ValidationType.NONE:
        return

    if user.team is Team.BLUE:
        text_team = "💙"
    elif user.team is Team.RED:
        text_team = "❤️"
    elif user.team is Team.YELLOW:
        text_team = "💛"

    string = r'\n(.|❤️)(\d\d|\d) - @{}'.format(username)
    logging.debug("Text:{}\nString:{}".format(text, string))
    text = re.sub(string, "", text)
    if data == "list_join":
        text = text + "\n{0}{1} - @{2}".format(
            text_team,
            user.level,
            username
        )
    button_list = [[
        InlineKeyboardButton(text="Me apunto!", callback_data='list_join'),
        InlineKeyboardButton(text="Paso...", callback_data='list_left')
    ]]
    bot.edit_message_text(
        text=text,
        chat_id=chat_id,
        message_id=message_id,
        reply_markup=InlineKeyboardMarkup(button_list),
        disable_web_page_preview=True
    )
    return

@run_async
def joy_close(bot, update):
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)
    if message.reply_to_message is None or message.reply_to_message.chat.id != chat_id:
        return

    if message.reply_to_message.from_user.id != bot.id:
        return

    if are_banned(user_id, chat_id) or not is_admin(chat_id, user_id, bot):
        return

    text = message.reply_to_message.text
    if REGLIST.search(text) is None:
        return

    bot.edit_message_reply_markup(
        chat_id=chat_id,
        message_id=message.reply_to_message.message_id,
        reply_markup=None
    )

@run_async
def joy_reopen(bot, update):
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)
    if message.reply_to_message is None or message.reply_to_message.chat.id != chat_id:
        return

    if message.reply_to_message.from_user.id != bot.id:
        return

    if are_banned(user_id, chat_id) or not is_admin(chat_id, user_id, bot):
        return

    text = message.reply_to_message.text
    if REGLIST.search(text) is None:
        return

    button_list = [[
        InlineKeyboardButton(text="Me apunto!", callback_data='list_join'),
        InlineKeyboardButton(text="Paso...", callback_data='list_left')
    ]]

    bot.edit_message_reply_markup(
        chat_id=chat_id,
        message_id=message.reply_to_message.message_id,
        reply_markup=InlineKeyboardMarkup(button_list)
    )
    
@run_async
def refloat(bot, update):
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)
    if message.reply_to_message is None or message.reply_to_message.chat.id != chat_id:
        return

    if message.reply_to_message.from_user.id != bot.id:
        return

    if are_banned(user_id, chat_id) or not is_admin(chat_id, user_id, bot):
        return

    text = message.reply_to_message.text
    if REGLIST.search(text) is None:
        return

    text = message.reply_to_message.text
    button_list = [[
        InlineKeyboardButton(text="Me apunto!", callback_data='list_join'),
        InlineKeyboardButton(text="Paso...", callback_data='list_left')
    ]]

    bot.send_message(
        chat_id=chat_id,
        text=text,
        reply_markup=InlineKeyboardMarkup(button_list),
        disable_web_page_preview=True
    )
    delete_message(chat_id, message.reply_to_message.message_id, bot)
