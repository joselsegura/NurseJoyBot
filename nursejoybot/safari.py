#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
#help - Muestra la ayuda
#tabla - Muestra la tabla solicitada
#whois - Muestra info del perfil de alguien (en privado)
#maxiv - Envía una cadena con los CP de un Pokémon 100IV
#fclist - Envía un listado de los FC publicos en un grupo
#privacity - Gestiona la privacidad de tu perfil en Joy (en privado)

import os
import re
import random
import logging
from threading import Thread

import telegram
from telegram.ext.dispatcher import run_async

from nursejoybot.config import get_config

def generate_safari(bot, update):
	return


def send_pokemon(bot, chat_id, pkmn_id):
	config = get_config()
	path = os.path.expanduser(config['general']['pokemon'])
	dirs = os.listdir(path)
	output = ""

	sticker_path = '{}/pokemon_icon_{}_{}.webp'.format(path, pkmn_id, gender)
	safari_id = set_pkmn()
	button_list = [[
		InlineKeyboardButton(text="🔴 Pokeball", callback_data="catch_poke({})".format(safari_id)),
		InlineKeyboardButton(text="🔵 Superball", callback_data="catch_super({})".format(safari_id)),
		InlineKeyboardButton(text="🌕 Ultraball", callback_data="catch_ultra({})".format(safari_id))],
		[InlineKeyboardButton(text="🔮 Masterball", callback_data="catch_master({})".format(safari_id)),
		InlineKeyboardButton(text="✨ Honorball", callback_data="catch_honor({})".format(safari_id))]]
	bot.send_sticker(
		chat_id=chat_id,
		sticker=open(sticker_path, 'rb'))
	bot.sendMessage(
		chat_id=chat_id, 
		text=output, 
		parse_mode=telegram.ParseMode.MARKDOWN)


def catch_pokemon():
	return
'''

(5/(ATK+1) IV
6/(STA+1) IV
6/(DEF+1)IV)*(20/LVL)
*RAND(0.75-1.5)



Pkball - x1
super - x1,25
ultra - x1,5
master & honor - x255

baseCaptureRate': 0.20000000298023224, 
'baseFleeRate': 0.10000000149011612, 








0.89999
<--->
0.05 
0.029
0.01
'''
'''
total_rate = pkbl * catch_rate
if total_rate > pkmn_catch_rate
	return
'''
def pick_stop():
	return

def send_stop():
	return

def list_items():
	return

def list_pkmn():
	return

