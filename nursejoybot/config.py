#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV‡

import configparser
import os
from os.path import expanduser


__CONFIG = None


class ConfigurationNotLoaded(Exception):
    pass


def get_default_config_path():
    return expanduser('~/.config/nursejoy/config.ini')


def create_default_config(config_dest):
    configdir = os.path.dirname(config_dest)

    if not os.path.exists(configdir):
        os.makedirs(configdir)

    if os.path.exists(config_dest):
        os.rename(config_dest, config_dest + '~')

    with open(config_dest, "w") as f:
        f.write("""[general]
log=~/.local/share/nursejoy/log/debug.log
photos=~/.local/share/nursejoy/photos/
inspectdir=~/.local/share/nursejoy/inspectimages/
modelsdir=~/.local/share/nursejoy/models/
tablesdir=~/.local/share/nursejoy/tables/
mediadir=~/.local/share/nursejoy/media/
[database]
debug=False
host=localhost
port=3306
user=###
password=###
schema=nursejoy
# Uncomment the following line to provide a SQLAlchemy DB URL. This will
# override the rest of database configurations,except "debug"
# db-url=
[telegram]
token=###
blind_token=###
botalias=NurseJoyBot
bothelp=nursejoybot.com
helpgroup=https://t.me/joinchat/H5Oa_k-2OkvW9YoN6r0Wcg
bangroup=https://t.me/joinchat/H5Oa_kyIDoRSILCPpRqYhg
group_validation=####
user_validation=####
newsID=####
staff_id=[0]
[gorochu]
pikachu_endpoint=####
pikachu_token=####
""")

    print("Configuration file has been created on «{}».\nPlease check "
          "the configuration and change it as your wishes.".format(
              config_dest))


def get_config(config_path=None):
    global __CONFIG

    if __CONFIG is not None:
        return __CONFIG

    if not config_path:
        print('Error: no configuration path is provided')
        return None

    if not os.path.isfile(config_path):
        return None

    __CONFIG = configparser.ConfigParser()
    __CONFIG.read(config_path)

    return __CONFIG
