#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import os
import re
import logging
import random
import time
import urllib.request

import html
import telegram
from threading import Thread
from telegram.error import BadRequest
from telegram.ext import run_async
from telegram.utils.helpers import mention_markdown, mention_html, escape_markdown
from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup
)

from nursejoybot.model import (
    User,
    Team,
    Types,
    ValidationType
)
from nursejoybot.storagemethods import (
    get_user,
    has_rules,
    get_welc_pref,
    get_custom_welcome,
    get_welc_buttons,
    set_welc_preference,
    set_custom_welcome,
    are_banned
)

from nursejoybot.supportmethods import (
    build_keyboard,
    get_welcome_type,
    markdown_parser,
    escape_invalid_curly_brackets,
    extract_update_info,
    delete_message,
    is_admin
)

VALID_WELCOME_FORMATTERS = ['nombre', 'apellido', 'nombre_completo', 'usuario', 'id', 'count', 'title', 'pogo', 'mention']


def send_welcome(bot, update):
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    chat = update.effective_chat  # type: Optional[Chat]

    ENUM_FUNC_MAP = {
        Types.TEXT.value: bot.sendMessage,
        Types.BUTTON_TEXT.value: bot.sendMessage,
        Types.STICKER.value: bot.sendSticker,
        Types.DOCUMENT.value: bot.sendDocument,
        Types.PHOTO.value: bot.sendPhoto,
        Types.AUDIO.value: bot.sendAudio,
        Types.VOICE.value: bot.sendVoice,
        Types.VIDEO.value: bot.sendVideo
    }

    should_welc, cust_welcome, welc_type = get_welc_pref(chat.id)
    if should_welc:
        sent = None
        new_members = update.effective_message.new_chat_members
        for new_mem in new_members:

            # If welcome message is media, send with appropriate function
            if welc_type != Types.TEXT and welc_type != Types.BUTTON_TEXT:
                msg = ENUM_FUNC_MAP[welc_type](chat.id, cust_welcome)
                return msg
            # else, move on
            first_name = new_mem.first_name or "Null"  # edge case of empty name - occurs for some bugs.

            if cust_welcome:
                if new_mem.last_name:
                    fullname = "{} {}".format(first_name, new_mem.last_name)
                else:
                    fullname = first_name
                count = chat.get_members_count()
                mention = mention_markdown(new_mem.id, first_name)
                if new_mem.username:
                    username = "@" + escape_markdown(new_mem.username)
                else:
                    username = mention

                valid_format = escape_invalid_curly_brackets(cust_welcome, VALID_WELCOME_FORMATTERS)
                res = valid_format.format(nombre=escape_markdown(first_name),
                                          apellido=escape_markdown(new_mem.last_name or first_name),
                                          pogo=replace_trainername(new_mem.id, first_name),
                                          nombre_completo=escape_markdown(fullname), usuario=username, mention=mention,
                                          count=count, title=escape_markdown(chat.title), id=new_mem.id)
                buttons = get_welc_buttons(chat.id)
                keyb = build_keyboard(buttons)
                if has_rules(chat.id):
                    url = "t.me/NurseJoyBot?start={}".format(chat.id)
                    keyb.append([InlineKeyboardButton("Normas", url=url)])
            else:
                return

            keyboard = InlineKeyboardMarkup(keyb)

            sent = send(bot, chat_id, res, keyboard)  # type: Optional[Message]
            return sent
        
def replace_trainername(user_id, name):
    user = get_user(user_id)
    logging.info('nursejoybot:Welcome %s', user)

    if user is None or user.team is Team.NONE:
        text_team = "🖤"
    elif user.team is Team.BLUE:
        text_team = "💙"
    elif user.team is Team.RED:
        text_team = "❤️"
    elif user.team is Team.YELLOW:
        text_team = "💛"

    text_trainername = escape_markdown("{}".format(user.trainer_name)
                        if user is not None and user.trainer_name is not None
                        else "{}".format(name))
    
    text_level = ("*{}*".format(user.level)
                  if user is not None and user.level is not None
                  else "*??*")

    if user is not None and user.banned == 1:
        text_validationstatus = "⛔️"

    elif user is not None and user.validation_type in [
            ValidationType.INTERNAL, ValidationType.PIKACHU, ValidationType.GOROCHU]:
        text_validationstatus = "✅"

    else:
        text_validationstatus = "⚠️"

    if user is not None and user.admin == 1:
        text_admin = "👩‍⚕️"

    else:
        text_admin = ""

    replace_pogo = "[{0}](tg://user?id={1}) - *L*{2} {3} {4} {5}".format(
        text_trainername,
        user_id,
        text_level,
        text_team,
        text_validationstatus,
        text_admin)

    if user is not None and user.level is 0:
        return "[{0}](tg://user?id={1}) - *L??* ⚠️".format(name, user_id)

    return replace_pogo


def send(bot, chat_id, message, keyboard):
    try:
        msg = bot.send_message(
            chat_id=chat_id,
            text=message, 
            parse_mode=telegram.ParseMode.MARKDOWN,
            disable_web_page_preview=True, 
            disable_notification=True,
            reply_markup=keyboard
        )

    except IndexError:
        msg = update.effective_message.reply_text(markdown_parser("\nBip bop bip: El mensaje tiene errores de"
                                                                  "Markdown, revisalo y configuralo de nuevo."),
                                                  parse_mode=telegram.ParseMode.MARKDOWN)
    except KeyError:
        msg = update.effective_message.reply_text(markdown_parser("\nBip bop bip: El mensaje tiene errores con"
                                                                  "las llaves, revisalo y configuralo de nuevo."),
                                                  parse_mode=telegram.ParseMode.MARKDOWN)
    except BadRequest as excp:
        if excp.message == "Button_url_invalid":
            msg = update.effective_message.reply_text(markdown_parser("\nBip bop bip: El mensaje tiene errores en"
                                                                  "los botones, revisalo y configuralo de nuevo."),
                                                      parse_mode=telegram.ParseMode.MARKDOWN)
        elif excp.message == "Unsupported url protocol":
            msg = update.effective_message.reply_text(markdown_parser("\nBip bop bip: El mensaje tiene URLs"
                                                                  "invalidas para Telegram, revisalo y configuralo de nuevo."),
                                                      parse_mode=telegram.ParseMode.MARKDOWN)
        elif excp.message == "Wrong url host":
            msg = update.effective_message.reply_text(markdown_parser("\nBip bop bip: El mensaje tiene URLs"
                                                                  "erroneas, revisalo y configuralo de nuevo."),
                                                      parse_mode=telegram.ParseMode.MARKDOWN)
        else:
            msg = update.effective_message.reply_text(markdown_parser("\nBip bop bip: Hubo un error mandando el mensaje."
                                                                  "Contacta con @Pikapiing"),
                                                      parse_mode=telegram.ParseMode.MARKDOWN)
    return msg

@run_async
def test_welcome(bot, update):
    logging.debug("%s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    chat = update.effective_chat  # type: Optional[Chat]
    first_name = update.message.from_user.first_name
    last_name = update.message.from_user.last_name
    username = update.message.from_user.username

    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    ENUM_FUNC_MAP = {
        Types.TEXT.value: bot.sendMessage,
        Types.BUTTON_TEXT.value: bot.sendMessage,
        Types.STICKER.value: bot.sendSticker,
        Types.DOCUMENT.value: bot.sendDocument,
        Types.PHOTO.value: bot.sendPhoto,
        Types.AUDIO.value: bot.sendAudio,
        Types.VOICE.value: bot.sendVoice,
        Types.VIDEO.value: bot.sendVideo
    }

    should_welc, cust_welcome, welc_type = get_welc_pref(chat.id)
    if should_welc:
        # If welcome message is media, send with appropriate function
        if welc_type != Types.TEXT and welc_type != Types.BUTTON_TEXT:
            msg = ENUM_FUNC_MAP[welc_type](chat.id, cust_welcome)
            return msg
        # else, move on
        first_name = first_name or "Null"  # edge case of empty name - occurs for some bugs.

        if cust_welcome:
            if last_name:
                fullname = "{} {}".format(first_name, last_name)
            else:
                fullname = first_name
            count = chat.get_members_count()
            mention = mention_markdown(user_id, first_name)
            if username:
                username = "@" + escape_markdown(username)
            else:
                username = mention

            valid_format = escape_invalid_curly_brackets(cust_welcome, VALID_WELCOME_FORMATTERS)
            res = valid_format.format(nombre=escape_markdown(first_name),
                                    apellido=escape_markdown(last_name or first_name),
                                    pogo=replace_trainername(user_id, first_name),
                                    nombre_completo=escape_markdown(fullname), usuario=username, mention=mention,
                                    count=count, title=escape_markdown(chat.title), id=user_id)
            buttons = get_welc_buttons(chat.id)
            keyb = build_keyboard(buttons)
            if has_rules(chat.id):
                url = "t.me/NurseJoyBot?start={}".format(chat.id)
                keyb.append([InlineKeyboardButton("Normas", url=url)])
        else:
            return

        keyboard = InlineKeyboardMarkup(keyb)

        send(bot, chat_id, res, keyboard)  # type: Optional[Message]

@run_async
def raw_welcome(bot, update):
    logging.debug("%s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    chat = update.effective_chat  # type: Optional[Chat]
    first_name = update.message.from_user.first_name
    last_name = update.message.from_user.last_name
    username = update.message.from_user.username

    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    ENUM_FUNC_MAP = {
        Types.TEXT.value: bot.sendMessage,
        Types.BUTTON_TEXT.value: bot.sendMessage,
        Types.STICKER.value: bot.sendSticker,
        Types.DOCUMENT.value: bot.sendDocument,
        Types.PHOTO.value: bot.sendPhoto,
        Types.AUDIO.value: bot.sendAudio,
        Types.VOICE.value: bot.sendVoice,
        Types.VIDEO.value: bot.sendVideo
    }

    should_welc, cust_welcome, welc_type = get_welc_pref(chat.id)
    if should_welc:
        # If welcome message is media, send with appropriate function
        if welc_type != Types.TEXT and welc_type != Types.BUTTON_TEXT:
            msg = ENUM_FUNC_MAP[welc_type](chat.id, cust_welcome)
            return msg
        # else, move on
        first_name = first_name or "Null"  # edge case of empty name - occurs for some bugs.

        if cust_welcome:
            if last_name:
                fullname = "{} {}".format(first_name, last_name)
            else:
                fullname = first_name
            count = chat.get_members_count()
            mention = mention_markdown(user_id, first_name)
            if username:
                username = "@" + escape_markdown(username)
            else:
                username = mention

            valid_format = escape_invalid_curly_brackets(cust_welcome, VALID_WELCOME_FORMATTERS)
            res = valid_format.format(nombre=escape_markdown(first_name),
                                    apellido=escape_markdown(last_name or first_name),
                                    pogo=replace_trainername(user_id, first_name),
                                    nombre_completo=escape_markdown(fullname), usuario=username, mention=mention,
                                    count=count, title=escape_markdown(chat.title), id=user_id)
            buttons = get_welc_buttons(chat.id)
            keyb = build_keyboard(buttons)
            if has_rules(chat.id):
                url = "t.me/NurseJoyBot?start={}".format(chat.id)
                keyb.append([InlineKeyboardButton("Normas", url=url)])
        else:
            return

        keyboard = InlineKeyboardMarkup(keyb)

        send(bot, chat_id, res, keyboard)  # type: Optional[Message]