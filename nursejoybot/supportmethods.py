#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import os
import re
import time
import emoji
import logging
from threading import Thread
from datetime import datetime, timedelta
import json

import cv2
import numpy as np
import pytesseract
from pytz import all_timezones
from skimage.measure import compare_ssim as ssim

import telegram
from telegram.utils.helpers import mention_markdown, mention_html, escape_markdown
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.error import (
    TelegramError,
    Unauthorized,
    BadRequest,
    TimedOut,
    ChatMigrated,
    NetworkError
)

from nursejoybot.config import ConfigurationNotLoaded, get_config
from nursejoybot.db import get_session
from nursejoybot.storagemethods import (
    expire_validations,
    get_verified_providers,
    is_news_subscribed,
    get_welcome_settings,
    get_nests_settings,
    get_join_settings,
    get_nanny_settings,
    get_group_settings,
    get_particular_admin,
    get_admin,
    get_group
)
from nursejoybot.model import (
    ValidationRequiered,
    WarnLimit,
    Team,
    Types,
    News,
    NewsSubs,
    ValidationType,
    ValidationStep,
    GroupType,
    MinLevel,
    MinDays,
    MinMessages,
    Verified,
    Group,
    User,
    UserGroup,
    Validation,
    LinkedGroups,
    AdminGroups,
    SettingsNurse,
    SettingsNests,
    SettingsJoin,
    Nests,
    Welcome,
    WelcomeButtons,
    CustomCommands
)
from nursejoybot.profile_parser import Profile, ProfileParseException

validation_pokemons = ["larvitar", "whismur", "growlithe", "diglett", "marill", "houndour", "nosepass", "dunsparce"]
validation_profiles = ["model1", "model2", "model3", "model4", "model5", "model6"]
validation_names = ["Calabaza", "Puerro", "Cebolleta", "Remolacha", "Aceituna", "Pimiento", "Zanahoria", "Tomate", "Guisante", "Coliflor", "Pepino", "Berenjena", "Perejil", "Batata", "Aguacate", "Alcaparra", "Escarola", "Lechuga", "Hinojo"]
alolan_species = ['marowak','rattata','raticate','exeggutor','grimer','muk','golem','graveler','geodude','persian','meowth','dugtrio','diglett','ninetales','vulpix','sandslash','sandshrew','raichu']
badname_species = ['mr','mr.','ho','ho-oh','porygon','porygon-z','mime']
altered_species = ['deoxys','cherrim','shaymin','shellos','gastrodon','rotom','castform','giratina','arceus','burmy','wormadam']

try:
    with open('/var/local/nursejoy/gamemaster.json') as f:
        poke_json = json.load(f)
except:
    poke_json = None
try:
    with open('/var/local/nursejoy/move_translations.json') as f:
        move_json = json.load(f)
except:
    move_json = None
try:
    with open('/var/local/nursejoy/pkdx_translations.json') as f:
        trsl_json = json.load(f)
except:
    trsl_json = None

REGPKDX = re.compile(
    r'^(SPAWN_|)V([0-9]{4})_(POKEMON|MOVE)_([a-zA-Z0-9\- ]{3,10})(|.*)$',
    flags=re.IGNORECASE
)
#--------------Welcome--------------#
MATCH_MD = re.compile(r'\*(.*?)\*|'
                      r'_(.*?)_|'
                      r'`(.*?)`|'
                      r'(?<!\\)(\[.*?\])(\(.*?\))|'
                      r'(?P<esc>[*_`\[])')

LINK_REGEX = re.compile(r'(?<!\\)\[.+?\]\((.*?)\)')

BTN_URL_REGEX = re.compile(r"(\[([^\[]+?)\]\(buttonurl:(?:/{0,2})(.+?)(:same)?\))")

#----------------Pkdx---------------#
def get_gamemaster_data(pkmn, form):
    for k in poke_json["itemTemplates"]:
        match = REGPKDX.match(k["templateId"])
        if match is not None:
            logging.debug("%s", match)
        if match is not None and match.group(3) == "POKEMON" and ((match.group(2) == pkmn) or (match.group(4).lower() == pkmn.lower())):
            logging.debug("MATCH:%s", match)
            if form is not None and match.group(5) == form:
                if match.group(1) == "SPAWN_":
                    spawn_info = k
                    logging.debug("%s", spawn_info)
                    if pkmn_info is not None:
                        return match.group(2), pkmn_info, spawn_info
                else:
                    pkmn_info = k
                    logging.debug("%s", pkmn_info)
                    if spawn_info is not None:
                        return match.group(2), pkmn_info, spawn_info
            elif form is None:
                if match.group(1) == "SPAWN_":
                    spawn_info = k
                    logging.debug("%s", spawn_info)
                    if pkmn_info is not None:
                        return match.group(2), pkmn_info, spawn_info
                else:
                    pkmn_info = k
                    logging.debug("%s", pkmn_info)
                    if spawn_info is not None:
                        return match.group(2), pkmn_info, spawn_info

    return None, None, None

def get_move_data(move_name):
    for k in poke_json["itemTemplates"]:
        if k["moveSettings"]["movementId"] == move_name:
            match = REGPKDX.match(k["templateId"])
            return match.group(2), k

    return None

def get_trsl_data(pkmn_id, lang):
    for k in trsl_json:
        if k["Key"] == "pokemon_name_{}".format(pkmn_id):
            if lang == "en_EN":
                name = k["English"]
            if lang == "ja_JP":
                name = k["Japanese"]
            if lang == "fr_FR":
                name = k["French"]
            if lang == "es_ES":
                name = k["Spanish"]
            if lang == "de_DE":
                name = k["German"]
            if lang == "it_IT":
                name = k["Italian"]
            if lang == "ko_KR":
                name = k["Korean"]
            if lang == "zh_CN":
                name = k["ChineseTraditional"]
            if lang == "pt_PT":
                name = k["BrazilianPortuguese"]
        elif k["Key"] == "pokemon_desc_{}".format(pkmn_id):
            if lang == "en_EN":
                desc = k["English"]
            if lang == "ja_JP":
                desc = k["Japanese"]
            if lang == "fr_FR":
                desc = k["French"]
            if lang == "es_ES":
                desc = k["Spanish"]
            if lang == "de_DE":
                desc = k["German"]
            if lang == "it_IT":
                desc = k["Italian"]
            if lang == "ko_KR":
                desc = k["Korean"]
            if lang == "zh_CN":
                desc = k["ChineseTraditional"]
            if lang == "pt_PT":
                desc = k["BrazilianPortuguese"]
        elif k["Key"] == "pokemon_category_{}".format(pkmn_id):
            if lang == "en_EN":
                category = k["English"]
            if lang == "ja_JP":
                category = k["Japanese"]
            if lang == "fr_FR":
                category = k["French"]
            if lang == "es_ES":
                category = k["Spanish"]
            if lang == "de_DE":
                category = k["German"]
            if lang == "it_IT":
                category = k["Italian"]
            if lang == "ko_KR":
                category = k["Korean"]
            if lang == "zh_CN":
                category = k["ChineseTraditional"]
            if lang == "pt_PT":
                category = k["BrazilianPortuguese"]
            return name, desc, category

    return None, None, None

def get_move_trsl(move_id, lang):
    for k in move_json:
        if k["Key"] == "move_name_{}".format(move_id):
            if lang == "en_EN":
                return k["English"]
            if lang == "ja_JP":
                return k["Japanese"]
            if lang == "fr_FR":
                return k["French"]
            if lang == "es_ES":
                return k["Spanish"]
            if lang == "de_DE":
                return k["German"]
            if lang == "it_IT":
                return k["Italian"]
            if lang == "ko_KR":
                return k["Korean"]
            if lang == "zh_CN":
                return k["ChineseTraditional"]
            if lang == "pt_PT":
                return k["BrazilianPortuguese"]

    return None

def get_pkmn_type_emoji(pkmn_type):
    if pkmn_type == "POKEMON_TYPE_BUG":
        return "🐛"
    if pkmn_type == "POKEMON_TYPE_ICE":
        return "❄️"
    if pkmn_type == "POKEMON_TYPE_ROCK":
        return "⛰"
    if pkmn_type == "POKEMON_TYPE_FIRE":
        return "🔥"
    if pkmn_type == "POKEMON_TYPE_DARK":
        return "🌑"
    if pkmn_type == "POKEMON_TYPE_GHOST":
        return "👻"
    if pkmn_type == "POKEMON_TYPE_STEEL":
        return "⛓"
    if pkmn_type == "POKEMON_TYPE_GRASS":
        return "🌱"
    if pkmn_type == "POKEMON_TYPE_FAIRY":
        return "🧚‍"
    if pkmn_type == "POKEMON_TYPE_WATER":
        return "💧"
    if pkmn_type == "POKEMON_TYPE_FLYING":
        return "🕊"
    if pkmn_type == "POKEMON_TYPE_DRAGON":
        return "🐉"
    if pkmn_type == "POKEMON_TYPE_NORMAL":
        return "⚪️"
    if pkmn_type == "POKEMON_TYPE_POISON":
        return "🦠"
    if pkmn_type == "POKEMON_TYPE_GROUND":
        return "🌍"
    if pkmn_type == "POKEMON_TYPE_PSYCHIC":
        return "🔮"
    if pkmn_type == "POKEMON_TYPE_ELECTRIC":
        return "⚡️"
    if pkmn_type == "POKEMON_TYPE_FIGHTING":
        return "🥊"

def get_weather_emoji(weather):
    if pkmn_type == "WEATHER_AFFINITY_CLEAR":
        return "☀️"
    if pkmn_type == "WEATHER_AFFINITY_FOG":
        return "🌫"
    if pkmn_type == "WEATHER_AFFINITY_OVERCAST":
        return "☁️"
    if pkmn_type == "WEATHER_AFFINITY_PARTLY_CLOUDY":
        return "🌤"
    if pkmn_type == "WEATHER_AFFINITY_RAINY":
        return "🌧"
    if pkmn_type == "WEATHER_AFFINITY_SNOW":
        return "❄️"
    if pkmn_type == "WEATHER_AFFINITY_WINDY":
        return "⛓"

def get_weather_from_type(pkmn_type):
    for k in poke_json["itemTemplates"]:
        if re.match(r"WEATHER_AFFINITY_\((.+?)\)", k["templateId"]):
            if pkmn_type in k["weatherAffinities"]["pokemonType"]:
                return k["templateId"]
    return None

def parse_pokemon(pkmn, att):
    form = None
    if pkmn.lower() in alolan_species:
        if att.lower() in ['a', 'alola', 'alolan']:
            form = '_ALOLA'

    if pkmn.lower() in badname_species:
        if pkmn.lower() in ['mr', 'mr.'] and att.lower() == 'mime':
            form = '_MIME'
        elif pkmn.lower() == 'mime' and att.lower() == 'jr':
            form = '_JR'
        elif (pkmn.lower() == 'ho' and att.lower() == 'oh') or pkmn.lower() == 'ho-oh':
            pkmn = 'HO'
            form = '_OH'
        elif (pkmn.lower() == 'porygon' and att.lower() == 'z') or pkmn.lower() == 'porygon-z':
            pkmn = 'PORYGON'
            form = '_Z'

    if pkmn.lower() in altered_species:
        if pkmn.lower() == 'deoxys':
            if att.lower() in ['atk', 'ataque', 'attack']:
                form = '_ATTACK'
            elif att.lower() in ['def', 'defense', 'defensa']:
                form = '_DEFENSE'
            elif att.lower() in ['nor', 'normal']:
                form = '_NORMAL'
            elif att.lower() in ['vel', 'speed', 'velocidad']:
                form = '_SPEED'
        if pkmn.lower() == 'cherrim':
            if att.lower() in ['sunny', 'sun', 'soleado', 'sol']:
                form = '_SUNNY'
        if pkmn.lower() == 'shaymin':
            if att.lower() in ['tierra', 'land']:
                form = '_LAND'
            elif att.lower() in ['sky', 'cielo']:
                form = '_SKY'
        if pkmn.lower() in ['shellos','gastrodon']:
            if att.lower() in ['east', 'este']:
                form = '_EAST_SEA'
            elif att.lower() in ['west', 'oeste']:
                form = '_WEST_SEA'
        if pkmn.lower() == 'rotom':
            if att.lower() in ['fan', 'ventilador']:
                form = '_FAN'
            elif att.lower() in ['frost', 'frio']:
                form = '_FROST'
            elif att.lower() in ['heat', 'calor']:
                form = '_HEAT'
            elif att.lower() in ['mow', 'corte']:
                form = '_MOW'
            elif att.lower() in ['normal', 'nor']:
                form = '_NORMAL'
            elif att.lower() in ['wash', 'lavado']:
                form = '_WASH'
        if pkmn.lower() in ['burmy','wormadam']:
            if att.lower() in ['plant', 'planta']:
                form = '_PLANT'
            elif att.lower() in ['sandy', 'arena']:
                form = '_SANDY'
            elif att.lower() in ['trash', 'basura']:
                form = '_TRASH'
        if pkmn.lower() == 'castform':
            if att.lower() in ['normal', 'nor']:
                form = '_NORMAL'
            elif att.lower() in ['rainy', 'lluvia', 'luvioso']:
                form = '_RAINY'
            elif att.lower() in ['snow','snowy','nieve']:
                form = '_SNOWY'
            elif att.lower() in ['soleado', 'sol', 'sun', 'sunny']:
                form = '_SUNNY'
        if pkmn.lower() == 'giratina':
            if att.lower() in ['aletered', 'alterado', 'alterada']:
                form = '_ALTERED'
            elif att.lower() in ['origin', 'origen']:
                form = '_ORIGIN'
        if pkmn.lower() == 'arceus':
            if att.lower() in ['bug', 'bicho']:
                form = '_BUG'
            elif att.lower() in ['dark', 'siniestro']:
                form = '_DARK'
            elif att.lower() in ['dragon', 'dragón']:
                form = '_DRAGON'
            elif att.lower() in ['electric', 'eléctrico', 'electrico']:
                form = '_ELECTRIC'
            elif att.lower() in ['hada', 'fairy']:
                form = '_FAIRY'
            elif att.lower() in ['fighting', 'lucha']:
                form = '_FIGHTING'
            elif att.lower() in ['fuego', 'fire']:
                form = '_FIRE'
            elif att.lower() in ['volador', 'flying']:
                form = '_FLYING'
            elif att.lower() in ['ghost', 'fantasma']:
                form = '_GHOST'
            elif att.lower() in ['grass', 'planta']:
                form = '_GRASS'
            elif att.lower() in ['tierra', 'ground']:
                form = '_GROUND'
            elif att.lower() in ['ice', 'hielo']:
                form = '_ICE'
            elif att.lower() in ['normal', 'nor']:
                form = '_NORMAL'
            elif att.lower() in ['poison', 'veneno']:
                form = '_POISON'
            elif att.lower() in ['psychic', 'psíquico', 'psiquico']:
                form = '_PSYCHIC'
            elif att.lower() in ['roca', 'rock']:
                form = '_ROCK'
            elif att.lower() in ['steel', 'acero']:
                form = '_STEEL'
            elif att.lower() in ['water', 'agua']:
                form = '_WATER'

    return pkmn, form

#----------------Pkdx---------------#
def build_keyboard(buttons):
    keyb = []
    for btn in buttons:
        if btn.same_line and keyb:
            keyb[-1].append(InlineKeyboardButton(btn.name, url=btn.url))
        else:
            keyb.append([InlineKeyboardButton(btn.name, url=btn.url)])

    return keyb


def get_welcome_type(msg, cmd=False):
    data_type = None
    content = None
    text = ""
    offset = 0

    if cmd and msg.text is not None:
        msg.text = "keyword " + msg.text
        offset = 8
    if msg.text is None:
        msg.text = "keyword "
        offset = 0
        
    args = msg.text.split(None, 1)  # use python's maxsplit to separate cmd and args

    buttons = []
    # determine what the contents of the filter are - text, image, sticker, etc
    if msg.reply_to_message:
        message = msg.reply_to_message
    else:
        message = msg
    
    if len(args) >= 2:
        offset = len(args[1]) - len(msg.text) - offset  # set correct offset relative to command + notename
        text, buttons = button_markdown_parser(args[1], entities=msg.parse_entities(), offset=offset)
        if buttons:
            data_type = Types.BUTTON_TEXT
        else:
            data_type = Types.TEXT

    elif message and message.sticker:
        content = message.sticker.file_id
        text = message.text
        data_type = Types.STICKER

    elif message and message.document:
        content = message.document.file_id
        text = message.text
        data_type = Types.DOCUMENT

    elif message and message.photo:
        content = message.photo[-1].file_id  # last elem = best quality
        text = message.text
        data_type = Types.PHOTO

    elif message and message.audio:
        content = message.audio.file_id
        text = message.text
        data_type = Types.AUDIO

    elif message and message.voice:
        content = message.voice.file_id
        text = message.text
        data_type = Types.VOICE

    elif message and message.video:
        content = message.video.file_id
        text = message.text
        data_type = Types.VIDEO
        
    return text, data_type, content, buttons


def button_markdown_parser(txt, entities=None, offset=0):
    markdown_note = markdown_parser(txt, entities, offset)
    prev = 0
    note_data = ""
    buttons = []
    for match in BTN_URL_REGEX.finditer(markdown_note):
        # Check if btnurl is escaped
        n_escapes = 0
        to_check = match.start(1) - 1
        while to_check > 0 and markdown_note[to_check] == "\\":
            n_escapes += 1
            to_check -= 1

        # if even, not escaped -> create button
        if n_escapes % 2 == 0:
            # create a thruple with button label, url, and newline status
            buttons.append((match.group(2), match.group(3), bool(match.group(4))))
            note_data += markdown_note[prev:match.start(1)]
            prev = match.end(1)
        # if odd, escaped -> move along
        else:
            note_data += markdown_note[prev:to_check]
            prev = match.start(1) - 1
    else:
        note_data += markdown_note[prev:]

    return note_data, buttons


def _calc_emoji_offset(to_calc):
    # Get all emoji in text.
    emoticons = emoji.get_emoji_regexp().finditer(to_calc)
    # Check the utf16 length of the emoji to determine the offset it caused.
    # Normal, 1 character emoji don't affect; hence sub 1.
    # special, eg with two emoji characters (eg face, and skin col) will have length 2, so by subbing one we
    # know we'll get one extra offset,
    return sum(len(e.group(0).encode('utf-16-le')) // 2 - 1 for e in emoticons)


def markdown_parser(txt, entities= None, offset=0):
    """
    Parse a string, escaping all invalid markdown entities.

    Escapes URL's so as to avoid URL mangling.
    Re-adds any telegram code entities obtained from the entities object.

    :param txt: text to parse
    :param entities: dict of message entities in text
    :param offset: message offset - command and notename length
    :return: valid markdown string
    """
    if not entities:
        entities = {}
    if not txt:
        return ""

    prev = 0
    res = ""
    # Loop over all message entities, and:
    # reinsert code
    # escape free-standing urls
    for ent, ent_text in entities.items():
        if ent.offset < -offset:
            continue

        start = ent.offset + offset  # start of entity
        end = ent.offset + offset + ent.length - 1  # end of entity

        # we only care about code, url, text links
        if ent.type in ("code", "url", "text_link"):
            # count emoji to switch counter
            count = _calc_emoji_offset(txt[:start])
            start -= count
            end -= count

            # URL handling -> do not escape if in [](), escape otherwise.
            if ent.type == "url":
                if any(match.start(1) <= start and end <= match.end(1) for match in LINK_REGEX.finditer(txt)):
                    continue
                # else, check the escapes between the prev and last and forcefully escape the url to avoid mangling
                else:
                    # TODO: investigate possible offset bug when lots of emoji are present
                    res += _selective_escape(txt[prev:start] or "") + escape_markdown(ent_text)

            # code handling
            elif ent.type == "code":
                res += _selective_escape(txt[prev:start]) + '`' + ent_text + '`'

            # handle markdown/html links
            elif ent.type == "text_link":
                res += _selective_escape(txt[prev:start]) + "[{}]({})".format(ent_text, ent.url)

            end += 1

        # anything else
        else:
            continue

        prev = end

    res += _selective_escape(txt[prev:])  # add the rest of the text
    return res


def _selective_escape(to_parse):
    """
    Escape all invalid markdown

    :param to_parse: text to escape
    :return: valid markdown string
    """
    offset = 0  # offset to be used as adding a \ character causes the string to shift
    for match in MATCH_MD.finditer(to_parse):
        if match.group('esc'):
            ent_start = match.start()
            to_parse = to_parse[:ent_start + offset] + '\\' + to_parse[ent_start + offset:]
            offset += 1
    return to_parse


def escape_invalid_curly_brackets(text, valids):
    new_text = ""
    idx = 0
    while idx < len(text):
        if text[idx] == "{":
            if idx + 1 < len(text) and text[idx + 1] == "{":
                idx += 2
                new_text += "{{{{"
                continue
            else:
                success = False
                for v in valids:
                    if text[idx:].startswith('{' + v + '}'):
                        success = True
                        break
                if success:
                    new_text += text[idx: idx + len(v) + 2]
                    idx += len(v) + 2
                    continue
                else:
                    new_text += "{{"

        elif text[idx] == "}":
            if idx + 1 < len(text) and text[idx + 1] == "}":
                idx += 2
                new_text += "}}}}"
                continue
            else:
                new_text += "}}"

        else:
            new_text += text[idx]
        idx += 1

    return new_text


#--------------Welcome--------------#
def cleanup(signum, frame):
    logging.info("Closing bot!")
    exit(0)


def is_admin(chat_id, user_id, bot):
    is_admin = False
    for admin in bot.get_chat_administrators(chat_id):
        if user_id == admin.user.id:
            is_admin = True

    return is_admin


def is_staff(user_id):
    is_staff = False
    config = get_config()
    for staff in config["telegram"]["staff_id"]:
        if user_id == staff:
            is_staff = True

    return is_staff


def extract_update_info(update):
    logging.debug("supportmethods:extract_update_info")
    try:
        message = update.message
    except:
        message = update.channel_post
    if message is None:
        message = update.channel_post
    text = message.text
    try:
        user_id = message.from_user.id
    except:
        user_id = None
    chat_id = message.chat.id
    chat_type = message.chat.type
    return (chat_id, chat_type, user_id, text, message)


def send_message_timed(chat_id, text, sleep_time, bot):
    logging.debug("supportmethods:send_message_timed: %s %s %s %s" % (chat_id, text, sleep_time, bot))
    time.sleep(sleep_time)
    bot.sendMessage(chat_id=chat_id, text=text, parse_mode=telegram.ParseMode.MARKDOWN)


def delete_message_timed(chat_id, message_id, sleep_time, bot):
    time.sleep(sleep_time)
    delete_message(chat_id, message_id, bot)


def delete_message(chat_id, message_id, bot):
    try:
        bot.deleteMessage(chat_id=chat_id, message_id=message_id)
        return True

    except:
        return False


def ensure_escaped(username):
    if username.find("_") != -1 and username.find("\\_") == -1:
        username = username.replace("_","\\_")
    return username


def update_validations_status(bot):
    '''
    Method executed in a separated thread to check when a validation process
    has expired and modify the database accordingly
    '''

    from nursejoybot.storagemethods import expire_validations

    logging.debug("supportmethods:update_validations_status")
    validations = expire_validations(datetime.now() + timedelta(hours=-6))

    for v in validations:
        logging.debug(v)
        logging.debug(
            "Sending notification for validation ID %s, user ID %s",
            v.id, v.user_id
        )
        try:
            bot.sendMessage(
                chat_id=v.user_id,
                text=("⚠ El proceso de validación pendiente ha caducado porque"
                      " han pasado 6 horas desde que empezó. Si quieres "
                      "validarte, debes volver a empezar el proceso."),
                parse_mode=telegram.ParseMode.MARKDOWN
            )

        except Exception as e:
            logging.debug(
                "supportmethods:update_validations_status error: %s", str(e)
            )


def callback_update_validations_status(bot, job):
    Thread(target=update_validations_status, args=(bot,)).start()


def error_callback(bot, update, error):
    try:
        raise error
    except Unauthorized:
        logging.debug("TELEGRAM ERROR: Unauthorized - %s" % error)
    except BadRequest:
        logging.debug("TELEGRAM ERROR: Bad Request - %s" % error)
    except TimedOut:
        logging.debug("TELEGRAM ERROR: Slow connection problem - %s" % error)
    except NetworkError:
        logging.debug("TELEGRAM ERROR: Other connection problems - %s" % error)
    except ChatMigrated as e:
        logging.debug("TELEGRAM ERROR: Chat ID migrated?! - %s" % error)
    except TelegramError:
        logging.debug("TELEGRAM ERROR: Other error - %s" % error)
    except:
        logging.debug("TELEGRAM ERROR: Unknown - %s" % error)


#tofix
def get_settings_keyboard(chat_id, keyboard="main"):
    logging.debug("supportmethods:get_settings_keyboard")

    #1.MAIN SETTINGS
    if keyboard == "main":
        settings_keyboard = [
            [InlineKeyboardButton("Nidos »", callback_data='settings_goto_nests')],
            [InlineKeyboardButton("⚠️ Safari »", callback_data='settings_goto_safari')],
            [InlineKeyboardButton("Noticias »", callback_data='settings_goto_news')],
            [InlineKeyboardButton("Bienvenida »", callback_data='settings_goto_welcome')],
            [InlineKeyboardButton("Modo enfermera »", callback_data='settings_goto_nanny')],
            [InlineKeyboardButton("Ajustes generales »", callback_data='settings_goto_general')],
            [InlineKeyboardButton("Ajustes de entrada (Joy) »", callback_data='settings_goto_join_joy')],
            [InlineKeyboardButton("Ajustes de entrada (Pika) »", callback_data='settings_goto_join_pikachu')],
            [InlineKeyboardButton("Ajustes de administración »", callback_data='settings_goto_ladmin')],
            [InlineKeyboardButton("Terminado", callback_data='settings_done')]
        ]
    
    #2.GROUP SETTINGS
    elif keyboard == "general":
        group = get_group_settings(chat_id)
        if group.pvp == 1:
            pvp_text = "✅ PvP"
        else:
            pvp_text = "▪️ PvP"
        if group.jokes == 1:
            jokes_text = "✅ Chistes"
        else:
            jokes_text = "▪️ Chistes"
        if group.games == 1:
            games_text = "✅ Juegos"
        else:
            games_text = "▪️ Juegos"
        if group.hard == 1:
            hard_text = "✅ Ban (Warns)"
        else:
            hard_text = "▪️ Kick (Warns)"
        if group.trades == 1:
            trades_text = "✅ Intercambios"
        else:
            trades_text = "▪️ Intercambios"
        if group.notes == 1:
            notes_text = "✅ Recordatorios"
        else:
            notes_text = "▪️ Recordatorios"
        if group.reply_on_group == 1:
            reply_on_group_text = "✅ Respuestas en el grupo"
        else:
            reply_on_group_text = "▪️ Respuestas al privado"
        if group.command == 1:
            command_text = "✅ Comandos personalizados"
        else:
            command_text = "▪️ Comandos personalizados"
        if group.warn is WarnLimit.SO_EXTRICT:
            warn_text = "Limite de warns: 3"
        elif group.warn is WarnLimit.EXTRICT:
            warn_text = "Limite de warns: 5"
        elif group.warn is WarnLimit.LOW_PERMISIVE:
            warn_text = "Limite de warns: 10"
        elif group.warn is WarnLimit.MED_PERMISIVE:
            warn_text = "Limite de warns: 25"
        elif group.warn is WarnLimit.HIGH_PERMISIVE:
            warn_text = "Limite de warns: 50"
        elif group.warn is WarnLimit.SO_TOLERANT:
            warn_text = "Limite de warns: 100"

        settings_keyboard = [
            #[InlineKeyboardButton(pvp_text, callback_data='settings_general_pvp')],
            [InlineKeyboardButton(jokes_text, callback_data='settings_general_jokes')],
            [InlineKeyboardButton(games_text, callback_data='settings_general_games')],
            [InlineKeyboardButton(hard_text, callback_data='settings_general_hard')],
            #[InlineKeyboardButton(trades_text, callback_data='settings_general_trades')],
            #[InlineKeyboardButton(notes_text, callback_data='settings_general_notes')],
            [InlineKeyboardButton(reply_on_group_text, callback_data='settings_general_reply')],
            #[InlineKeyboardButton(command_text, callback_data='settings_general_cmd')],
            [InlineKeyboardButton(warn_text, callback_data='settings_general_warn')],
            [InlineKeyboardButton("« Menú principal", callback_data='settings_goto_main')]
        ]  
    
    #3.JOIN SETTINGS
    elif keyboard == "join_joy":
        join = get_join_settings(chat_id)
        if join.validationrequired is ValidationRequiered.NO_VALIDATION:
            validationrequired_text = "▪️ Grupo abierto"

        elif join.validationrequired is ValidationRequiered.VALIDATION:
            validationrequired_text = "✅ Validación obligatoria"

        elif join.validationrequired is ValidationRequiered.RED_EXCLUSIVE:
            validationrequired_text = "🔴 Grupo exclusivo Rojo"

        elif join.validationrequired is ValidationRequiered.BLUE_EXCLUSIVE:
            validationrequired_text = "🔵 Grupo exclusivo Azul"

        elif join.validationrequired is ValidationRequiered.YELLOW_EXCLUSIVE:
            validationrequired_text = "🌕 Grupo exclusivo Amarillo"

        if join.levelrequired is not MinLevel.NO_LEVEL:
            level_text = "✅ Nivel mínimo {}".format(join.levelrequired.value)
        else:
            level_text = "▪️ Sin nivel mínimo"

        if join.joy is True:
            joy_text = "✅ No registrados"
        else:
            joy_text = "▪️ No registrados"
        if join.mute is True:
            mute_text = "✅ Expulsiones silenciosas"
        else:
            mute_text = "▪️ Expulsiones notificadas"
        if join.silence is True:
            silence_text = "✅ Borrar -> entró al grupo"
        else:
            silence_text = "▪️ Borrar -> entró al grupo"


        settings_keyboard = [
            [InlineKeyboardButton(joy_text, callback_data='settings_joy_joy')],
            [InlineKeyboardButton(mute_text, callback_data='settings_joy_mute')],
            [InlineKeyboardButton(silence_text, callback_data='settings_joy_silence')],
            [InlineKeyboardButton(level_text, callback_data='settings_joy_level')],
            [InlineKeyboardButton(validationrequired_text, callback_data='settings_joy_val')],
            [InlineKeyboardButton("« Menú principal", callback_data='settings_goto_main')]
        ]

    elif keyboard == "join_pikachu":
        join = get_join_settings(chat_id)
        if join.pikachu is True:
            pikachu_text = "✅ No registrados"
        else:
            pikachu_text = "▪️ No registrados"
        if join.valpikachu is True:
            valpikachu_text = "✅ Validación obligatoria"
        else:
            valpikachu_text = "▪️ Validación obligatoria"
        if join.mute_pikachu is True:
            mutepikachu_text = "✅ Silenciar no registrados"
        else:
            mutepikachu_text = "▪️ Silenciar no registrados"


        settings_keyboard = [
            [InlineKeyboardButton(pikachu_text, callback_data='settings_pika_pika')],
            [InlineKeyboardButton(valpikachu_text, callback_data='settings_pika_valpika')],
            [InlineKeyboardButton(mutepikachu_text, callback_data='settings_pika_pikamute')],
            [InlineKeyboardButton("« Menú principal", callback_data='settings_goto_main')]
        ]
    
    #4.NESTS SETTINGS
    elif keyboard == "nests":
        snests = get_nests_settings(chat_id)
        if snests.nests == 1:
            nests_text = "✅ Nidos"
        else:
            nests_text = "▪️ Nidos"
        if snests.alerts == 1:
            alerts_text = "✅ Avistamientos"
        else:
            alerts_text = "▪️ Avistamientos"
        if snests.rankings == 1:
            rankings_text = "✅ Rankings"
        else:
            rankings_text = "▪️ Rankings"
        if snests.by_text == 1:
            by_text_text = "✅ Mediante texto"
        else:
            by_text_text = "▪️ Mediante texto"
        if snests.by_location == 1:
            by_location_text = "✅ Mediante ubicación"
        else:
            by_location_text = "▪️ Mediante ubicación"
        if snests.min_days is not MinDays.NONE:
            days_text = "✅ Mínimo de días {}".format(snests.min_days.value)
        else:
            days_text = "▪️ Mínimo de días 0"
        if snests.min_messages is not MinMessages.NONE:
            messages_text = "✅ Mínimo de mensajes {}".format(snests.min_messages.value)
        else:
            messages_text = "▪️ Mínimo de mensajes 0"

        settings_keyboard = [
            [InlineKeyboardButton(nests_text, callback_data='settings_nests_nests')],
            #[InlineKeyboardButton(alerts_text, callback_data='settings_nests_alerts')],
            #[InlineKeyboardButton(rankings_text, callback_data='settings_nests_rankings')],
            #[InlineKeyboardButton(by_text_text, callback_data='settings_nests_text')],
            #[InlineKeyboardButton(by_location_text, callback_data='settings_nests_location')],
            [InlineKeyboardButton(days_text, callback_data='settings_nests_mindays')],
            [InlineKeyboardButton(messages_text, callback_data='settings_nests_minmessages')],
            [InlineKeyboardButton("« Menú principal", callback_data='settings_goto_main')]
        ]

    #5.NEWS SETTINGS
    elif keyboard == "news":
        providers = get_verified_providers()
        settings_keyboard = []
        for k in providers:
            if is_news_subscribed(chat_id, k.id):
                status = "✅ @"
            else:
                status = "▪️ @"
            text = status + k.alias
            settings_keyboard.append(
                [InlineKeyboardButton(
                    text,
                    callback_data='settings_news_{}'.format(k.id))
                ]
            )

        settings_keyboard.append(
            [InlineKeyboardButton(
                "« Menú principal",
                callback_data='settings_goto_main')
            ]
        )

    #6.WELCOME SETTINGS
    elif keyboard == "welcome":
        welcome = get_welcome_settings(chat_id)
        if welcome.should_welcome == 1:
            welcome_text = "✅ Bienvenida"
        else:
            welcome_text = "▪️ Bienvenida"
        settings_keyboard = [
            [InlineKeyboardButton(welcome_text, callback_data='settings_welcome_welcome')],
            [InlineKeyboardButton("« Menú principal", callback_data='settings_goto_main')]
        ]
    
    #7.NANNY SETTINGS
    elif keyboard == "nanny":
        nanny = get_nanny_settings(chat_id)
        if nanny.voice == 1:
            voice_text = "✅ Audio y Voz"
        else:
            voice_text = "▪️ Audio y Voz"
        if nanny.command == 1:
            comandos_text = "✅ Comandos"
        else:
            comandos_text = "▪️ Comandos"
        if nanny.contact == 1:
            contact_text = "✅ Contactos"
        else:
            contact_text = "▪️ Contactos"
        if nanny.animation == 1:
            animation_text = "✅ GIFs y Documentos"
        else:
            animation_text = "▪️ GIFs y Documentos"
        if nanny.photo == 1:
            photo_text = "✅ Imagenes"
        else:
            photo_text = "▪️ Imagenes"
        if nanny.games == 1:
            games_text = "✅ Juegos"
        else:
            games_text = "▪️ Juegos"
        if nanny.text == 1:
            text_text = "✅ Mensajes"
        else:
            text_text = "▪️ Mensajes"
        if nanny.sticker == 1:
            sticker_text = "✅ Stickers"
        else:
            sticker_text = "▪️ Stickers"
        if nanny.location == 1:
            location_text = "✅ Ubicaciones"
        else:
            location_text = "▪️ Ubicaciones"
        if nanny.urls == 1:
            url_text = "✅ URLs"
        else:
            url_text = "▪️ URLs"
        if nanny.video == 1:
            video_text = "✅ Video"
        else:
            video_text = "▪️ Video"
        if nanny.warn == 1:
            warn_text = "✅ Warns"
        else:
            warn_text = "▪️ Warns"
        if nanny.admin_too == 1:
            admin_too_text = "️✅ Mensajes de administradores"
        else:
            admin_too_text = "️️️▪️ Mensajes de administradores"

        settings_keyboard = [
            [InlineKeyboardButton(voice_text, callback_data='settings_nanny_voice')],
            [InlineKeyboardButton(comandos_text, callback_data='settings_nanny_command')],
            [InlineKeyboardButton(contact_text, callback_data='settings_nanny_contact')],
            [InlineKeyboardButton(animation_text, callback_data='settings_nanny_animation')],
            [InlineKeyboardButton(photo_text, callback_data='settings_nanny_photo')],
            [InlineKeyboardButton(games_text, callback_data='settings_nanny_games')],
            [InlineKeyboardButton(text_text, callback_data='settings_nanny_text')],
            [InlineKeyboardButton(sticker_text, callback_data='settings_nanny_sticker')],
            [InlineKeyboardButton(location_text, callback_data='settings_nanny_location')],
            [InlineKeyboardButton(url_text, callback_data='settings_nanny_url')],
            [InlineKeyboardButton(video_text, callback_data='settings_nanny_video')],
            [InlineKeyboardButton(warn_text, callback_data='settings_nanny_warn')],
            [InlineKeyboardButton(admin_too_text, callback_data='settings_nanny_admin_too')],
            [InlineKeyboardButton("« Menú principal", callback_data='settings_goto_main')]
        ] 

    #7.SAFARI
    elif keyboard == "safari":
        safari = get_safari_settings(chat_id)        
        if safari.pokegram == 1:
            pokegram = "✅ Safari"
        else:
            pokegram = "▪️ Safari"
        if safari.allow_spawn == 1:
            allow_spawn = "✅ Pokémon"
        else:
            comandos_text = "▪️ Pokémon"
        if safari.allow_stops == 1:
            allow_stops = "✅ Pokeparadas"
        else:
            allow_stops = "▪️ Pokeparadas"
        if safari.allow_battles == 1:
            allow_battles = "✅ Batallas"
        else:
            allow_battles = "▪️ Batallas"
        if safari.magikarp_jump == 1:
            magikarp_jump = "✅ Magikarp Jump"
        else:
            magikarp_jump = "▪️ Magikarp Jump"

        settings_keyboard = [
            [InlineKeyboardButton(pokegram, callback_data='settings_safari_pokegram')],
            [InlineKeyboardButton(allow_spawn, callback_data='settings_safari_spawn')],
            [InlineKeyboardButton(allow_stops, callback_data='settings_safari_stops')],
            #[InlineKeyboardButton(allow_battles, callback_data='settings_safari_battles')],
            #[InlineKeyboardButton(magikarp_jump, callback_data='settings_safari_karp')],
            [InlineKeyboardButton("« Menú principal", callback_data='settings_goto_main')]
        ] 

    #8. LOCAL ADMIN
    elif keyboard == "ladmin":
        adgroup = get_particular_admin(chat_id)
        if adgroup.admin == 1:
            admin_text = "✅ @admin"
        else:
            admin_text = "▪️ @admin"
        if adgroup.welcome == 1:
            welcome_text = "✅ Entrada de usuarios"
        else:
            welcome_text = "▪️ Entrada de usuarios"
        if adgroup.goodbye == 1:
            goodbye_text = "✅ Salida de usuarios"
        else:
            goodbye_text = "▪️ Salida de usuarios"        
        if adgroup.nests == 1:
            nests_text = "✅ Solicitud de nidos"
        else:
            nests_text = "▪️ Solicitud de nidos"
        if adgroup.globalEjections == 1:
            gEjections_text = "✅ Expulsiones masivas"
        else:
            gEjections_text = "▪️ Expulsiones masivas"
        if adgroup.ejections == 1:
            ejections_text = "✅ Expulsiones individuales"
        else:
            ejections_text = "▪️ Expulsiones individuales"

        settings_keyboard = [
            [InlineKeyboardButton(admin_text, callback_data='settings_ladmin_admin')],
            [InlineKeyboardButton(welcome_text, callback_data='settings_ladmin_welcome')],
            [InlineKeyboardButton(goodbye_text, callback_data='settings_ladmin_goodbye')],
            [InlineKeyboardButton(nests_text, callback_data='settings_ladmin_nests')],
            [InlineKeyboardButton(gEjections_text, callback_data='settings_ladmin_gejections')],
            [InlineKeyboardButton(ejections_text, callback_data='settings_ladmin_ejections')],
            [InlineKeyboardButton("« Menú principal", callback_data='settings_goto_main')]
        ] 

    #0. ADMIN
    elif keyboard == "admin":
        adgroup = get_admin(chat_id)
        if adgroup.admin == 1:
            admin_text = "✅ @admin"
        else:
            admin_text = "▪️ @admin"
        if adgroup.welcome == 1:
            welcome_text = "✅ Entrada de usuarios"
        else:
            welcome_text = "▪️ Entrada de usuarios"
        if adgroup.goodbye == 1:
            goodbye_text = "✅ Salida de usuarios"
        else:
            goodbye_text = "▪️ Salida de usuarios"        
        if adgroup.nests == 1:
            nests_text = "✅ Solicitud de nidos"
        else:
            nests_text = "▪️ Solicitud de nidos"
        if adgroup.globalEjections == 1:
            gEjections_text = "✅ Expulsiones masivas"
        else:
            gEjections_text = "▪️ Expulsiones masivas"
        if adgroup.ejections == 1:
            ejections_text = "✅ Expulsiones individuales"
        else:
            ejections_text = "▪️ Expulsiones individuales"
        
        spy_mode = "🚸 Modo Incognito"

        settings_keyboard = [
            [InlineKeyboardButton(admin_text, callback_data='settings_admin_admin')],
            [InlineKeyboardButton(welcome_text, callback_data='settings_admin_welcome')],
            [InlineKeyboardButton(goodbye_text, callback_data='settings_admin_goodbye')],
            [InlineKeyboardButton(nests_text, callback_data='settings_admin_nests')],
            [InlineKeyboardButton(gEjections_text, callback_data='settings_admin_gejections')],
            [InlineKeyboardButton(ejections_text, callback_data='settings_admin_ejections')],
            #[InlineKeyboardButton(spy_mode, callback_data='settings_admin_spy')],
            [InlineKeyboardButton("Terminado", callback_data='settings_done')]
        ] 

    settings_markup = InlineKeyboardMarkup(settings_keyboard)
    return settings_markup


def update_settings_message(chat_id, bot, message_id, keyboard = "main"):
    logging.debug("supportmethods:update_settings_message: %s %s" % (chat_id, keyboard))

    settings_markup = get_settings_keyboard(chat_id, keyboard=keyboard)

    text = (
        "Elige una categoría para ver las opciones disponibles. Cuando"
        " termines, pulsa el botón <b>Terminado</b> para borrar el "
        "mensaje."
    )

    return bot.edit_message_text(
        text=text,
        chat_id=chat_id,
        message_id=message_id,
        reply_markup=settings_markup,
        parse_mode=telegram.ParseMode.HTML,
        disable_web_page_preview=True
    )


def parse_profile_image(filename, desired_pokemon, store_path=None,
                        store_base_name=None):
    
    logging.debug("%s", filename)

    config = get_config()
    models_dir = os.path.expanduser(config['general']['modelsdir'])

    inspect = store_path is not None and store_base_name is not None

    if inspect:
        store_path = os.path.expanduser(store_path)
        store_base_filename = os.path.join(store_path, store_base_name)

        if not os.path.exists(os.path.expanduser(store_path)):
            os.makedirs(store_path)

        logging.debug(
            "Inspect files will be stored in %s, using %s as basename",
            store_path, store_base_name
        )

    # Load possible pokemons
    if desired_pokemon is not None:
        thispoke_models = []
        for j in range(1, 10):
            pokfname = os.path.join(models_dir, 'pokemon', '{}{}.png'.format(
                desired_pokemon, j)
            )


            if not os.path.isfile(pokfname):
                break

            p = cv2.imread(pokfname)
            p = cv2.cvtColor(p, cv2.COLOR_BGR2GRAY)
            p = cv2.resize(p, (60,60))
            thispoke_models.append(p)

        pokemon = {"models": thispoke_models}

    # Load possible profiles
    profiles = {}
    for i in validation_profiles:
        img_path = os.path.join(models_dir, 'profiles', '{}.jpg'.format(i))
        p = cv2.imread(img_path)
        p = cv2.cvtColor(p, cv2.COLOR_BGR2GRAY)
        p = cv2.resize(p, (120, 120))
        profiles[i] = {"model": p}

    # Load the full image
    image = cv2.imread(filename)
    height, width, _ = image.shape
    aspect_ratio = height/width

    # Preprocessing image
    logging.debug("Preprocessing image...")

    # Raise error for unsupported aspect ratios
    if aspect_ratio <= 1.64 or aspect_ratio >= 2.17:
        if inspect:
            cv2.imwrite("{}_img.png".format(store_base_filename), image)

        raise ProfileParseException("Aspect ratio not supported")

    # crop image
    image, height, width, aspect_ratio = crop_image(
        image, inspect=False, inspectFilename="failed"
    )
    logging.debug("Ratio: %.2f", aspect_ratio)



    # Extract profile layout for profile Testing
    logging.debug("Extracting image info...")

    profile_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    profile_gray = profile_gray[int(820*width/720):int(1030*width/720), int(0):int(width)]  # Crop from {y1:y2, x1:x2}
    profile_gray = cv2.resize(profile_gray, (120, 120))

    if inspect:
        cv2.imwrite(
            "{}_profile_BGR.png".format(store_base_filename),
            profile_gray
        )

    # Test extracted profile layout against possible profile models
    chosen_profile = None
    chosen_similarity = 0.0

    for i in validation_profiles:
        profiles[i]["similarity"] = ssim(profiles[i]["model"], profile_gray)
        logging.debug("Similarity with %s: %.2f", i, profiles[i]["similarity"])

        if (profiles[i]["similarity"] > 0.7 and
            (chosen_profile is None or
             chosen_similarity < profiles[i]["similarity"])):
            chosen_profile = i
            chosen_similarity = profiles[i]["similarity"]

            if profiles[i]["similarity"] > 0.9:
                continue

    if inspect:
        cv2.imwrite(
            "{}_profile_B_N.png".format(store_base_filename),
            profile_gray
        )

    logging.debug("Chosen profile: %s", chosen_profile)

    # Extract and OCR team
    team1_img = image[int(100*width/720):int(125*width/720),int(width-7):int(width)] # y1:y2,x1:x2 (RECORTE)

    if inspect:
        cv2.imwrite(
            "{}_team_BGR.png".format(store_base_filename),
            team1_img
        )

    # Prepare color boundaries to extract team#Genta
    boundariesteam = {
        Team.RED: ([0, 0, 140], [100, 70, 255]),  # BGR
        Team.BLUE: ([90, 50, 0], [255, 140, 70]),  # BGR
        Team.YELLOW: ([0, 180, 200], [100, 225, 255])# BGR
    }

    chosen_color = None
    values = {}

    for color in boundariesteam:
        # create NumPy arrays from the boundaries
        lower = np.array(boundariesteam[color][0], dtype="uint8")
        upper = np.array(boundariesteam[color][1], dtype="uint8")

        # find the colors within the specified boundaries and apply the mask
        mask = cv2.inRange(team1_img, lower, upper)
        output = cv2.bitwise_and(team1_img, team1_img, mask=mask)
        values[color] = output.mean()

        logging.debug("%s: Mean value for color %s: %s", store_base_filename, color, values[color])

        if chosen_color is None or values[color] > values[chosen_color]:
            chosen_color = color

    if inspect:
        cv2.imwrite(
            "{}_team_B_N.png".format(store_base_filename),
            mask
        )

    logging.debug("%s: Chosen color: %s", store_base_filename, chosen_color)

    # Prepare color boundaries to extract trainer and pokémon name
    boundariestext = {
        Team.YELLOW: ([0, 125, 170], [70, 175, 255]),
        Team.RED: ([35, 20, 120], [75, 60, 160]),
        Team.BLUE: ([90, 70, 10], [150, 125, 100])
    }

    # create NumPy arrays from the boundaries
    lower = np.array(boundariestext[chosen_color][0], dtype="uint8")
    upper = np.array(boundariestext[chosen_color][1], dtype="uint8")

    # Extract and OCR trainer and Pokémon name
    nick1_img = image[int(200*width/720):int(300*width/720),int(width/18):int(width/18+width/2)]

    # find colors within the specified boundaries
    nick1_gray = cv2.inRange(nick1_img, lower, upper)
    nick1_gray = 255 - nick1_gray

    # Find boundaries and clean small artifacts
    contours, chier = cv2.findContours(nick1_gray, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        if w*h > 15:
            continue

        # draw a white rectangle to mask the unwanted artifact
        cv2.rectangle(nick1_gray, (x, y), (x+w, y+h), (255, 255, 255), -1)

    # Do the OCR
    trainer_name_grey, pokemon_name_grey = image_to_trainer_and_pokemon(nick1_gray)
    trainer_name, pokemon_name = image_to_trainer_and_pokemon(nick1_img)

    if inspect:
        cv2.imwrite(
            "{}_names_img.png".format(store_base_filename),
            nick1_img
        )

        cv2.imwrite(
            "{}_names_gray.png".format(store_base_filename),
            nick1_gray
        )


    logging.debug("%s: Trainer name: %s", store_base_filename, trainer_name)
    logging.debug("%s: Pokemon name: %s", store_base_filename, pokemon_name)

    # Extract and OCR level
    level1_img = image[
        int(820*width/720):int(890*width/720),
        int(width/18):int(width/18+width/9)
    ]

    # find colors within the specified boundaries
    # Prepare color boundaries to extract level
    boundarieslevel = {
        Team.YELLOW: ([0, 180, 220], [90, 210, 255]),        
        Team.RED: ([70, 70, 200], [125, 125, 255]),
        Team.BLUE: ([210, 130, 0], [255, 190, 105])
    }
    # create NumPy arrays from the boundaries
    lower = np.array(boundarieslevel[chosen_color][0], dtype="uint8")
    upper = np.array(boundarieslevel[chosen_color][1], dtype="uint8")

    level1_gray = cv2.inRange(level1_img, lower, upper)
    level1_gray = 255 - level1_gray

    # Do the OCR
    level = pytesseract.image_to_string(level1_gray, config="--psm 6")
    logging.debug("supportmethods:parse_profile_image: Raw Level: %s" % level)
    level = re.sub('A','4',level)
    level = re.sub('a','2',level)
    level = re.sub('O','0',level)
    level = re.sub('o','0',level)
    level = re.sub('/','7',level)
    level = re.sub('77','7',level)
    level = re.sub(r'\D','',level)
    logging.debug("supportmethods:parse_profile_image: Clean level: %s" % level)

    if len(level) > 0:
        level = int(level)

    logging.debug("%s: Level: %s", store_base_filename, level)

    try:
        if level < 5 or level > 40:
            level = None

    except Exception as e:
        level = None

    if inspect:
        cv2.imwrite(
            "{}_level_img.png".format(store_base_filename),
            level1_img
        )
        cv2.imwrite(
            "{}_level_gray.png".format(store_base_filename),
            level1_gray
        )

    # Extract Pokemon
    logging.debug("Extracting pokemon...")
    pokemon_img = image[int(height/3+height/40):int(height-height/3), int(width/5):int(width/2)]
    pokemon_img = image[int(600*width/720):int(800*width/720), int(180*width/720):int(350*width/720)]

    pokemon_gray = cv2.cvtColor(pokemon_img, cv2.COLOR_BGR2GRAY)
    pokemon_gray = cv2.resize(pokemon_gray, (60, 60))

    # Test extracted pokemon against possible pokemons
    chosen_pokemon = None
    chosen_similarity = 0.0

    if desired_pokemon is not None:
        for pokemon_model in pokemon["models"]:
            pokemon["similarity"] = ssim(pokemon_model, pokemon_gray)
            logging.debug(
                "Similarity with %s: %.2f", desired_pokemon,
                pokemon["similarity"])

            if (
                pokemon["similarity"] > 0.7 and
                (chosen_pokemon is None or
                 chosen_similarity < pokemon["similarity"]
                )
            ):
                chosen_pokemon = desired_pokemon
                chosen_similarity = pokemon["similarity"]

                if pokemon["similarity"] > 0.9:
                    break

    if inspect:
        cv2.imwrite(
            "{}_pokemon_img.png".format(store_base_filename),
            pokemon_img
        )
        cv2.imwrite(
            "{}_pokemon_gray.png".format(store_base_filename),
            pokemon_gray
        )
    logging.debug("%s: Chosen Pokemon: %s", store_base_filename, chosen_pokemon)

    # Cleanup and return
    return Profile(trainer_name, trainer_name_grey, level, chosen_color,
            chosen_pokemon, pokemon_name, pokemon_name_grey, chosen_profile)


def crop_image(image, inspect=False, inspectFilename="failed"):
    logging.debug("")

    height, width, _ = image.shape
    aspect_ratio = height/width

    logging.debug("Aspect ratio %s", aspect_ratio)

    # Validations will be saved for debugging purposes if passed inspect=True
    config = get_config()
    inspectdir = os.path.expanduser(config['general']['inspectdir'])

    if not os.path.exists(inspectdir):
        os.makedirs(inspectdir)

   # Crop GalaxyS8+ fullscreen and normal mode bars, Google Pixel XL3 bars
    if aspect_ratio > 2.04 and aspect_ratio < 2.06:
        logging.debug("supportmethods:crop_image: Detected GalaxyS8+/PixelXL3 Ratio")
        logging.debug("supportmethods:crop_image: Testing GalaxyS8+ Normal Black bars...")
        bottombar_img = image[int(height-height/12.2):int(height-height/18),int(0):int(width)] # y1:y2,x1:x2
        bottombar_gray = cv2.cvtColor(bottombar_img, cv2.COLOR_BGR2GRAY)
        topbar_img = image[int(height/32):int(height/19),int(0):int(width)] # y1:y2,x1:x2
        topbar_gray = cv2.cvtColor(bottombar_img, cv2.COLOR_BGR2GRAY)
        if inspect:
            cv2.imwrite(inspectdir + "/%s_img_s8_topbar_nml.png" % inspectFilename, topbar_img)
            cv2.imwrite(inspectdir + "/%s_img_s8_bottombar_nml.png" % inspectFilename, bottombar_img)
        if bottombar_gray.mean() < 10 and topbar_gray.mean() < 10:
            logging.debug("supportmethods:crop_image: Detected GalaxyS8+ Normal Black bars, cropping!")
            image = image[int(69*width/622):int(height-105*width/622),int(0):int(width)] # y1:y2,x1:x2
            height, width, _ = image.shape
            aspect_ratio = height/width
            if inspect:
                cv2.imwrite(inspectdir + "/%s_img_s8_nml.png" % inspectFilename, image)
        else:
            logging.debug("supportmethods:crop_image: Testing Google Pixel XL3 bars...")
            bottombar_img = image[int(height-60*width/622):int(height),int(0):int(100*width/622)] # y1:y2,x1:x2
            bottombar_gray = cv2.cvtColor(bottombar_img, cv2.COLOR_BGR2GRAY)
            topbar_img = image[0:int(73*width/622),int(0):int(16*width/622)] # y1:y2,x1:x2
            topbar_gray = cv2.cvtColor(topbar_img, cv2.COLOR_BGR2GRAY)
            if inspect:
                cv2.imwrite(inspectdir + "/%s_img_xl3_topbar.png" % inspectFilename, topbar_img)
                cv2.imwrite(inspectdir + "/%s_img_xl3_bottombar.png" % inspectFilename, bottombar_img)
            if bottombar_gray.mean() < 3 and topbar_gray.mean() < 3:
                logging.debug("supportmethods:crop_image: Detected Google Pixel XL3 bars, cropping!")
                image = image[int(70*width/622):int(height-61*width/622),int(0):int(width)] # y1:y2,x1:x2
                height, width, _ = image.shape
                aspect_ratio = height/width
                if inspect:
                    cv2.imwrite(inspectdir + "/%s_img_xl3.png" % inspectFilename, image)
            else:
                logging.debug("supportmethods:crop_image: Testing GalaxyS8+ Fullscreen Black bars...")
                bottombar_img = image[int(height-height/15):int(height),int(0):int(width)] # y1:y2,x1:x2
                bottombar_gray = cv2.cvtColor(bottombar_img, cv2.COLOR_BGR2GRAY)
                topbar_img = image[int(height/60):int(height/15),int(0):int(width)] # y1:y2,x1:x2
                topbar_gray = cv2.cvtColor(topbar_img, cv2.COLOR_BGR2GRAY)
                if inspect:
                    cv2.imwrite(inspectdir + "/%s_img_s8_topbar_fs.png" % inspectFilename, topbar_img)
                    cv2.imwrite(inspectdir + "/%s_img_s8_bottombar_fs.png" % inspectFilename, bottombar_img)
                if bottombar_gray.mean() < 40 and topbar_gray.mean() < 40:
                    logging.debug("supportmethods:crop_image: Detected GalaxyS8+ Fullscreen Black bars, cropping!")
                    image = image[int(height/14):int(height-height/14),int(0):int(width)] # y1:y2,x1:x2
                    height, width, _ = image.shape
                    aspect_ratio = height/width
                    if inspect:
                        cv2.imwrite(inspectdir + "/%s_img_s8_fs.png" % inspectFilename, image)

    # Partially crop iPhone 6S/7 sharing internet bar
    if aspect_ratio > 1.776 and aspect_ratio < 1.782:
        logging.debug("supportmethods:crop_image: Detected possible iPhone 6S or iPhone 7 Ratio (%s)" % aspect_ratio)
        barheight = height/19 if aspect_ratio < 1.778 else height/17
        topbar_img = image[0:int(barheight),int(0):int(width)] # y1:y2,x1:x2
        # Prepare color boundaries to extract topbars
        boundaries = {
            "sharing": ([210, 110, 30], [250, 140, 75])
        }
        # create NumPy arrays from the boundaries
        lower = np.array(boundaries["sharing"][0], dtype = "uint8")
        upper = np.array(boundaries["sharing"][1], dtype = "uint8")
        topbar_gray = 255 - cv2.inRange(topbar_img, lower, upper)
        h, w = topbar_gray.shape
        topbar_gray_left = topbar_gray[int(h/2):int(h),int(0):int(w/6)]
        topbar_gray_right = topbar_gray[int(h/2):int(h),int(w-w/6):int(w)]

        logging.debug("supportmethods:crop_image: Detecting sharing top bar means... %s %s %s" % (topbar_gray.mean(),topbar_gray_left.mean(),topbar_gray_right.mean()))
        if topbar_gray_left.mean() < 8 and topbar_gray_right.mean() < 8:
             logging.debug("supportmethods:crop_image: Detected sharing top bar, cropping!")
             image = image[int(barheight/2):int(height),int(0):int(width)] # y1:y2,x1:x2
             height, width, _ = image.shape
             aspect_ratio = height/width
        if inspect:
            cv2.imwrite(inspectdir + "/%s_img_iphone_topbar.png" % inspectFilename, topbar_img)
            cv2.imwrite(inspectdir + "/%s_img_iphone_topbar_gray.png" % inspectFilename, topbar_gray)
            cv2.imwrite(inspectdir + "/%s_img_iphone_topbar_grayL.png" % inspectFilename, topbar_gray_left)
            cv2.imwrite(inspectdir + "/%s_img_iphone_topbar_grayR.png" % inspectFilename, topbar_gray_right)
            cv2.imwrite(inspectdir + "/%s_img_iphone_image.png" % inspectFilename, image)

    # Crop One Plus 6T white + black bars
    if aspect_ratio > 2.16 and aspect_ratio < 2.17:
        logging.debug("supportmethods:crop_image: Possible OnePlus 6T. Testing for grey bar + letterbox...")
        whitebar = image[int(height-69*width/590):int(height),int(0):int(width/25)] # y1:y2,x1:x2
        blackbar_bottom = image[int(height-126*width/590):int(height-70*width/590),int(0):int(width/25 )] # y1:y2,x1:x2
        blackbar_top = image[0:int(104*width/590),int(0):int(width/30)] # y1:y2,x1:x2
        if ((whitebar.mean() > 220 and whitebar.mean() < 245) or whitebar.mean() < 5) and whitebar.std() < 4 and blackbar_bottom.mean() < 4 and blackbar_bottom.std() < 4 and blackbar_top.mean() < 4 and blackbar_top.std() < 4:
            logging.debug("supportmethods:crop_image: TRUE for OnePlus 6T")
            image = image[int(105*width/590):int(height-126*width/590),int(0):int(width)] # y1:y2,x1:x2
            height, width, _ = image.shape
            aspect_ratio = height/width

        if inspect:
            cv2.imwrite(inspectdir + "/%s_img_oneplus6t_1.png" % inspectFilename, whitebar)
            cv2.imwrite(inspectdir + "/%s_img_oneplus6t_2.png" % inspectFilename, blackbar_bottom)
            cv2.imwrite(inspectdir + "/%s_img_oneplus6t_3.png" % inspectFilename, blackbar_top)

    # Crop One Plus 6 white + black bars
    if aspect_ratio > 2.10 and aspect_ratio < 2.12:
        logging.debug("supportmethods:crop_image: Possible OnePlus 6. Testing for grey bar + letterbox...")
        whitebar = image[int(height-64*width/606):int(height),int(0):int(width/25)] # y1:y2,x1:x2
        blackbar_bottom = image[int(height-112*width/606):int(height-72*width/606),int(0):int(width/25 )] # y1:y2,x1:x2
        blackbar_top = image[0:int(90*width/606),int(0):int(width/30)] # y1:y2,x1:x2
        if ((whitebar.mean() > 220 and whitebar.mean() < 245) or whitebar.mean() < 5) and whitebar.std() < 4 and blackbar_bottom.mean() < 4 and blackbar_bottom.std() < 4 and blackbar_top.mean() < 4 and blackbar_top.std() < 4:
            logging.debug("supportmethods:crop_image: TRUE for OnePlus 6")
            image = image[int(90*width/606):int(height-112*width/606),int(0):int(width)] # y1:y2,x1:x2
            height, width, _ = image.shape
            aspect_ratio = height/width

        if inspect:
            cv2.imwrite(inspectdir + "/%s_img_oneplus6_1.png" % inspectFilename, whitebar)
            cv2.imwrite(inspectdir + "/%s_img_oneplus6_2.png" % inspectFilename, blackbar_bottom)
            cv2.imwrite(inspectdir + "/%s_img_oneplus6_3.png" % inspectFilename, blackbar_top)

    # Crop One Plus 5T white + black bars
    if aspect_ratio > 1.99 and aspect_ratio < 2.01:
        logging.debug("supportmethods:crop_image: Possible OnePlus 5T. Testing for grey/black bar + letterbox...")
        whitebar = image[int(height-67*width/640):int(height),int(0):int(width/25)] # y1:y2,x1:x2
        blackbar_bottom = image[int(height-82*width/640):int(height-75*width/640),int(0):int(width/25)] # y1:y2,x1:x2
        blackbar_top = image[0:int(58*width/640),int(0):int(width/42)] # y1:y2,x1:x2
        if ((whitebar.mean() > 220 and whitebar.mean() < 245) or whitebar.mean() < 5) and whitebar.std() < 4 and blackbar_bottom.mean() < 4 and blackbar_bottom.std() < 4 and blackbar_top.mean() < 4 and blackbar_top.std() < 4:
            logging.debug("supportmethods:crop_image: TRUE for OnePlus 5T")
            image = image[int(58*width/640):int(height-82*width/640),int(0):int(width)] # y1:y2,x1:x2
            height, width, _ = image.shape
            aspect_ratio = height/width

        if inspect:
            cv2.imwrite(inspectdir + "/%s_img_oneplus5t_1.png" % inspectFilename, whitebar)
            cv2.imwrite(inspectdir + "/%s_img_oneplus5t_2.png" % inspectFilename, blackbar_bottom)
            cv2.imwrite(inspectdir + "/%s_img_oneplus5t_3.png" % inspectFilename, blackbar_top)

    # Notch cropping
    notches = [
        {
            "model": "Xiaomi Mi8 (new)",
            "aspectratio": { "min": 2.08, "max": 2.09 },
            "notchheight": int(110*width/1080),
            "testingwidth": int(40*width/1080)
        },
        {
            "model": "Xiaomi Mi8 (old)",
            "aspectratio": { "min": 2.08, "max": 2.09 },
            "notchheight": int(102*width/1080),
            "testingwidth": int(40*width/1080)
        },
        {
            "model": "Pocophone F1",
            "aspectratio": { "min": 2.08, "max": 2.09 },
            "notchheight": int(49*width/615),
            "testingwidth": int(40*width/1080)
        },
        {
            "model": "Essential Phone",
            "aspectratio": { "min": 1.95, "max": 1.96 },
            "notchheight": int(71*width/656),
            "testingwidth": int(22*width/656)
        },
        {
            "model": "Huawei P20",
            "aspectratio": { "min": 2.07, "max": 2.08 },
            "notchheight": int(48*width/616),
            "testingwidth": int(24*width/616)
        },
        {
            "model": "Huawei Mate 20",
            "aspectratio": { "min": 2.07, "max": 2.08 },
            "notchheight": int(45*width/616),
            "testingwidth": int(30*width/616)
        },
        {
            "model": "Huawei Honor Play",
            "aspectratio": { "min": 2.16, "max": 2.17 },
            "notchheight": int(49*width/590),
            "testingwidth": int(30*width/590)
        },
        {
            "model": "Huawei Mate 20 Pro",
            "aspectratio": { "min": 2.16, "max": 2.17 },
            "notchheight": int(41*width/590),
            "testingwidth": int(30*width/590)
        },
        {
            "model": "Huawei Honor 10",
            "aspectratio": { "min": 2.11, "max": 2.12 },
            "notchheight": int(50*width/606),
            "testingwidth": int(30*width/606)
        }
    ]

    for notch in notches:
        if aspect_ratio > notch["aspectratio"]["min"] and aspect_ratio < notch["aspectratio"]["max"]:
            logging.debug("supportmethods:crop_image: Possible %s. Testing for notch black bar..." % notch["model"])
            blackbar = image[int(0):notch["notchheight"],int(0):notch["testingwidth"]] # y1:y2,x1:x2
            if blackbar.mean() < 4 and blackbar.std() < 4:
                logging.debug("supportmethods:crop_image: TRUE for %s!" % notch["model"])
                image = image[notch["notchheight"]+1:int(height),int(0):int(width)] # y1:y2,x1:x2
                height, width, _ = image.shape
                aspect_ratio = height/width
                if inspect:
                    cv2.imwrite(inspectdir + "/%s_img_%s_cut.png" % (inspectFilename, notch["model"]), image)
            if inspect:
                cv2.imwrite(inspectdir + "/%s_img_%s_test.png" % (inspectFilename, notch["model"]), blackbar)

    # Crop several sizes bottom bars
    for cropbarheight in [int(height/13),int(height/14),int(height/15),int(height/16),int(height/17),int(height/18),int(height/19),int(height/20),int(height/21)]:
        logging.debug("supportmethods:crop_image: Testing for bottom bar %ipx..." % cropbarheight)
        bottombar_img = image[int(height-cropbarheight):int(height),int(0):int(width/4)]
        bottombar_gray = cv2.cvtColor(bottombar_img, cv2.COLOR_BGR2GRAY)
        bh, bw = bottombar_gray.shape
        bottombartop_gray = bottombar_gray[0:int(bh/10),int(0):int(bw)]
        bottombarleft = bottombar_img[0:bh, 0:int(bw/14)]
        bottombarleft2 = bottombar_img[0:bh, int(bw/14):int(bw/7)]
        diffleft = abs(bottombarleft.mean() - bottombarleft2.mean())
        difftop = abs(bottombartop_gray.mean() - bottombar_gray.mean())

        # Avoid detecting raid invitations as bars
        boundaries = ([70, 40, 0], [80, 50, 18])
        # create NumPy arrays from the boundaries
        lower = np.array(boundaries[0], dtype = "uint8")
        upper = np.array(boundaries[1], dtype = "uint8")
        # find the colors within the specified boundaries and apply the mask
        mask = cv2.inRange(bottombarleft2, lower, upper)
        output = cv2.bitwise_and(bottombarleft2, bottombarleft2, mask = mask)
        logging.debug("supportmethods:crop_image: Mean value for raid invitation color: %s" % (output.mean()))
        if output.mean() > 30:
            continue

        if ((bottombar_gray.mean() < 30 or bottombar_gray.std() < 30) and
            difftop < 11 and diffleft < 10 and bottombartop_gray.std() < 10) or bottombartop_gray.mean() < 8:
            logging.debug("supportmethods:crop_image: TRUE bottom bar! Mean: %s Std: %s Difftop: %s Diffleft: %s Stdtop: %s Meantop: %s" % (bottombar_gray.mean(),bottombar_gray.std(),difftop,diffleft,bottombartop_gray.std(), bottombartop_gray.mean()))
            newimage = image[int(0):int(height-cropbarheight),int(0):int(width)] # y1:y2,x1:x2
            if inspect:
                cv2.imwrite(inspectdir + "/%s_img_%icropped.png" % (inspectFilename,cropbarheight), newimage)
                croppedbar = image[int(height-cropbarheight):int(height),int(0):int(width)] # y1:y2,x1:x2
                cv2.imwrite(inspectdir + "/%s_img_%ibar.png" % (inspectFilename,cropbarheight), croppedbar)
                cv2.imwrite(inspectdir + "/%s_img_%ibar2.png" % (inspectFilename,cropbarheight), bottombarleft)
                cv2.imwrite(inspectdir + "/%s_img_%ibar3.png" % (inspectFilename,cropbarheight), bottombarleft2)
            image = newimage
            height, width, _ = image.shape
            aspect_ratio = height/width
            break
        else:
            logging.debug("supportmethods:crop_image: False bottom bar! Mean: %s Std: %s Difftop: %s Diffleft: %s Stdtop: %s Meantop: %s" % (bottombar_gray.mean(),bottombar_gray.std(),difftop,diffleft,bottombartop_gray.std(),bottombartop_gray.mean()))

    # Absurd new Samsung Galaxy game mode screenshots (fuck you Samsung)
    for cropbarheight in [int(height/55), int(height/105)]:
        logging.debug("supportmethods:crop_image: Testing for absurd Samsung Galaxy mode screenshot %ipx..." % cropbarheight)
        bottombar_img = image[int(height-cropbarheight):int(height),int(0):int(width)]
        bottombar_gray = cv2.cvtColor(bottombar_img, cv2.COLOR_BGR2GRAY)
        if (bottombar_gray.mean() < 5 and bottombar_gray.std() < 5):
            logging.debug("supportmethods:crop_image: TRUE bottom bar! Mean: %s Std: %s" % (bottombar_gray.mean(),bottombar_gray.std()))
            newimage =  image[0:int(height-cropbarheight),int(0):int(width)]
            image = newimage
            height, width, _ = image.shape
            aspect_ratio = height/width
            if inspect:
                cv2.imwrite(inspectdir + "/%s_img_samsunggame.png" % (inspectFilename), image)

    # Crop side bars like in LG Q6 or Sony Xperia Z
    for cropbarwidth in [int(25*width/720),int(8*width/720)]:
        logging.debug("supportmethods:crop_image: Testing for side bars %ipx..." % cropbarwidth)
        sidebar_img = image[0:int(height/2),int(0):cropbarwidth]
        sidebar_gray = cv2.cvtColor(sidebar_img, cv2.COLOR_BGR2GRAY)
        bh, bw = sidebar_gray.shape
        if (sidebar_gray.mean() < 5 and sidebar_gray.std() < 5):
            logging.debug("supportmethods:crop_image: TRUE side bar! Mean: %s Std: %s" % (sidebar_gray.mean(),sidebar_gray.std()))
            newimage = image[0:int(height),int(cropbarwidth):int(width-cropbarwidth)] # y1:y2,x1:x2
            if inspect:
                cv2.imwrite(inspectdir + "/%s_img_cropped.png" % (inspectFilename), newimage)
                cv2.imwrite(inspectdir + "/%s_img_sidebar.png" % (inspectFilename), sidebar_img)
            image = newimage
            height, width, _ = image.shape
            aspect_ratio = height/width
            break
        else:
            logging.debug("supportmethods:crop_image: False side bar! Mean: %s Std: %s" % (sidebar_gray.mean(),sidebar_gray.std()))

    return (image, height, width, aspect_ratio)



def get_unique_from_query(query):
    '''
    Handly method to retrieve an element from a query when it should be unique.

    If no element matches the query or there are several ones, return None
    '''

    return query[0] if query.count() == 1 else None


def get_unified_timezone(informal_timezone):
    '''
    Method to retrieve the standard timezone string from any string
    '''

    regexp = re.compile(r'.*{}.*'.format(informal_timezone))

    return [tz for tz in all_timezones if regexp.match(tz)]


def create_needed_paths():
    '''
    Method that will create the needed directory structure based on the
    loaded configuration
    '''

    config = get_config()

    if not config:
        logging.error(
            'Not configuration loaded. Please, use `create_needed_paths` '
            'only after loading a valid configuration'
        )

        raise ConfigurationNotLoaded()

    needed_dirs = []

    needed_dirs.append(
        os.path.dirname(
            os.path.expanduser(config['general'].get('log'))) or
        os.getcwd()
    )

    needed_dirs.append(os.path.expanduser(config['general']['photos']))
    needed_dirs.append(
        os.path.dirname(os.path.expanduser(config['general']['inspectdir']))
    )

    needed_dirs.append(
        os.path.dirname(os.path.expanduser(config['general']['modelsdir']))
    )

    for directory in needed_dirs:
        if not os.path.exists(directory):
            os.makedirs(directory)

    return True


def image_to_trainer_and_pokemon(image_array):
    '''
    This method uses pytesseract to retrieve the trainer and Pokémon name
    from an image array
    '''
    logging.debug("supportmethods:image_to_trainer_and_pokemon:h")
    text = pytesseract.image_to_string(
        image_array,
        config='--psm 6'
    )

    logging.debug("supportmethods:image_to_trainer_and_pokemon:OCR_DONE")
    trainer_name = re.sub(r'\n+.*$', '', text)
    trainer_name = trainer_name.replace(" ", "").replace("|", "l").replace("||", "ll").replace("ﬁ", "ri").replace("ﬂ", "ri")
    pokemon_name = re.sub(r'^.*\n+([^ ]+)[ ]?', '', text).replace(" ", "")

    if pokemon_name == "":
        # Alternative pokemon name parsing
        pokemon_name = re.sub(r'^.*\n+(et|&|con|e)[ ]*', '', text)
    logging.debug("supportmethods:image_to_trainer_and_pokemon:names_fixed")

    return trainer_name, pokemon_name
