# Nurse Joy Bot
 
Master: [![build status](https://gitlab.com/Pikaping/NurseJoyBot/badges/master/build.svg)](https://gitlab.com/Pikaping/NurseJoyBot/commits/master)

Prerelease: [![build status](https://gitlab.com/Pikaping/NurseJoyBot/badges/Prerelease/build.svg)](https://gitlab.com/Pikaping/NurseJoyBot/commits/Prerelease)

A Telegram Bot to ...

## Requirements

Requires Python 3.4+ and a MySQL 5.5+ or MariaDB database.

To install the required Python libraries, just run:

```bash
sudo pip3 install -r requirements.txt
```

The system package `tesseract-ocr` is also required. To install it in Debian-based systems, just run:

```bash
sudo apt-get install tesseract-ocr
```

## Configuration

After the first run, an example configuration file will be written in `~/.config/nursejoy/config.ini` containing three sections:

* `database` to configure the connection to the MySQL database
* `telegram` to configure the Telegram bot Token
* `googlemaps` to configure the Google Maps API key

The database should be initialized manually with the contents of `schema.sql`. The user must have access to the application schema and read-only access to the built-in `mysql` schema.

You must load the timezone info into the `mysql` schema. This is usually done by the script `mysql_tzinfo_to_sql` included in MySQL distributions. Look at [the MySQL manual](https://dev.mysql.com/doc/refman/5.5/en/mysql-tzinfo-to-sql.html) for more details.

After all that hard work, you should be up and running!
