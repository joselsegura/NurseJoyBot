# -*- coding: utf-8 -*-

import os


def get_base_path():
    return os.path.abspath(os.path.join(os.path.dirname(__file__)))
