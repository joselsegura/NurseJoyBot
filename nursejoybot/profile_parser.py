#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

from Levenshtein import distance


class ProfileParseException(Exception):
    '''
    Concrete exception to allow to catch the right ones
    '''
    pass


class Profile:
    '''
    This class contains all the needed values for checking a profile
    screenshot
    '''
    def __init__(self, name, name_grey, level, team, pokemon, buddy_name,
                 buddy_name_grey, model=None):
        '''
        :param name: read trainer name from the color screenshot
        :param name_grey: read trainer name from the b&w screeshot
        :param level: read level of the trainer from the screenshot
        :param team: read team of the trainer from the screenshot
        :param pokemon: read buddy Pokemon from the screenshot
        :param buddy_name: read buddy name from the color screenshot
        :param buddy_name_grey: read buddy name from the b&w screenshot
        :param model: matched profile model
        '''

        self.names = [n.lower() for n in [name, name_grey]]
        self.level = level
        self.team = team
        self.buddy = pokemon
        self.buddy_names = [n.lower() for n in [buddy_name, buddy_name_grey]]
        self.matched_model = model

    def check_name(self, name):
        '''
        This method checks if the argument name is similar enough to the
        names detected by the OCR for this profile
        :param name: str containing the name to check
        :return: bool. True if "name" is similar enough. False otherwise
        '''

        distances = [distance(name.lower(), n) for n in self.names]

        return min(distances) < 3

    def check_buddy_name(self, buddy_name):
        '''
        This method checks if the argument buddy_name is similar enough to the
        buddy names detected by the OCR for this profile
        :param buddy_name: str containing the buddy name to check
        :return: bool. True if "buddy_name" is similar enough. False otherwise
        '''

        distances = [distance(buddy_name.lower(), n)
                     for n in self.buddy_names]

        return min(distances) < 3
