#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base

from nursejoybot.config import get_config


__ENGINE = None
__BASE = None
__SESSION = None


def get_db_engine():
    global __ENGINE

    if __ENGINE is None:
        config = get_config()

        try:
            db_url = config['database']['db-url']

        except KeyError:
            db_url = 'mysql://{user}:{password}@{host}:{port}/{schema}?charset=utf8mb4'.format(
                **config['database']
            )

        debug = config['database'].get('debug', "false")

        if debug.lower() == "false":
            debug = False

        else:
            try:
                debug = bool(int(debug))  # numeric values

            except ValueError:
                debug = bool(debug)

        if debug:
            print('DB URL: ', db_url)

        __ENGINE = create_engine(db_url, pool_size=35, max_overflow=50, echo=False, pool_pre_ping=True)

    return __ENGINE


def get_declarative_base():
    global __BASE

    if __BASE is None:
        __BASE = declarative_base()

    return __BASE


def get_session():
    global __SESSION

    if __SESSION is None:
        session_factory = sessionmaker(bind=get_db_engine(), autoflush=False,)
        __SESSION = scoped_session(session_factory)

    return __SESSION
