#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import os
import re
import logging
import telegram

from uuid import uuid4
from telegram.ext.dispatcher import run_async

from nursejoybot.model import (
    User,
    Team,
    Validation,
    ValidationType,
    ValidationStep,
    ValidationRequiered
)
from nursejoybot.supportmethods import (
    is_admin,
    extract_update_info,
    delete_message,
    ensure_escaped
)
from nursejoybot.storagemethods import (
    commit_user,
    set_user,
    get_user,
    ban_user,
    del_user,
    unban_user,
    get_user_by_name,
    active_validation,
    expire_validation,
    get_users_from_group
)


@run_async
def edit_cmd(bot, update, args=None):
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    bot.deleteMessage(chat_id=chat_id, message_id=message.message_id)

    if message.reply_to_message is None or message.reply_to_message.chat.id != chat_id:
        return

    if message.reply_to_message.from_user.id != bot.id:
        return

    if args is None or len(args)!=1:
        return

    text = message.reply_to_message.caption
    match = re.match(r"^(.*)Trainer ID:([0-9]{1,15})\n(New Register|Level UP):([0-9a-zA-Z\_\-]{3,15}) \d\d Team\.(RED|BLUE|YELLOW)", text)
    if match:
        try:
            commit_user(user_id=match.group(2), nick=args[0])
            output = "Antiguo nick: {} - Asociado al ID: {} - Nuevo nick: {}".format(
                match.group(4),
                match.group(2),
                args[0]
            )
        except Exception as ex:
            output = "Ha habido algun fallo {}".format(ex)
    else:
        output = "Ha habido algun fallo"

    bot.sendMessage(chat_id=chat_id, text=output, parse_mode=telegram.ParseMode.MARKDOWN)


@run_async
def validate(bot, update):
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    bot.deleteMessage(chat_id=chat_id, message_id=message.message_id)

    if not is_admin(chat_id, user_id, bot):
        return

    if message.reply_to_message is not None:
        replied_userid = message.reply_to_message.from_user.id

    validate = active_validation(replied_userid)
    
    if validate is None:
        bot.sendMessage(
            chat_id=chat_id, 
            text="❌ No current validation process", 
            parse_mode=telegram.ParseMode.MARKDOWN)
        return

    output = ("*Validation Info:*\n- Status: {0}\n-Trainername: {1}\n-"
        "Pkmn: {2}\n-Pkname: {3}\n-Tries: {4}").format(
                validate.step,
                validate.trainer_name,
                validate.pokemon,
                validate.pokemon_name,
                validate.tries)
    output = ensure_escaped(output)
    bot.sendMessage(chat_id=chat_id, text=output, parse_mode=telegram.ParseMode.MARKDOWN)


@run_async
def ch_staff(bot, update, args=None):
    logging.debug("nursejoybot:settings: %s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    
    bot.deleteMessage(chat_id=chat_id, message_id=message.message_id)

    if not is_admin(chat_id, user_id, bot):
        return

    if message.reply_to_message is not None:
        replied_userid = message.reply_to_message.from_user.id
        if message.reply_to_message.from_user.username is None:
            replied_username = "None"
        else:
            replied_username = ensure_escaped(message.reply_to_message.from_user.username)
        user_username = message.from_user.username
    else: 
        return

    if args == None or len(args)>4:
        logging.debug("%s", len(args))
        sent_message = bot.sendMessage(
            chat_id=chat_id, 
            text="Change what", 
            parse_mode=telegram.ParseMode.MARKDOWN)
        return

    output="👩🏼‍⚕️ [{0}](tg://user?id={1}) made changes to [{2}](tg://user?id={3}):".format(
            message.from_user.first_name,
            message.from_user.id,
            replied_username,        
            replied_userid
    )

    user = get_user(replied_userid)
    if user is None:
        set_user(replied_userid, message.reply_to_message.from_user.username)
    
    nick = None
    team = None
    validated = None
    level = None

    for arg in args:      
        if arg=="v":
            validated = ValidationType.INTERNAL
            change="\n- Validation set to *validated*"

        elif arg.lower() in ["y","r","b"]:
            if arg=="b":
                team = Team.BLUE
                change="\n- Team set to *Blue*"
            elif arg=="y":
                team = Team.YELLOW
                change="\n- Team set to *Yellow*"
            elif arg=="r":
                team = Team.RED
                change="\n- Team set to *Red*"
        elif arg.isdigit() and int(arg) >= 5 and int(arg) <= 40:
            level = int(arg)
            change="\n- Level set to *{}*".format(level)
        elif re.match(r'[a-zA-Z0-9]{3,15}$', arg) is not None:
            nick = arg
            change="\n- Trainername set to *{}*".format(nick)

        output="{}{}".format(output,change)

    try:
        commit_user(replied_userid, nick, team, validated, level)
        validate = active_validation(replied_userid)
        if validate is not None:
            expire_validation(replied_userid)
            output="{}\n-Validation process set to *EXPIRED*".format(output)
    except:
        output="Buuug @Pikaping"
    bot.sendMessage(chat_id=chat_id, text=output, parse_mode=telegram.ParseMode.MARKDOWN)


@run_async
def ban_cmd(bot, update, args=None):
    logging.debug("%s %s" % (bot, update))
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    user_username = message.from_user.username

    if args is None or len(args)!=2 or not args[0].isdigit():
        return
        
    user = get_user(args[0])
    if user is None:
        bot.sendMessage(
            chat_id=chat_id,
            text="El ID {} no está registrado".format(args[0]),
            parse_mode=telegram.ParseMode.MARKDOWN
        )

    ban_user(args[0], args[1])

    bot.sendMessage(
        chat_id=chat_id,
        text="El usuario {} - {} - @{} ha sido baneado por @{}.".format(
            user.id,
            user.trainer_name,
            user.username,
            user_username
        ),
        parse_mode=telegram.ParseMode.MARKDOWN
    )


@run_async
def reset_cmd(bot, update, args=None):
    logging.debug("%s %s" % (bot, update))
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    user_username = message.from_user.username

    if args is None or len(args)!=1:
        return
        
    user = get_user(args[0])
    if user is None:
        bot.sendMessage(
            chat_id=chat_id,
            text="El ID {} no está registrado".format(args[0]),
            parse_mode=telegram.ParseMode.MARKDOWN
        )

    del_user(args[0])

    bot.sendMessage(
        chat_id=chat_id,
        text="El usuario {} - {} - @{} ha sido eliminado por @{}.".format(
            user.id,
            user.trainer_name,
            user.username,
            user_username
        ),
        parse_mode=telegram.ParseMode.MARKDOWN
    )


@run_async
def unban_cmd(bot, update, args=None):
    logging.debug("%s %s" % (bot, update))
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    user_username = message.from_user.username

    if args is None or len(args)!=1 or not args[0].isdigit():
        return

    user = get_user(args[0])
    if user is None:
        bot.sendMessage(
            chat_id=chat_id,
            text="El ID {} no está registrado".format(args[0]),
            parse_mode=telegram.ParseMode.MARKDOWN
        )

    unban_user(args[0])

    bot.sendMessage(
        chat_id=chat_id,
        text="El usuario {} - {} - @{} ha sido desbaneado por @{}.".format(
            user.id,
            user.trainer_name,
            user.username,
            user_username
        ),
        parse_mode=telegram.ParseMode.MARKDOWN
    )

