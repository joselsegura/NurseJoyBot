#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import os
import re
import logging
import random
import time
import urllib.request
import telegram
from Levenshtein import distance

from telegram.ext import ConversationHandler
from telegram.ext.dispatcher import run_async
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from nursejoybot.supportmethods import is_admin, ensure_escaped, \
    extract_update_info, delete_message, get_welcome_type

from nursejoybot.model import Types, CustomCommands

from nursejoybot.storagemethods import set_custom_command, \
    set_custom_command_reply, get_active_command, get_commands, \
    rm_command, are_banned, get_group_settings

GET_CMD, GET_REPLY = range(2)

REG_DEL_CMD = \
    re.compile(r'^eliminar comando ([0-9]{1,10})$',
               flags=re.IGNORECASE)
REG_CHECK_CMD = re.compile(r'^([a-zA-ZÀ-Üà-ü0-9\- /()·\'•]{3,50})$',
                           flags=re.IGNORECASE)


@run_async
def create_command(bot, update, args=None):
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    if text.split(" ")[0] == "/new_cmd":
        delete_message(chat_id, message.message_id, bot)
        if message.reply_to_message:
            reply_message = message.reply_to_message
            cmd = ' '.join(args)
            if check_text(cmd):
                output = \
                    'Este comando no es un comando permitido. Quizás supere el maximo de caracteres permitido o sea un comando reservado'

                bot.sendMessage(chat_id=chat_id, text=output,
                                parse_mode=telegram.ParseMode.MARKDOWN)
                return

            set_custom_command(chat_id, cmd.lower())

            text, data_type, content, buttons = get_welcome_type(reply_message, True)

            if data_type is not None:
                try:
                    set_custom_command_reply(chat_id, content or text, data_type, buttons)
                    output = 'Comando creado exitosamente!'
                    bot.sendMessage(
                        chat_id=chat_id, 
                        text=output,
                        parse_mode=telegram.ParseMode.MARKDOWN)            
                    return
                except:
                    output = "Ha habido un error, prueba de nuevo"
                    pass
            else:
                output = 'Respuesta no permitida'

            bot.sendMessage(
                chat_id=chat_id, 
                text=output,
                parse_mode=telegram.ParseMode.MARKDOWN)            
            return

    #old code 13-sep-19
    active = get_active_command(chat_id)
    if active is None:
        output = 'De acuerdo, dime que comando quieres que cree.'
        bot.sendMessage(chat_id=chat_id, text=output,
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return GET_CMD
    else:
        output = \
            'Habías empezado la creación del comando _{}_. Continua envíandome la respuesta para ese comando'.format(active.command)
        bot.sendMessage(chat_id=chat_id, text=output,
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return GET_REPLY

#old code 13-sep-19
@run_async
def get_cmd(bot, update):
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    if check_text(text):
        output = \
            'Este comando no es un comando permitido. Quiz\xc3\xa1s supere el maximo de caracteres permitido o sea un comando reservado'

        bot.sendMessage(chat_id=chat_id, text=output,
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return GET_CMD

    set_custom_command(chat_id, text.lower())
    output = 'Perfecto! Ahora dime que quieres que responda.'

    bot.sendMessage(chat_id=chat_id, text=output,
                    parse_mode=telegram.ParseMode.MARKDOWN)
    return GET_REPLY

#old code 13-sep-19
@run_async
def get_reply(bot, update):
    (chat_id, chat_type, user_id, text, message) = extract_update_info(update)
    msg = update.effective_message  # type: Optional[Message]
    logging.debug("%s", msg.text)

    text, data_type, content, buttons = get_welcome_type(msg, True)

    logging.debug("%s %s %s %s", chat_id, content, text, data_type)

    if data_type is not None:
        try:
            set_custom_command_reply(chat_id, content or text, data_type, buttons)
            output = 'Comando creado exitosamente!'
            bot.sendMessage(
                chat_id=chat_id, 
                text=output,
                parse_mode=telegram.ParseMode.MARKDOWN)            
            return ConversationHandler.END
        except:
            output = "Ha habido un error, prueba de nuevo"
            pass
    else:
        output = 'Respuesta no permitida'

    bot.sendMessage(
        chat_id=chat_id, 
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN)            
    return GET_REPLY


@run_async
def cancel_command(bot, update):
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    rm_command(chat_id, None)
    output = 'Comando cancelado exitosamente!'

    bot.sendMessage(chat_id=chat_id, text=output,
                    parse_mode=telegram.ParseMode.MARKDOWN)
    return ConversationHandler.END


@run_async
def list_commands(bot, update):
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)

    if are_banned(user_id, chat_id):
        return

    commands = get_commands(chat_id)
    output = "*Listado de comandos*:"

    if commands is None:
        output = 'No conozco comandos personalizados en este grupo'
        bot.sendMessage(chat_id=chat_id, text=output,
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return
    if is_admin(chat_id, user_id, bot):
        for command in commands:
            if command.command_type == Types.TEXT:
                output = output + "\n#{} 📝 {}".format(command.id, command.command)
            elif command.command_type == Types.BUTTON_TEXT:
                output = output + "\n#{} 📝 {}".format(command.id, command.command)
            elif command.command_type == Types.STICKER:
                output = output + "\n#{} 📄 {}".format(command.id, command.command)
            elif command.command_type == Types.DOCUMENT:
                output = output + "\n#{} 📎 {}".format(command.id, command.command)
            elif command.command_type == Types.PHOTO:
                output = output + "\n#{} 📷 {}".format(command.id, command.command)
            elif command.command_type == Types.AUDIO:
                output = output + "\n#{} 🎙 {}".format(command.id, command.command)
            elif command.command_type == Types.VOICE:
                output = output + "\n#{} 🔉 {}".format(command.id, command.command)
            elif command.command_type == Types.VIDEO:
                output = output + "\n#{} 🎥 {}".format(command.id, command.command)
    else:
        for command in commands:
            if command.command_type == Types.TEXT:
                output = output + "\n📝 {}".format(command.command)
            elif command.command_type == Types.BUTTON_TEXT:
                output = output + "\n📝 {}".format(command.command)
            elif command.command_type == Types.STICKER:
                output = output + "\n📄 {}".format(command.command)
            elif command.command_type == Types.DOCUMENT:
                output = output + "\n📎 {}".format(command.command)
            elif command.command_type == Types.PHOTO:
                output = output + "\n📷 {}".format(command.command)
            elif command.command_type == Types.AUDIO:
                output = output + "\n🎙 {}".format(command.command)
            elif command.command_type == Types.VOICE:
                output = output + "\n🔉 {}".format(command.command)
            elif command.command_type == Types.VIDEO:
                output = output + "\n🎥 {}".format(command.command)

    if chat_type != "private":
        group = get_group_settings(chat_id)
        if group.reply_on_group:
            dest_id = chat_id
        else:
            dest_id = user_id
    else:
        dest_id = user_id

    bot.sendMessage(chat_id=dest_id, text=output,
                    parse_mode=telegram.ParseMode.MARKDOWN)


@run_async
def delete_commands(bot, update):
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)
    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    match = REG_DEL_CMD.match(text)

    if match is None:
        output = 'No conozco comandos personalizados en este grupo'
        bot.sendMessage(chat_id=chat_id, text=output,
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return

    if rm_command(chat_id, match.group(1)):
        output = 'Comando eliminado correctamente'
    else:
        output = 'No he encontrado el comando'

    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )


def check_text(text):

    match = REG_DEL_CMD.match(text)
    matched = REG_CHECK_CMD.match(text)

    if text.lower() is 'listado de comandos':
        return True
    elif text.lower() is 'nuevo comando':

        return True
    elif text.lower() is 'cancelar comando':

        return True
    elif match is not None or matched is None:

        return True

    return False