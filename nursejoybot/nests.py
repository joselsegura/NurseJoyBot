#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import logging
import re
import time
import urllib.request
import datetime
from datetime import datetime, timedelta
from threading import Thread

import telegram
from telegram.ext.dispatcher import run_async
from telegram.utils.helpers import escape_markdown
from nursejoybot.config import get_config

from nursejoybot.supportmethods import (
    is_admin,
    is_staff,
    extract_update_info,
    delete_message,
    delete_message_timed
)
from nursejoybot.group import replace
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from nursejoybot.storagemethods import (
    are_banned,
    add_nest,
    get_user,
    get_nests_settings,
    get_group_nest,
    get_user_group,
    search_pokemon,
    search_place,
    delete_nest,
    delete_all_nest,
    delete_group_nest,
    get_admin_from_linked,
    get_particular_admin,
    set_user_group,
    nest_link
)
from nursejoybot.model import (
    ValidationRequiered,
    ValidationType,
    Team,
    Types,
    MinDays,
    MinMessages,
    Group,
    User,
    UserGroup,
    LinkedGroups,
    AdminGroups,
    SettingsNests,
    Nests
)

pokemon_list = ["Bulbasaur","Ivysaur","Venusaur","Charmander","Charmeleon","Charizard","Squirtle","Wartortle","Blastoise","Caterpie","Metapod","Butterfree","Weedle","Kakuna","Beedrill","Pidgey","Pidgeotto","Pidgeot","Rattata","Raticate","Spearow","Fearow","Ekans","Arbok","Pikachu","Raichu","Sandshrew","Sandslash","NidoranH","Nidorina","Nidoqueen","NidoranM","Nidorino","Nidoking","Clefairy","Clefable","Vulpix","Ninetales","Jigglypuff","Wigglytuff","Zubat","Golbat","Oddish","Gloom","Vileplume","Paras","Parasect","Venonat","Venomoth","Diglett","Dugtrio","Meowth","Persian","Psyduck","Golduck","Mankey","Primeape","Growlithe","Arcanine","Poliwag","Poliwhirl","Poliwrath","Abra","Kadabra","Alakazam","Machop","Machoke","Machamp","Bellsprout","Weepinbell","Victreebel","Tentacool","Tentacruel","Geodude","Graveler","Golem","Ponyta","Rapidash","Slowpoke","Slowbro","Magnemite","Magneton","Farfetch’d","Doduo","Dodrio","Seel","Dewgong","Grimer","Muk","Shellder","Cloyster","Gastly","Haunter","Gengar","Onix","Drowzee","Hypno","Krabby","Kingler","Voltorb","Electrode","Exeggcute","Exeggutor","Cubone","Marowak","Hitmonlee","Hitmonchan","Lickitung","Koffing","Weezing","Rhyhorn","Rhydon","Chansey","Tangela","Kangaskhan","Horsea","Seadra","Goldeen","Seaking","Staryu","Starmie","Mr.Mime","Scyther","Jynx","Electabuzz","Magmar","Pinsir","Tauros","Magikarp","Gyarados","Lapras","Ditto","Eevee","Vaporeon","Jolteon","Flareon","Porygon","Omanyte","Omastar","Kabuto","Kabutops","Aerodactyl","Snorlax","Dratini","Dragonair","Dragonite","Chikorita","Bayleef","Meganium","Cyndaquil","Quilava","Typhlosion","Totodile","Croconaw","Feraligatr","Sentret","Furret","Hoothoot","Noctowl","Ledyba","Ledian","Spinarak","Ariados","Crobat","Chinchou","Lanturn","Pichu","Cleffa","Igglybuff","Togepi","Togetic","Natu","Xatu","Mareep","Flaaffy","Ampharos","Bellossom","Marill","Azumarill","Sudowoodo","Politoed","Hoppip","Skiploom","Jumpluff","Aipom","Sunkern","Sunflora","Yanma","Wooper","Quagsire","Espeon","Umbreon","Murkrow","Slowking","Misdreavus","Unown","Wobbuffet","Girafarig","Pineco","Forretress","Dunsparce","Gligar","Steelix","Snubbull","Granbull","Qwilfish","Scizor","Shuckle","Heracross","Sneasel","Teddiursa","Ursaring","Slugma","Magcargo","Swinub","Piloswine","Corsola","Remoraid","Octillery","Delibird","Mantine","Skarmory","Houndour","Houndoom","Kingdra","Phanpy","Donphan","Porygon2","Stantler","Smeargle","Tyrogue","Hitmontop","Smoochum","Elekid","Magby","Miltank","Blissey","Larvitar","Pupitar","Tyranitar","Treecko","Grovyle","Sceptile","Torchic","Combusken","Blaziken","Mudkip","Marshtomp","Swampert","Poochyena","Mightyena","Zigzagoon","Linoone","Wurmple","Silcoon","Beautifly","Cascoon","Dustox","Lotad","Lombre","Ludicolo","Seedot","Nuzleaf","Shiftry","Taillow","Swellow","Wingull","Pelipper","Ralts","Kirlia","Gardevoir","Surskit","Masquerain","Shroomish","Breloom","Slakoth","Vigoroth","Slaking","Nincada","Ninjask","Shedinja","Whismur","Loudred","Exploud","Makuhita","Hariyama","Azurill","Nosepass","Skitty","Delcatty","Sableye","Mawile","Aron","Lairon","Aggron","Meditite","Medicham","Electrike","Manectric","Plusle","Minun","Volbeat","Illumise","Roselia","Gulpin","Swalot","Carvanha","Sharpedo","Wailmer","Wailord","Numel","Camerupt","Torkoal","Spoink","Grumpig","Spinda","Trapinch","Vibrava","Flygon","Cacnea","Cacturne","Swablu","Altaria","Zangoose","Seviper","Lunatone","Solrock","Barboach","Whiscash","Corphish","Crawdaunt","Baltoy","Claydol","Lileep","Cradily","Anorith","Armaldo","Feebas","Milotic","Castform","Kecleon","Shuppet","Banette","Duskull","Dusclops","Tropius","Chimecho","Absol","Wynaut","Snorunt","Glalie","Spheal","Sealeo","Walrein","Clamperl","Huntail","Gorebyss","Relicanth","Luvdisc","Bagon","Shelgon","Salamence","Beldum","Metang","Metagross","Turtwig","Grotle","Torterra","Chimchar","Monferno","Infernape","Piplup","Prinplup","Empoleon","Starly","Staravia","Staraptor","Bidoof","Bibarel","Kricketot","Kricketune","Shinx","Luxio","Luxray","Budew","Roserade","Cranidos","Rampardos","Shieldon","Bastiodon","Burmy","Wormadam","Mothim","Combee","Vespiquen","Pachirisu","Buizel","Floatzel","Cherubi","Cherrim","Shellos","Gastrodon","Ambipom","Drifloon","Drifblim","Buneary","Lopunny","Mismagius","Honchkrow","Glameow","Purugly","Chingling","Stunky","Skuntank","Bronzor","Bronzong","Bonsly","Mime Jr.","Happiny","Chatot","Spiritomb","Gible","Gabite","Garchomp","Munchlax","Riolu","Lucario","Hippopotas","Hippowdon","Skorupi","Drapion","Croagunk","Toxicroak","Carnivine","Finneon","Lumineon","Mantyke","Snover","Abomasnow","Weavile","Magnezone","Lickilicky","Rhyperior","Tangrowth","Electivire","Magmortar","Togekiss","Yanmega","Leafeon","Glaceon","Gliscor","Mamoswine","Porygon-Z","Gallade","Probopass","Dusknoir","Froslass","Rotom","Snivy","Servine","Serperior","Tepig","Pignite","Emboar","Oshawott","Dewott","Samurott","Patrat","Lillipup","Herdier","Stoutland","Purrloin","Liepard","Pansage","Simisage","Pansear","Simisear","Panpour","Simipour","Munna","Musharna","Pidove","Tranquill","Unfezant","Blitzle","Zebstrika","Roggenrola","Boldore","Gigalith","Woobat","Swoobat","Drillbur","Excadrill","Audino","Timburr","Gurdurr","Conkeldurr","Tympole","Palpitoad","Seismitoad","Throh","Sawk","Sewaddle","Swadloon","Leavanny","Venipede","Whirlipede","Scolipede","Cottonee","Whimsicott","Petilil","Lilligante","Basculin","Sandile","Krokorok","Krookodile","Darumaka","Darmanitan","Maractus","Dwebble","Crustle","Scraggy","Scrafty","Sigilyph","Yamask","Cofragigus","Tirtouga","Carracosta","Archen","Archeops","Trubbish","Garbodor","Zorua","Zoroark","Minccino","Cinccino","Gothita","Gothorita","Gothielle","Solosis","Duosion","Reuniclus","Ducklett","Swanna","Vanillite","Vanilluxe","Deerling","Sawsbuck","Emolga","Karrablast","Escavalier","Foongus","Amoonguss","Frillish","Jellicent","Alomomola","Joltik","Galvantula","Ferroseed","Ferrothorn","Klink","Klang","Klinklang","Tynamo","Eelectrik","Eelektross","Elgyem","Beheeyem","Litwick","Lampent","Chandelure","Axew","Fraxure","Haxorus","Cubchoo","Beartic","Cryogonal","Shelmet","Accelgor","Stunfisk","Mienfoo","Mienshao","Druddigon","Golett","Golurk","Pawniard","Bisharp","Bouffalant","Rufflet","Braviary","Vullaby","Mandibuzz","Heatmor","Durant","Deino","Zweilous","Hydreigon"]

REGADDNEST = re.compile(
        r'^Registr(ar|o) (nido|spawn) de ([a-zA-Z0-9\- ]{3,10})'
        r' en ([a-zA-ZÀ-Üà-ü0-9\- \/.•()·\']{3,50})$',
        flags=re.IGNORECASE
)
REGDELNEST = re.compile(
        r'^Eliminar (nido|spawn) n[uú]mero ([0-9]{1,5})$',
        flags=re.IGNORECASE
)
REGFINDNEST = re.compile(
        r'^Buscar (nido|spawn) (de|en) ([a-zA-ZÀ-Üà-ü0-9\- \/.•()·\']{3,50})$',
        flags=re.IGNORECASE
)

@run_async
def new_nest(bot, update):
    logging.debug("%s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    if are_banned(user_id, chat_id):
        return

    user = get_user(user_id)
    if user is None or user.validation_type == ValidationType.NONE:
        bot.sendMessage(
            chat_id=chat_id,
            text="❌ Debes registrarte para usar esta funcionalidad",
            parse_mode=telegram.ParseMode.MARKDOWN
        )
        return

    match = REGADDNEST.match(text)
    if match is None:
    	return

    spawn = match.group(2) 
    pokemon = match.group(3)
    place = match.group(4)
    
    if spawn.lower() == 'nido':
        low = False
    elif spawn.lower() == 'spawn':
        low = True

    if check_pokemon(pokemon) is True:
    	add_nest(user_id, chat_id, pokemon, place, low)
    	output = "Nido registrado correctamente"
    else:
    	output = "❌ no vale "

    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )

@run_async
def erase_nest(bot, update):
    logging.debug("%s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id, chat_id):
        return

    match = REGDELNEST.match(text)
    if match is None:
        return

    numer = match.group(2)
    if delete_nest(chat_id, numer):
        output = "Nido borrado correctamente"
    else:
    	output = "❌ No he encontrado el nido."

    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )

@run_async
def erase_group_nests(bot, update):
    logging.debug("%s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id, chat_id):
        return
    
    if delete_group_nest(chat_id):
        output = "Nidos borrados correctamente"
    else:
        output = "Ha habido un error, prueba en unos minutos"

    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )

@run_async
def list_nests(bot, update):
    logging.debug("%s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    output = check_user(user_id, chat_id, message.from_user.first_name)
    if output is not None:
        if output != "ban":
            sent = bot.send_message(
                chat_id=chat_id,
                text=output,
                parse_mode=telegram.ParseMode.MARKDOWN
            )
            Thread(target=delete_message_timed,
                   args=(chat_id, sent.message_id, 5, bot)).start()
        else:
            return
        ladmin = get_particular_admin(chat_id)
        if ladmin is not None and ladmin.nests:
            admin = get_admin_from_linked(chat_id)
            if admin is not None and admin.nests is True:
                replace_pogo = replace(user_id, message.from_user.first_name)
                message_text=("ℹ️ {}\n👤 {} ha intentado pedir los nidos").format(message.chat.title, replace_pogo)
                bot.sendMessage(
                    chat_id=admin.id,
                    text=message_text,
                    parse_mode=telegram.ParseMode.MARKDOWN
                )
        return

    nidos = get_group_nest(chat_id)
    output = "Listado de nidos en *{}*:\n\n".format(message.chat.title)
    if nidos is None:
        output = "No conozco nidos cerca de aqui, continua explorando."
        bot.sendMessage(
            chat_id=chat_id,
            text=output,
            parse_mode=telegram.ParseMode.MARKDOWN
        )
        return

    for nido in nidos:
        output = output + "- #{} Nido de *{}* en _{}_".format(nido.id, nido.pokemon, nido.place)
        if nido.spawn == True:
            output = output + "🐾"
        output = output + "\n"


    bot.sendMessage(
        chat_id=user_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )

    output = "Me acaban de pedir los nidos.\n¿Alguien quiere una copia?"
    button_list = [[
        InlineKeyboardButton(text="🙋 Yo!", callback_data='send_nests')
    ]]
    reply_markup = InlineKeyboardMarkup(button_list)
    bot.send_message(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN,
        reply_markup=reply_markup
    )
    ladmin = get_particular_admin(chat_id)
    if ladmin is not None and ladmin.nests:
        admin = get_admin_from_linked(chat_id)
        if admin is not None and admin.nests is True:
            replace_pogo = replace(user_id, message.from_user.first_name)
            message_text=("ℹ️ {}\n👤 {} ha pedido los nidos").format(message.chat.title, replace_pogo)
            bot.sendMessage(
                chat_id=admin.id,
                text=message_text,
                parse_mode=telegram.ParseMode.MARKDOWN
            )

@run_async
def list_nests_callback(bot, user, chat_id, message):
    output = check_user(user.id, chat_id, escape_markdown(user.first_name))

    if output is not None:
        if output is "ban":
            return
        ladmin = get_particular_admin(chat_id)
        if ladmin is not None and ladmin.nests:
            admin = get_admin_from_linked(chat_id)
            if admin is not None and admin.nests is True:
                replace_pogo = replace(user.id, user.first_name)
                message_text=("ℹ️ {}\n👤 {} ha intentado pedir los nidos").format(message.chat.title, replace_pogo)
                bot.sendMessage(
                    chat_id=admin.id,
                    text=message_text,
                    parse_mode=telegram.ParseMode.MARKDOWN
                )
        if output != "ban":
            sent = bot.send_message(
                chat_id=chat_id,
                text=output,
                parse_mode=telegram.ParseMode.MARKDOWN
            )
            Thread(target=delete_message_timed,
                   args=(chat_id, sent.message_id, 5, bot)).start()
        return

    nidos = get_group_nest(chat_id)
    output = "Listado de nidos en *{}*:\n\n".format(message.chat.title)
    if nidos is None:
        output = "No conozco nidos cerca de aqui, continua explorando."
    else:
        for nido in nidos:
            output = output + "- #{} Nido de *{}* en _{}_".format(nido.id, nido.pokemon, nido.place)
            if nido.spawn == True:
                output = output + "🐾"
            output = output + "\n"

    bot.sendMessage(
        chat_id=user.id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )
    ladmin = get_particular_admin(chat_id)
    if ladmin is not None and ladmin.nests:
        admin = get_admin_from_linked(chat_id)
        if admin is not None and admin.nests is True:
            replace_pogo = replace(user.id, user.first_name)
            message_text=("ℹ️ {}\n👤 {} ha pedido los nidos").format(message.chat.title, replace_pogo)
            bot.sendMessage(
                chat_id=admin.id,
                text=message_text,
                parse_mode=telegram.ParseMode.MARKDOWN
            )
    return

@run_async
def find_nest(bot, update):
    logging.debug("%s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    
    output = check_user(user_id, chat_id, message.from_user.first_name)
    if output is not None:
        if output is "ban":
            return
        admin = get_admin_from_linked(chat_id)        
        if admin is not None and admin.nests is True:
            replace_pogo = replace(user_id, message.from_user.first_name)
            message_text=("ℹ️ {}\n👤 {} ha intentado pedir los nidos").format(message.chat.title, replace_pogo)
            bot.sendMessage(
                chat_id=admin.id,
                text=message_text,
                parse_mode=telegram.ParseMode.MARKDOWN
        )
        if output != "ban":
            bot.send_message(
                chat_id=chat_id,
                text=output,
                parse_mode=telegram.ParseMode.MARKDOWN
            )
        return

    match = REGFINDNEST.match(text)
    if match is None:
    	return

    var = match.group(2)
    arg = match.group(3)
    output = ""
    if var == 'de' and check_pokemon(arg) is True:
        places = search_pokemon(chat_id, arg)
        logging.debug("%s", places)
        if places is None:
            output = "No se donde vive ahora ese pokemon"
        for place in places:  
            if place.spawn is True:
                output = output + f"\n🐾 *{place.pokemon}* en _{place.place}_"
            else:
                output = output + f"\n*{place.pokemon}* en _{place.place}_"
    else:
        pokemons = search_place(chat_id, arg)
        if pokemons is None:
            output = "No se que hay ahi"        
        for pokemon in pokemons:    
            if pokemon.spawn is True:
                output = output + "\n🐾 " + pokemon.pokemon
            else:
                output = output + "\n" + pokemon.pokemon

    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )    
    admin = get_admin_from_linked(chat_id)        
    if admin is not None and admin.nests is True:
        replace_pogo = replace(user_id, message.from_user.first_name)
        message_text=("ℹ️ {}\n👤 {} ha pedido los nidos").format(message.chat.title, replace_pogo)
        bot.sendMessage(
            chat_id=admin.id,
            text=message_text,
            parse_mode=telegram.ParseMode.MARKDOWN
        ) 

def check_pokemon(parsed_pokemon):
	for pokemon in pokemon_list:
		m = re.match("^%s$" % pokemon.lower(), parsed_pokemon.lower(), flags=re.IGNORECASE)
		if m is not None:
			return True
	return False

def check_user(user_id, group_id, name):
    if are_banned(user_id, group_id):
        return "ban"

    text = None
    user_group = get_user_group(user_id, group_id) 
    if user_group is None:
        set_user_group(user_id, group_id)
    group = get_nests_settings(group_id)
    user = get_user(user_id)

    if user is None or user.validation_type == ValidationType.NONE:
        text="[{}](tg://user?id={}), debes registrarte para usar esta funcionalidad".format(
            name,
            user_id
        )
        return text

    days = (datetime.utcnow() - user_group.join_date).days

    if (user_group.total_messages < group.min_messages or
        days < group.min_days):      
        text="[{}](tg://user?id={}), llevas poco tiempo por aqui forastero.".format(
            name,
            user_id
        )

    return text

@run_async
def link_nests(bot, update, args=None):
    logging.debug('%s %s %s', bot, update, args)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)
    chat_title = message.chat.title

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    if args is None or len(args) != 1 or get_admin(args[0]) is None:
        output = 'Parece ser que has introducido un ID incorrecto'
        bot.sendMessage(chat_id=chat_id, text=output,
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return

    try:
        nest_link(chat_id, args[0])
        output = "Nidos enlazados con éxito"
    except:
        output = (
            "No se ha podido enlazar el grupo, asegurate de que ese ID per"
            "tenezca a un grupo que use Joy y no esté ya enlazado."
        )
    bot.sendMessage(chat_id=chat_id, text=output,
                    parse_mode=telegram.ParseMode.MARKDOWN)

@run_async
def unlink_nests(bot, update, args=None):
    logging.debug('%s %s %s', bot, update, args)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)
    chat_title = message.chat.title

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    if args is None or len(args) != 1 or get_admin(args[0]) is None:
        output = 'Parece ser que has introducido un ID incorrecto'
        bot.sendMessage(chat_id=chat_id, text=output,
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return

    try:
        nest_link(chat_id, args[0], True)
        output = "Nidos desenlazados con éxito"
    except:
        output = (
            "No se ha podido desenlazar este ID. Asegurate de qu"
            "e ese ID esté enlazado a este grupo."
        )
    bot.sendMessage(chat_id=chat_id, text=output,
                    parse_mode=telegram.ParseMode.MARKDOWN)
