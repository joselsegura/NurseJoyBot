# -*- coding: utf-8 -*-

"""
Module cointaing the test cases for checking the main handlers
"""

import os
import unittest
from unittest.mock import MagicMock, patch
from datetime import datetime

import telegram

from nursejoybot.config import get_config
from nursejoybot.nursejoybot import joyping

from test import get_base_path


@patch('nursejoybot.nursejoybot.Thread')
class BotCommandTestCase(unittest.TestCase):
    """
    This TestCase checks the main bot handlers

    Two patches are needed:
    - run_async method from Python Telegram Bot library, due to it decorates
      every handler function
    - start method from Thread class, because patching it we inhibite the spawn
      of new threads
    """

    def setUp(self):
        """

        :return:
        """

        get_config(os.path.join(get_base_path(), 'config/basic_config.ini'))

    @patch('nursejoybot.db.create_engine', lambda *args, **kwargs: MagicMock())
    def test_joyping(self, thread_init_mock):
        """
        This test will ensure that, in different situations, the joyping method
        acts as it is required
        """

        expected_chat_id = 1001
        expected_msg_id = 1
        message_date = datetime(2018, 9, 7, 16, 15, 0, 0)
        fake_now = datetime(2018, 9, 7, 16, 15, 1, 0)

        fake_message = MagicMock()
        fake_message.date = message_date
        fake_message.message_id = expected_msg_id
        fake_message.chat.id = expected_chat_id
        fake_message.chat_id = expected_chat_id
        fake_message.chat.type = "group"  # != 'private'
        fake_message.from_user.id = expected_chat_id

        fake_update = MagicMock()
        fake_update.message = fake_message

        bot_mock = MagicMock()

        # Preparing Thread mock
        thread_mock = MagicMock()
        thread_init_mock.return_value = thread_mock

        with patch('nursejoybot.nursejoybot.datetime') as datetime_mock:
            datetime_mock.now.return_value = fake_now

            joyping.__wrapped__(bot_mock, fake_update)
            bot_mock.deleteMessage.assert_called_with(
                chat_id=expected_chat_id, message_id=expected_msg_id)
            bot_mock.sendMessage.assert_called_with(
                chat_id=expected_chat_id,
                text=(
                    "¿Ves como no era para tanto el pinchazo? Fueron solo 1 "
                    "segundos 🤗"
                ),
                parse_mode=telegram.ParseMode.HTML,
                disable_web_page_preview=True
            )

            # thread_mock.start.assert_called()

        bot_mock.reset_mock()
        thread_mock.reset_mock()

        fake_message.chat.type = 'private'

        with patch('nursejoybot.nursejoybot.datetime') as datetime_mock:
            datetime_mock.now.return_value = fake_now

            joyping.__wrapped__(bot_mock, fake_update)
            bot_mock.deleteMessage.assert_not_called()
            bot_mock.sendMessage.assert_called_with(
                chat_id=expected_chat_id,
                text=(
                    "¿Ves como no era para tanto el pinchazo? Fueron solo 1 "
                    "segundos 🤗"
                ),
                parse_mode=telegram.ParseMode.HTML,
                disable_web_page_preview=True
            )

            # thread_mock.start.assert_not_called()
