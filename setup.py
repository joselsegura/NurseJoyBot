#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

from setuptools import setup, find_packages

DIST_NAME = 'nursejoybot'
VERSION = '0.3.2'


setup(
    name=DIST_NAME,
    packages=find_packages(exclude=['test*']),
    version=VERSION,
    description='Telegram bot for helping Pokémon Go groups management',
    install_requires=[
        'emoji==0.5.1',
        'opencv-python==4.0.0.21',
        'pytesseract==0.2.6',
        'python-Levenshtein==0.12.0',
        'python-telegram-bot==11.1.0',
        'pytz>=2018.9',
        'requests==2.21.0',
        'scikit-image==0.14.2',
        'SQLAlchemy==1.2.17',
    ],
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ],
    entry_points={
        'console_scripts': [
            'nursejoy = nursejoybot.cli:start_bot',
            'nursejoy-create-models = nursejoybot.cli:generate_models'
        ],
    },
    include_package_data=True,
)
