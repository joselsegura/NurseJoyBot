#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV


class Validator:
    validators = []

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        cls.validators.append(cls)


class ValidationData:
    def __init__(self, user_id, user_alias, trainer_name, valid, team, level):
        '''
        Class to handle the Validation data retrieved from another bot
        '''

        self.user_id = user_id
        self.user_alias = user_alias
        self.trainer_name = trainer_name
        self.valid = valid
        self.team = team
        self.level = level

    def __str__(self):
        return (
            'user_id: {}, alias: {}, trainer_name: {}, valid: {}, '
            'team: {}, level: {}'.format(
                self.user_id,
                self.user_alias,
                self.trainer_name,
                self.valid,
                self.team,
                self.level
            )
        )


import nursejoybot.validation.detective_pikachu


def get_validator(bot_id):
    for cls in Validator.validators:
        if cls.BOT_ID == bot_id:
            return cls

    return None
