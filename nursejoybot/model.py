# -*- coding: utf-8 -*-

import enum
import datetime
from itertools import cycle

from sqlalchemy import (
    Column,
    Boolean,
    Integer,
    String,
    UnicodeText,
    BigInteger,
    Enum,
    ForeignKey,
    DateTime,
    Time
)

from nursejoybot.db import get_declarative_base, get_db_engine

Base = get_declarative_base()


@enum.unique
class ValidationRequiered(enum.Enum):
    NO_VALIDATION = 'n'
    VALIDATION = 'v'
    RED_EXCLUSIVE = 'r'
    BLUE_EXCLUSIVE = 'b'
    YELLOW_EXCLUSIVE = 'y'


@enum.unique
class WarnLimit(enum.IntEnum):
    SO_EXTRICT = 3
    EXTRICT = 5
    LOW_PERMISIVE = 10
    MED_PERMISIVE = 25
    HIGH_PERMISIVE = 50
    SO_TOLERANT = 100


@enum.unique
class Team(enum.Enum):
    RED = 'red'
    BLUE = 'blue'
    YELLOW = 'yellow'
    NONE = ''


@enum.unique
class Types(enum.IntEnum):
    TEXT = 0
    BUTTON_TEXT = 1
    STICKER = 2
    DOCUMENT = 3
    PHOTO = 4
    AUDIO = 5
    VOICE = 6
    VIDEO = 7


@enum.unique
class ValidationType(enum.Enum):
    NONE = 'none'
    PIKACHU = 'pikachu'
    INTERNAL = 'internal'
    GOROCHU = 'gorochu'


@enum.unique
class ValidationStep(enum.Enum):
    WAITING_NAME = 'waitingtrainername'
    WAITING_SCREENSHOT = 'waitingscreenshot'
    FAILED = 'failed'
    EXPIRED = 'expired'
    COMPLETED = 'completed'


@enum.unique
class GroupType(enum.Enum):
    RAID = 'raid'
    TRADE = 'trade'
    GENERAL = 'general'
    FRIENDS = 'friends'
    OTHER = 'other'
    ALERTS = 'alerts'
    QUESTS = 'quests'
    YELLOW = 'yellow'
    BLUE = 'blue'
    RED = 'red'
    EX = 'ex'


@enum.unique
class MinLevel(enum.IntEnum):
    NO_LEVEL = 0
    NOOB = 15
    CASUAL = 20
    LOW_PLAYER = 25
    MED_PLAYER = 30
    HIGH_PLAYER = 35
    HARD_PLAYER = 40


@enum.unique
class MinDays(enum.IntEnum):
    NONE = 0
    FIRST = 1
    THIRD = 3
    FIFTH = 5
    WEEK = 7
    FORTNIGHT = 15
    MONTH = 30


@enum.unique
class MinMessages(enum.IntEnum):
    NONE = 0
    WASHINGTON = 1
    LINCOLN = 5
    HAMILTON = 10
    JACKSON = 20
    GRANT = 50
    FRANKLIN = 100


@enum.unique
class Verified(enum.IntEnum):
    NONE = 0
    REQUESTED = 1
    ACCEPTED = 2
    DENIED = 3


@enum.unique
class League(enum.IntEnum):
    SUPER = 0
    ULTRA = 1
    MASTER = 2


@enum.unique
class PoleType(enum.IntEnum):
    POLE = 0
    PLATA = 1
    FAIL = 2
    CANARIA = 3
    PI = 4
    HIGH = 5
    MORNING = 6
    ANDALUZA = 7
    CUSTOM = 8
    NULL = 9


#-------------------------------------

#TO_DELETE
class SettingsNews(Base):
    __tablename__ = 'settingsnews'

    id = Column(BigInteger, primary_key=True)
    news_honesto = Column(Boolean, default=False)
    news_joy = Column(Boolean, default=False)
    news_one = Column(Boolean, default=False)
    news_two = Column(Boolean, default=False)
    news_three = Column(Boolean, default=False)
    
    def set_newset_from_str(self, settings_str):
        '''
        This method takes a settings column name and apply it to the Group
        object
        '''
        if settings_str == 'news_honesto':
            self.news_honesto = not self.news_honesto

        elif settings_str == 'news_joy':
            self.news_joy = not self.news_joy

        elif settings_str == 'news_one':
            self.news_one = not self.news_one

        elif settings_str == 'news_two':
            self.news_two = not self.news_two

        elif settings_str == 'news_three':
            self.news_three = not self.news_three

#-------------------------------------
#--------------------USERS---------------------#
class User(Base):
    __tablename__ = 'users'

    id = Column(BigInteger, primary_key=True)
    username = Column(String(33), unique=True)
    trainer_name = Column(String(20), unique=True)
    language = Column(String(7), default='es_ES')
    level = Column(Integer, nullable=True)
    team = Column(Enum(Team), default=Team.NONE)
    adm_alerts = Column(Boolean, default=False)
    usr_alerts = Column(Boolean, default=False)
    friend_id = Column(String(12), default=None)
    switch_id = Column(String(40), default=None)
    ds_id = Column(String(40), default=None)
    fclists = Column(Boolean, default=False)
    banned = Column(Boolean, default=False)
    validation_type = Column(Enum(ValidationType), default=ValidationType.NONE)
    validation_id = Column(Integer, ForeignKey('validations.id'), unique=True, nullable=True)
    flag = Column(String(12), default=None)
    admin = Column(Integer, default=0)
    me = Column(Integer, default=0)

    def __str__(self):
        return "User <id: {}><username: {}><trainer_name: {}>".format(
            self.id, self.username, self.trainer_name
        )


class UserGoGrupos(Base):
    __tablename__ = 'usersgogrupos'

    id = Column(BigInteger, primary_key=True)
    name = Column(String(33), unique=None)
    friend_id = Column(String(12), default=None)
    location = Column(String(30), unique=None)


class UserGroup(Base):
    __tablename__ = 'usergroup'

    user_id = Column(BigInteger, primary_key=True)
    group_id = Column(BigInteger, ForeignKey('groups.id'), primary_key=True)
    join_date = Column(DateTime, default=datetime.datetime.utcnow)
    last_message = Column(DateTime, default=datetime.datetime.utcnow)
    total_messages = Column(Integer, default=0)
    warn = Column(Integer, default=0)
    left_group = Column(Boolean, default=False)
    pole = Column(BigInteger)


class Validation(Base):
    __tablename__ = 'validations'

    id = Column(Integer, primary_key=True)
    user_id = Column(BigInteger, ForeignKey('users.id'))
    started_time = Column(DateTime, default=datetime.datetime.utcnow)
    step = Column(Enum(ValidationStep), default=ValidationStep.WAITING_NAME)
    tries = Column(Integer, default=0)
    pokemon = Column(String(15))
    pokemon_name = Column(String(15))
    trainer_name = Column(String(20))
    team = Column(Enum(Team), default=Team.NONE)
    level = Column(Integer, nullable=True)


#------------------END USERS-------------------#
#--------------------GROUP---------------------#
class Group(Base):
    __tablename__ = 'groups'

    id = Column(BigInteger, primary_key=True)
    title = Column(String(120))
    language = Column(String(7), default='es_ES')
    timezone = Column(String(60), default='Europe/Madrid')
    spreadsheet = Column(String(100), default=None)
    lat = Column(BigInteger, default=None)
    lng = Column(BigInteger, default=None)
    banned = Column(Boolean, default=False)
    verified = Column(Enum(Verified), default=Verified.NONE)

    def __repr__(self):
        return '<Group(title={}, alias={})>'.format(self.title, self.alias)


class SettingsGroup(Base):
    __tablename__ = 'settings_groups'

    id = Column(BigInteger, primary_key=True)
    jokes = Column(Boolean, default=False)
    games = Column(Boolean, default=False)
    warn = Column(Enum(WarnLimit), default=WarnLimit.HIGH_PERMISIVE)
    hard = Column(Boolean, default=False) #BAN/KICK
    trades = Column(Boolean, default=False)
    pvp = Column(Boolean, default=False)
    notes = Column(Boolean, default=False)
    command = Column(Boolean, default=False)
    reply_on_group = Column(Boolean, default=False)

    def set_settings_from_str(self, settings_str):
        '''
        This method takes a settings column name and apply it to the Group
        object
        '''

        if settings_str == 'jokes':
            self.jokes = not self.jokes

        elif settings_str == 'games':
            self.games = not self.games

        elif settings_str == 'warn':
            valid_warn_limits = cycle(list(WarnLimit))

            for i in valid_warn_limits:
                if self.warn is i:
                    self.warn = next(valid_warn_limits)
                    break
                    
        elif settings_str == 'hard':
            self.hard = not self.hard
                    
        elif settings_str == 'trades':
            self.trades = not self.trades
                    
        elif settings_str == 'pvp':
            self.pvp = not self.pvp
                    
        elif settings_str == 'notes':
            self.notes = not self.notes
                    
        elif settings_str == 'command':
            self.command = not self.command
                    
        elif settings_str == 'reply':
            self.reply_on_group = not self.reply_on_group


class Rules(Base):
    __tablename__ = "rules"
    chat_id = Column(String(14), primary_key=True)
    rules = Column(UnicodeText, default="")

    def __init__(self, chat_id):
        self.chat_id = chat_id

    def __repr__(self):
        return "<Chat {} rules: {}>".format(self.chat_id, self.rules)


class CustomCommands(Base):
    __tablename__ = "custom_commands"

    id = Column(Integer, primary_key=True)
    chat_id = Column(BigInteger, ForeignKey('groups.id'))
    command = Column(String(30), nullable=False)
    command_type = Column(Integer, default=Types.TEXT.value)
    media = Column(UnicodeText, nullable=False)


class CommandsButtons(Base):
    __tablename__ = "commands_urls"

    id = Column(Integer, primary_key=True, autoincrement=True)
    cmd_id = Column(BigInteger, ForeignKey('custom_commands.id'))
    name = Column(UnicodeText, nullable=False)
    url = Column(UnicodeText, nullable=False)
    same_line = Column(Boolean, default=False)

    def __init__(self, cmd_id, name, url, same_line=False):
        self.cmd_id = str(cmd_id)
        self.name = name
        self.url = url
        self.same_line = same_line


class Trades(Base):
    __tablename__ = "trades"

    id = Column(BigInteger, primary_key=True)
    name = Column(String(100))
    city = Column(String(100))
    user_id = Column(BigInteger)
    group_id = Column(BigInteger, ForeignKey('groups.id'))
    have = Column(UnicodeText)
    want = Column(UnicodeText)


class Notes(Base):
    __tablename__ = "notes"

    id = Column(BigInteger, primary_key=True)
    group_id = Column(BigInteger, ForeignKey('groups.id'))
    date = Column(DateTime)
    text = Column(UnicodeText)
    repeat = Column(Integer, default=0)



#------------------END GROUP-------------------#
#--------------------NESTS---------------------#
class LinkedNests(Base):
    __tablename__ = 'linkednests'

    id = Column(BigInteger, primary_key=True)
    group_id = Column(BigInteger, ForeignKey('settingsnests.id'))#Who gets the benefit
    linked_id = Column(BigInteger, ForeignKey('settingsnests.id'))    


class SettingsNests(Base):
    __tablename__ = 'settingsnests'

    id = Column(BigInteger, ForeignKey('groups.id'), primary_key=True)
    nests = Column(Boolean, default=False)
    alerts = Column(Boolean, default=False)
    by_location = Column(Boolean, default=False)
    by_text = Column(Boolean, default=False)
    min_messages = Column(Enum(MinMessages), default=MinMessages.NONE)
    min_days = Column(Enum(MinDays), default=MinDays.NONE)
    rankings = Column(Boolean, default=False)

    def set_nestset_from_str(self, settings_str):
        '''
        This method takes a settings column name and apply it to the Group
        object
        '''
        if settings_str == 'nests':
            self.nests = not self.nests

        elif settings_str == 'alerts':
            self.alerts = not self.alerts

        elif settings_str == 'rankings':
            self.rankings = not self.rankings

        elif settings_str == 'text':
            self.text = not self.text

        elif settings_str == 'location':
            self.location = not self.location

        elif settings_str == 'mindays':
            valid_days_requiereds = cycle(list(MinDays))

            for i in valid_days_requiereds:
                if self.min_days is i:
                    self.min_days = next(valid_days_requiereds)
                    break

        elif settings_str == 'minmessages':
            valid_messages_requiereds = cycle(list(MinMessages))

            for i in valid_messages_requiereds:
                if self.min_messages is i:
                    self.min_messages = next(valid_messages_requiereds)
                    break


class Nests(Base):
    __tablename__ = 'nests'

    id = Column(Integer, primary_key=True)
    user_id = Column(BigInteger)
    group_id = Column(BigInteger)
    lat = Column(BigInteger, default=None)
    lng = Column(BigInteger, default=None)
    pokemon = Column(String(15), default=0)
    place = Column(String(50), default=0)
    spawn = Column(Boolean, default=False)


class Alerts(Base):
    __tablename__ = 'alert'

    id = Column(Integer, primary_key=True)
    user_id = Column(BigInteger)
    group_id = Column(BigInteger)
    lat = Column(BigInteger, default=None)
    lng = Column(BigInteger, default=None)
    pokemon = Column(String(40), default=0)
    place = Column(String(50), default=0)


#------------------END NESTS-------------------#
#--------------------ADMIN---------------------#
class LinkedGroups(Base):
    __tablename__ = 'linkedgroups'

    admin_id = Column(BigInteger, ForeignKey('admingroups.id'))
    linked_id = Column(BigInteger, ForeignKey('groups.id'), primary_key=True)
    title = Column(String(120))
    group_type = Column(Enum(GroupType), default=GroupType.OTHER)
    label = Column(String(60), nullable=True)
    link = Column(String(60), nullable=True)


class SettingsAdmin(Base):#Particular settings of admin group in one group
    __tablename__ = 'settingsadmin'

    id = Column(BigInteger, ForeignKey('groups.id'), primary_key=True)
    welcome = Column(Boolean, default=False)
    goodbye = Column(Boolean, default=False)
    nests = Column(Boolean, default=False)
    globalEjections = Column(Boolean, default=False)
    ejections = Column(Boolean, default=False)
    admin = Column(Boolean, default=False)

    def set_setadm_from_str(self, settings_str):
        '''
        This method takes a settings column name and apply it to the Group
        object
        '''

        if settings_str == 'welcome':
            self.welcome = not self.welcome

        elif settings_str == 'goodbye':
            self.goodbye = not self.goodbye

        elif settings_str == 'nests':
            self.nests = not self.nests

        elif settings_str == 'globalEjections':
            self.globalEjections = not self.globalEjections

        elif settings_str == 'ejections':
            self.ejections = not self.ejections

        elif settings_str == 'admin':
            self.admin = not self.admin


class AdminGroups(Base):#Settings of admin group GENERAL
    __tablename__ = 'admingroups'

    id = Column(BigInteger, ForeignKey('groups.id'), primary_key=True)
    welcome = Column(Boolean, default=False)
    goodbye = Column(Boolean, default=False)
    nests = Column(Boolean, default=False)
    globalEjections = Column(Boolean, default=False)
    ejections = Column(Boolean, default=False)
    admin = Column(Boolean, default=False)
    spy_mode = Column(Boolean, default=False)

    def set_admset_from_str(self, settings_str):
        '''
        This method takes a settings column name and apply it to the Group
        object
        '''

        if settings_str == 'welcome':
            self.welcome = not self.welcome

        elif settings_str == 'goodbye':
            self.goodbye = not self.goodbye

        elif settings_str == 'nests':
            self.nests = not self.nests

        elif settings_str == 'globalEjections':
            self.globalEjections = not self.globalEjections

        elif settings_str == 'ejections':
            self.ejections = not self.ejections

        elif settings_str == 'admin':
            self.admin = not self.admin

        elif settings_str == 'spy_mode':
            self.spy_mode = not self.spy_mode


#------------------END ADMIN-------------------#
#--------------------NEWS----------------------#
class NewsSubs(Base):
    __tablename__ = "news_subs"

    id = Column(BigInteger, primary_key=True)
    user_id = Column(BigInteger)
    news_id = Column(BigInteger, ForeignKey('news.id'))
    status = Column(Boolean, default=False)


class News(Base):
    __tablename__ = "news"

    id = Column(BigInteger, primary_key=True)
    alias = Column(String(60))
    active = Column(Boolean, default=False)


#------------------END NEWS--------------------#
#------------------SETTINGS--------------------#
class SettingsNurse(Base):
    __tablename__ = 'settingsnurse'

    id = Column(BigInteger, primary_key=True)
    admin_too = Column(Boolean, default=False)
    animation = Column(Boolean, default=False)
    command = Column(Boolean, default=False)
    contact = Column(Boolean, default=False)    
    games = Column(Boolean, default=False)
    location = Column(Boolean, default=False)
    photo = Column(Boolean, default=False)
    reply = Column(UnicodeText, nullable=True)
    sticker = Column(Boolean, default=False)
    text = Column(Boolean, default=False)
    urls = Column(Boolean, default=False)
    video = Column(Boolean, default=False)
    voice = Column(Boolean, default=False)
    warn = Column(Boolean, default=False)

    def set_nurseset_from_str(self, settings_str):
        '''
        This method takes a settings column name and apply it to the Group
        object
        '''
        if settings_str == 'url':
            self.urls = not self.urls

        elif settings_str == 'cmd':
            self.command = not self.command

        elif settings_str == 'contact':
            self.contact = not self.contact

        elif settings_str == 'animation':
            self.animation = not self.animation

        elif settings_str == 'photo':
            self.photo = not self.photo

        elif settings_str == 'games':
            self.games = not self.games

        elif settings_str == 'text':
            self.text = not self.text

        elif settings_str == 'sticker':
            self.sticker = not self.sticker

        elif settings_str == 'location':
            self.location = not self.location

        elif settings_str == 'url':
            self.audio = not self.audio

        elif settings_str == 'video':
            self.video = not self.video

        elif settings_str == 'warn':
            self.warn = not self.warn

        elif settings_str == 'admin_too':
            self.admin_too = not self.admin_too


class SettingsJoin(Base):
    __tablename__ = 'settingsjoin'

    id = Column(BigInteger, ForeignKey('groups.id'), primary_key=True)
    levelrequired = Column(Enum(MinLevel), default=MinLevel.NO_LEVEL)
    validationrequired = Column(Enum(ValidationRequiered),
                                default=ValidationRequiered.NO_VALIDATION)
    mute = Column(Boolean, default=False)
    mute_pikachu = Column(Boolean, default=False)
    pikachu = Column(Boolean, default=False)
    joy = Column(Boolean, default=False)
    max_members = Column(Integer, default=0)
    delete_cooldown = Column(Integer, default=0)
    silence = Column(Boolean, default=False)
    valpikachu = Column(Boolean, default=False)

    def set_joinset_from_str(self, settings_str):
        '''
        This method takes a settings column name and apply it to the Group
        object
        '''
        if settings_str == 'level':
            valid_level_requiereds = cycle(list(MinLevel))

            for i in valid_level_requiereds:
                if self.levelrequired is i:
                    self.levelrequired = next(valid_level_requiereds)
                    break

        elif settings_str == 'val':
            valid_validation_requiereds = cycle(list(ValidationRequiered))

            for i in valid_validation_requiereds:
                if self.validationrequired is i:
                    self.validationrequired = next(valid_validation_requiereds)
                    break

        elif settings_str == 'mute':
            self.mute = not self.mute

        elif settings_str == 'pika':
            self.pikachu = not self.pikachu

        elif settings_str == 'joy':
            self.joy = not self.joy

        elif settings_str == 'silence':
            self.silence = not self.silence

        elif settings_str == 'valpika':
            self.valpikachu = not self.valpikachu
            
        elif settings_str == 'pikamute':
            self.mute_pikachu = not self.mute_pikachu


#----------------END SETTINGS------------------#
#-------------------WELCOME--------------------#
class Welcome(Base):
    __tablename__ = "welcome"
    chat_id = Column(String(14), primary_key=True)
    should_welcome = Column(Boolean, default=False)
    custom_welcome = Column(UnicodeText, nullable=True)
    welcome_type = Column(Integer, default=Types.TEXT.value)
    timer = Column(Integer, default=0)

    def __init__(self, chat_id):
        self.chat_id = chat_id

    def __repr__(self):
        return "<Chat {} should Welcome new users: {}>".format(self.chat_id, self.should_welcome)
    
    def set_welcomeset_from_str(self):
        '''
        This method takes a settings column name and apply it to the Group
        object
        '''
        self.should_welcome = not self.should_welcome


class WelcomeButtons(Base):
    __tablename__ = "welcome_urls"

    id = Column(Integer, primary_key=True, autoincrement=True)
    chat_id = Column(String(14), primary_key=True)
    name = Column(UnicodeText, nullable=False)
    url = Column(UnicodeText, nullable=False)
    same_line = Column(Boolean, default=False)

    def __init__(self, chat_id, name, url, same_line=False):
        self.chat_id = str(chat_id)
        self.name = name
        self.url = url
        self.same_line = same_line


#-----------------END WELCOME------------------#
#---------------------API----------------------#
class Bidoof(Base):
    __tablename__ = "bidoof"

    id = Column(BigInteger, primary_key=True)
    name = Column(String(120))
    alias = Column(String(60))
    api_key = Column(String(64))
    active = Column(Boolean, default=False)


class BidoofUsers(Base):
    __tablename__ = "bidoof_users"

    id = Column(BigInteger, primary_key=True)
    user_id = Column(BigInteger, ForeignKey('users.id'))
    bot_id = Column(BigInteger, ForeignKey('bidoof.id'))
    status = Column(Enum(Verified), default=Verified.NONE)


#-------------------END API--------------------#
#--------------------SAFARI--------------------#
class Safari(Base):
    __tablename__ = "safari"

    id = Column(Integer, primary_key=True)
    user_id = Column(BigInteger, ForeignKey('users.id'), nullable=True)
    pokemon_id = Column(Integer, nullable=False)
    gender = Column(Integer, nullable=False)
    shiny = Column(Boolean, default=False)
    level = Column(Integer, nullable=False)
    atk_iv = Column(Integer, nullable=False)
    def_iv = Column(Integer, nullable=False)
    sta_iv = Column(Integer, nullable=False)
    cp = Column(Integer, nullable=False)


class SettingsSafari(Base):
    __tablename__ = "settings_safari"

    id = Column(BigInteger, ForeignKey('groups.id'), primary_key=True)
    pokegram = Column(Boolean, default=False)
    allow_spawn = Column(Boolean, default=False)
    allow_stops = Column(Boolean, default=False)
    allow_battles = Column(Boolean, default=False)
    magikarp_jump = Column(Boolean, default=False)


class SafariBag(Base):
    __tablename__ = "safari_bag"

    id = Column(BigInteger, ForeignKey('users.id'), primary_key=True)
    pokeball = Column(Integer, nullable=False, default=0)
    superball = Column(Integer, nullable=False, default=0)
    ultraball = Column(Integer, nullable=False, default=0)
    honorball = Column(Integer, nullable=False, default=0)
    masterball = Column(Integer, nullable=False, default=0)
    rod = Column(Integer, nullable=False, default=0)
    lure = Column(Integer, nullable=False, default=0)
    level = Column(Integer, nullable=False, default=0)
    xp = Column(Integer, nullable=False, default=0)


class SafariQuests(Base):
    __tablename__ = "safari_quests"

    id = Column(BigInteger, primary_key=True)
    quest_name = Column(UnicodeText, nullable=False)
    quest_def = Column(UnicodeText, nullable=False)


class TrainerQuests(Base):
    __tablename__ = "trainer_quests"

    id = Column(BigInteger, primary_key=True)
    user_id = Column(BigInteger, ForeignKey('users.id'))
    quest_id = Column(BigInteger, ForeignKey('safari_quests.id'))


#------------------END SAFARI------------------#
#---------------------PVP----------------------#
class PvPBattles(Base):
    __tablename__ = "pvp_battles"

    id = Column(BigInteger, primary_key=True)
    first_id = Column(BigInteger, ForeignKey('users.id'))
    second_id = Column(BigInteger, ForeignKey('users.id'))
    winner = Column(BigInteger, ForeignKey('users.id'))


class PvPList(Base):
    __tablename__ = "pvp_list"

    id = Column(BigInteger, primary_key=True)
    battle_id = Column(BigInteger, ForeignKey('pvp.id'))
    user_id = Column(BigInteger, ForeignKey('users.id'))
    score = Column(Integer)
    lost = Column(Boolean, default=False)


class PvP(Base):
    __tablename__ = "pvp"

    id = Column(BigInteger, primary_key=True)
    league = Column(Integer, default=League.SUPER.value)
    date = Column(DateTime, nullable=True)
    max_players = Column(Integer, default=0)
    winners = Column(Boolean, default=False)  
    rules = Column(Boolean, default=False) #False = league | True = eliminatory
    text = Column(UnicodeText, default=None)


#-------------------END PVP--------------------#
#---------------------POLE---------------------#
class SettingsPole(Base):
    __tablename__ = "pole_settings"

    id = Column(BigInteger, ForeignKey('groups.id'), primary_key=True)
    pole = Column(Boolean, default=False)
    canaria = Column(Boolean, default=False)
    pi = Column(Boolean, default=False)
    high = Column(Boolean, default=False)
    morning = Column(Boolean, default=False)
    andaluza = Column(Boolean, default=False)
    hard = Column(Boolean, default=False)
    custom = Column(Boolean, default=False)
    random = Column(Boolean, default=False)
    time = Column(Time, nullable=True)


class Pole(Base):
    __tablename__ = "pole"

    id = Column(BigInteger, primary_key=True)
    group_id = Column(BigInteger, ForeignKey('groups.id'))
    user_id = Column(BigInteger, ForeignKey('users.id'))
    name = Column(UnicodeText, default=None)
    place = Column(Enum(PoleType), default=PoleType.NULL)
    time = Column(DateTime, default=datetime.datetime.utcnow)


#-------------------END POLE-------------------#
#-------------------LOCATIONS------------------#
class Locations(Base):
    __tablename__ = "locations"

    id = Column(BigInteger, primary_key=True)
    group_id = Column(BigInteger, ForeignKey('groups.id'))
    name = Column(String(128), nullable=False)
    latitude = Column(String(20), nullable=False)
    longitude = Column(String(20), nullable=False)
    keywords = Column(String(512), nullable=False)
    tags = Column(String(45), default=None)
    address = Column(String(128), default=None)
#-----------------END LOCATIONS----------------#
#----------------------------------------------#

def create_databases():
    global Base

    engine = get_db_engine()
    Base.metadata.create_all(engine)
