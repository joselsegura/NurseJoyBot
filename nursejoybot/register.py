#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import os
import re
import logging
import random
import time
import urllib.request

import telegram
from telegram.ext.dispatcher import run_async

from nursejoybot.model import (
    User,
    Team,
    Validation,
    ValidationType,
    ValidationStep
)
from nursejoybot.profile_parser import ProfileParseException
from nursejoybot.config import get_config
from nursejoybot.supportmethods import (
    extract_update_info,
    ensure_escaped,
    parse_profile_image,
    delete_message
)
from nursejoybot.storagemethods import (
    are_banned,
    active_validation,
    get_user,
    set_user,
    new_validation,
    waiting_trainer,
    waiting_picture,
    add_trainername,
    commit_user,
    update_level,
    owned_trainername,
    expire_validation,
    increase_validation,
    complete_validation,
    set_fc,
    set_dsfc,
    set_swichfc,
    update_fclist,
    update_mentions
)
from nursejoybot.validation import get_validator
from nursejoybot.gorochu import GorochuAPI, GorochuError
from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup
)

REGPRIV = re.compile(
        r'privacity_(bot|fc|back|men)_([a-z]{2,10})'
)

#--------------------Gorochuu---------------------#
#--------------------Gorochuu---------------------#
@run_async
def register(bot, update):
    logging.debug("nursejoybot:register: %s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    user_username = message.from_user.username

    if are_banned(user_id, chat_id):
        return
    try:
        if user_username is None:
            bot.sendMessage(
                chat_id=chat_id,
                text=(
                    "❌ Es necesario que configures un alias en Telegram antes de registrarte."
                ),
                parse_mode=telegram.ParseMode.MARKDOWN
            )
            return
    except:
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "❌ Es necesario que configures un alias en Telegram antes de registrarte."
            ),
            parse_mode=telegram.ParseMode.MARKDOWN
        )
        return


    validation = active_validation(user_id)
    
    if validation is not None:
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "❌ Ya has iniciado un proceso de validación. Debes completarlo"
                " antes de intentar comenzar de nuevo, o esperar 6 horas a que"
                " caduque."
            ),
            parse_mode=telegram.ParseMode.MARKDOWN
        )
        return

    config = get_config()
    endpoint = config['gorochu']['pikachu_endpoint']
    token = config['gorochu']['pikachu_token']
    gorochu = GorochuAPI(endpoint, token)

    try:
        if gorochu.is_registered(user_id):
            button_list = [[
                InlineKeyboardButton(text="✅ De acuerdo!", callback_data='gorochu_start'),
                InlineKeyboardButton(text="❌ No me interesa", callback_data='validation_start')
            ]]
            reply_markup = InlineKeyboardMarkup(button_list)
            bot.send_message(
                chat_id=chat_id,
                text=(
                    "¡Bienvenido @{} !\nIba a pedirte tus datos de Entrenado"
                    "r para rellenar la Ficha, pero @detectivepikachubot me acaba d"
                    "e decir que conoce tus datos.\n¿Quieres que le pida a Detectiv"
                    "e Pikachu los datos y así acelerar el proceso de registro?\nEn"
                    " caso contrario, iniciarás el proceso de validación mediante "
                    "captura.".format(ensure_escaped(user_username))
                    ),
                parse_mode=telegram.ParseMode.MARKDOWN,
                reply_markup=reply_markup
            )
        else:
            start_register(bot, user_id, user_username)


    except GorochuError:
        start_register(bot, user_id, user_username)

    except ConnectionError:
        start_register(bot, user_id, user_username)        

@run_async
def start_gorochu(bot, user_id, user_username):
    config = get_config()
    endpoint = config['gorochu']['pikachu_endpoint']
    token = config['gorochu']['pikachu_token']
    gorochu = GorochuAPI(endpoint, token)

    status = gorochu.is_authorized(user_id)
    if status.value != 'denied' and status.value != 'accepted':
        if gorochu.request_authorization(user_id):
            button_list = [[
                InlineKeyboardButton(text="✅ Actualizar", callback_data='gorochu_refresh'),
                InlineKeyboardButton(text="❌ Cancelar", callback_data='gorocu_cancel')
            ]]
            reply_markup = InlineKeyboardMarkup(button_list)
            bot.send_message(
                chat_id=user_id,
                text=(
                    "Pulsa ✅ **Actualizar** cuando hayas aceptado el envío de d"
                    "atos con Detective Pikachu. Si quieres detener el proceso, "
                    "pulsa ❌ **Cancelar**"
                    ),
                parse_mode=telegram.ParseMode.MARKDOWN,
                reply_markup=reply_markup
            )

        else:
            button_list = [[
                InlineKeyboardButton(text="✅ Volver a preguntar", callback_data='gorochu_start'),
                InlineKeyboardButton(text="❌ Cancelar", callback_data='gorocu_cancel')
            ]]
            reply_markup = InlineKeyboardMarkup(button_list)
            bot.send_message(
                chat_id=user_id,
                text=(
                    "❌ Parece ser que no tienes una conversacion iniciada con D"
                    "etective Pikachu. Dirigete a su chat y pulsa **Start**. Lu"
                    "ego regresa para volver a preguntar. De lo contrario, abor"
                    "ta con el botón Cancelar"
                    ),
                parse_mode=telegram.ParseMode.MARKDOWN,
                reply_markup=reply_markup
            )
    elif status.value == 'accepted':
        data = gorochu.get_data(user_id)
        logging.debug("%s", data)

        user = get_user(user_id)
        if user is None:
            try:
                set_user(user_id, user_username)
            except:
                bot.sendMessage(
                    chat_id=user_id,
                    text=("Parece ser que tu alias ya está registrado. E"
                        "ntra en [el grupo de ayuda a validaciones]({}) para continuar.".format(
                            config["telegram"]["helpgroup"])),
                    parse_mode=telegram.ParseMode.MARKDOWN)
                return

        for key, value in data.items():
            if key == 'trainername':
                trainername = value
            if key == 'status':
                if value == 'ok':
                    validation = ValidationType.GOROCHU
                elif value == 'incomplete':
                    validation = ValidationType.NONE
                elif value == 'banned':
                    validation = ValidationType.NONE
                    banID(user_id, user_username)
            if key == 'level':
                level = value
            if key == 'team':
                if value == 'mystic':
                    team = Team.BLUE
                elif value == 'valor':
                    team = Team.RED
                elif value == 'instinct':
                    team = Team.YELLOW               
        try:
            commit_user(
                user_id,
                trainername,
                team,
                validation,
                level)

            bot.sendMessage(
                chat_id=user_id,
                text=(
                    "👌 Has completado el proceso de validación correctamente.\n"
                    "*{0}*, se te ha asignado el equipo *{1}* y el nivel *{2}*.\n\n"
                    "A partir de ahora aparecerán tu nivel y equipo reflejados "
                    "en tu Ficha de Entrenador.\n\nSi subes de "
                    "nivel en el juego y quieres que se refleje en tu Ficha, "
                    "puedes enviarme en cualquier momento otra captura de tu "
                    "perfil del juego, no es necesario que cambies tu Pokémon "
                    "acompañante.".format(
                        ensure_escaped(trainername),
                        team, level
                    )
                ),
                parse_mode=telegram.ParseMode.MARKDOWN
            )
        except:
            bot.sendMessage(
                chat_id=user_id,
                text=("Parece ser que tu nombre de entrenador ya está registrado. E"
                    "ntra en [el grupo de ayuda a validaciones]({}) para continuar.".format(
                        config["telegram"]["helpgroup"])),
                parse_mode=telegram.ParseMode.MARKDOWN)

    elif status.value == 'denied':
        #blockGorochu(user_id)
        start_register(bot, user_id, user_username)
        return


@run_async
def refresh_gorochu(bot, user_id, user_username):
    config = get_config()
    endpoint = config['gorochu']['pikachu_endpoint']
    token = config['gorochu']['pikachu_token']
    gorochu = GorochuAPI(endpoint, token)

    status = gorochu.is_authorized(user_id)

    if status.value == 'denied':
        #blockGorochu(user_id)
        start_register(bot, user_id, user_username)
        return
    elif status.value == 'waiting':
        button_list = [[
            InlineKeyboardButton(text="✅ Actualizar", callback_data='gorochu_refresh'),
            InlineKeyboardButton(text="❌ Cancelar", callback_data='gorocu_cancel')
        ]]
        reply_markup = InlineKeyboardMarkup(button_list)
        bot.send_message(
            chat_id=user_id,
            text=(
                "❌ Aún no has aceptado la solicitud en @detectivepikachubot."
                ),
            parse_mode=telegram.ParseMode.MARKDOWN,
            reply_markup=reply_markup
        )
        return
    elif status.value == 'unknown':
        startGorochu(bot, user_id, user_username)
    elif status.value == 'accepted':
        data = gorochu.get_data(user_id)
        logging.debug("%s", data)

        user = get_user(user_id)
        if user is None:
            try:
                set_user(user_id, user_username)
            except:
                bot.sendMessage(
                    chat_id=user_id,
                    text=("Parece ser que tu alias ya está registrado. E"
                        "ntra en [el grupo de ayuda a validaciones]({}) para continuar.".format(
                            config["telegram"]["helpgroup"])),
                    parse_mode=telegram.ParseMode.MARKDOWN)
                return

        for key, value in data.items():
            if key == 'trainername':
                trainername = value
            if key == 'status':
                if value == 'ok':
                    validation = ValidationType.GOROCHU
                elif value == 'incomplete':
                    validation = ValidationType.NONE
                elif value == 'banned':
                    validation = ValidationType.NONE
                    banID(user_id, user_username)
            if key == 'level':
                level = value
            if key == 'team':
                if value == 'mystic':
                    team = Team.BLUE
                elif value == 'valor':
                    team = Team.RED
                elif value == 'instinct':
                    team = Team.YELLOW               
        try:
            commit_user(
                user_id,
                trainername,
                team,
                validation,
                level)

            bot.sendMessage(
                chat_id=user_id,
                text=(
                    "👌 Has completado el proceso de validación correctamente.\n"
                    "*{0}*, se te ha asignado el equipo *{1}* y el nivel *{2}*.\n\n"
                    "A partir de ahora aparecerán tu nivel y equipo reflejados "
                    "en tu Ficha de Entrenador.\n\nSi subes de "
                    "nivel en el juego y quieres que se refleje en tu Ficha, "
                    "puedes enviarme en cualquier momento otra captura de tu "
                    "perfil del juego, no es necesario que cambies tu Pokémon "
                    "acompañante.".format(
                        ensure_escaped(trainername),
                        team, level
                    )
                ),
                parse_mode=telegram.ParseMode.MARKDOWN
            )
        except:
            bot.sendMessage(
                chat_id=user_id,
                text=(
                    "Parece ser que tu nombre de entrenador ya está registrado. E"
                    "ntra en [el grupo de ayuda a validaciones]({}) para continuar.".format(
                        config["telegram"]["helpgroup"]
                    )
                ),
                parse_mode=telegram.ParseMode.MARKDOWN
            )



@run_async
def start_register(bot, user_id, user_username):
    logging.debug("%s %s", user_id, user_username)

    user = get_user(user_id)
    if user is not None and user.validation_type != ValidationType.NONE:
        bot.sendMessage(
            chat_id=user_id,
            text=(
                "Según los datos que tengo ya hicimos anteriormente tu Ficha "
                "de Entrenador. \n A no ser que quieras cambiar tu nombre de "
                "entrenador, bajar de nivel o cambiar de equipo, no necesitas"
                " volver a validarte. \n Si quieres informarme de que has sub"
                "ido de nivel, simplemente mándame una captura de pantalla de"
                " tu perfil de entrenador y te ahorrarás volver a pasar por e"
                "l proceso de validación. \n\n Si aún así quieres, puedes con"
                "tinuar con el proceso, o sino *espera 6 horas* a que caduque."
            ),
            parse_mode=telegram.ParseMode.MARKDOWN
        )

    elif user is None:
        try:
            set_user(user_id, user_username)
        except:
            bot.sendMessage(
                chat_id=user_id,
                text=("Parece ser que tu alias ya está registrado. E"
                    "ntra en [el grupo de ayuda a validaciones]({}) para continuar.".format(
                        config["telegram"]["helpgroup"])),
                parse_mode=telegram.ParseMode.MARKDOWN)
            return


    new_validation(user_id)    
    bot.sendMessage(
        chat_id=user_id,
        text=(
            "Para hacer tu Ficha de Entrenador voy a necesitar un par de datos..."
            "\n No te preocupes que es algo muy breve. \n\n El primer dato que ne"
            "cesito es el siguiente: ¿Qué nombre de entrenador utilizas en el juego?"
        ),
        parse_mode=telegram.ParseMode.MARKDOWN
    )
#-----------START Process Text Messages-----------#

@run_async
def process_private_message(bot, update):
    '''
    Function that handles a message received from a user
    '''

    logging.debug("private message received")
    user_id = update.message.chat.id
    if are_banned(user_id, user_id):
        return

    # Is this a forwarded message from some bot?

    if update.message.forward_from and update.message.forward_from.is_bot:
        register_from_bot(bot, update)
        return

    # Is on a validation process?
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    trainername = re.match(r'[a-zA-Z0-9_]{3,15}$', text)
    logging.debug("")
    if (not waiting_trainer(user_id)) or trainername is None:
        return

    else:
        logging.info("Sending trainer name")
        process_trainer_name(bot, update, trainername)
        return


@run_async
def process_trainer_name(bot, update, trainername):
    logging.info("%s", update.message.text)

    user_id = update.message.chat.id

    if owned_trainername(update.message.text, user_id):
        bot.sendMessage(
        chat_id=user_id,
        text=("Parece ser que tu nombre de entrenador ya está registrado. E"
            "ntra en [el grupo de ayuda a validaciones]({}) para continuar.".format(
                config["telegram"]["helpgroup"])),
        parse_mode=telegram.ParseMode.MARKDOWN)

    add_trainername(user_id, update.message.text)
    validation = active_validation(user_id)

    bot.sendMessage(
        chat_id=user_id,
        text=(
            "Así que tu nombre de entrenador es *{}*.\n\n"
            "Para completar el registro, debes enviarme "
            "una captura de pantalla de tu perfil del "
            "juego. En la de pantalla debes tener "
            "un *{}* llamado *{}* como compañero. Si no "
            "tienes ninguno, o no te apetece cambiar ahora"
            " de compañero, puedes volver a comenzar el "
            "registro en cualquier otro momento.\n\n*"
            "Recuerda que mandar imagenes que no sean de "
            "Pokémon GO es motivo de baneo*".format(
            validation.trainer_name,
            validation.pokemon.capitalize(),
            validation.pokemon_name)
            ),
        parse_mode=telegram.ParseMode.MARKDOWN
    )


@run_async 
def register_from_bot(bot, update):
    '''
    This function will try to register a new user using any existing validator
    for other bots profile functions
    '''

    logging.debug("nursejoybot:register_from_bot: %s %s", bot, update)

    chat_id, _, user_id, text, message = extract_update_info(update)
    this_date = message.date
    user_id = update.message.chat.id
    forward_date = message.forward_date
    forward_id = message.forward_from.id

    if are_banned(user_id, user_id):
        return

    config = get_config()
    validator = get_validator(forward_id)

    if validator:
        if (this_date - forward_date).total_seconds() < 120:
            validation_data = validator().validate(text)

            if not validation_data:
                bot.sendMessage(
                    chat_id=chat_id,
                    text=(
                        "❌ No he reconocido ese mensaje de {}. ¿Seguro que"
                        " le has pedido tu perfil correctamente?".format(
                            validator.BOT_NAME
                        )
                    ),
                    parse_mode=telegram.ParseMode.MARKDOWN
                )

            if validation_data.valid:
                # The trainer is already registered, but different Telegram ID
                logging.debug("checking owned_trainername")
                if owned_trainername(validation_data.trainer_name, user_id):
                    bot.sendMessage(
                        chat_id=chat_id,
                        text=(
                            "Parece ser que tu nombre de entrenador ya está registrado. E"
                            "ntra en [el grupo de ayuda a validaciones]({}) para continuar.".format(
                                config["telegram"]["helpgroup"]
                        )),
                        parse_mode=telegram.ParseMode.MARKDOWN
                    )
                    return

                logging.debug("checked owned_trainername")
                # The forwarded message is not from this user
                if user_id != validation_data.user_id:
                    bot.sendMessage(
                        chat_id=chat_id,
                        text=(
                            "❌ El mensaje que me has reenviado no "
                            "corresponde con tu usuario de Telegram. ¿Te "
                            "has equivocado al reenviar el mensaje? Pide "
                            "ayuda en @enfermerajoyayuda indicando tu "
                            "alias en telegram y tu nombre de entrenador "
                            "en el juego para que revisemos el caso "
                            "manualmente."
                        ),
                        parse_mode=telegram.ParseMode.MARKDOWN
                    )
                    return

                user = get_user(user_id)

                if validation_data.team == 'Valor':
                        validation_data.team = Team.RED
                elif validation_data.team == 'Instinto':
                    validation_data.team = Team.YELLOW
                elif validation_data.team == 'Sabiduría':
                    validation_data.team = Team.BLUE

                logging.debug("%s", validation_data)

                # The user doesn't exist on the database
                if user is None:

                    logging.debug("User None")

                    set_user(user_id, validation_data.user_alias)
                    commit_user(
                        user_id,
                        validation_data.trainer_name,
                        validation_data.team,
                        ValidationType.PIKACHU,
                        validation_data.level
                        )
                    user = get_user(user_id)
                    bot.sendMessage(
                        chat_id=chat_id,
                        text=(
                            "👌 Has completado el proceso de validación correctamente."
                            "\n*{0}*, se te ha asignado el equipo *{1}* y el nivel *{2}*.\n\n"
                            "A partir de ahora aparecerán tu nivel y equipo reflejados "
                            "en tu Ficha de Entrenador.\n\nSi subes de "
                            "nivel en el juego y quieres que se refleje en tu Ficha, "
                            "puedes enviarme en cualquier momento otra captura de tu "
                            "perfil del juego, no es necesario que cambies tu Pokémon "
                            "acompañante.".format(
                                ensure_escaped(user.trainer_name),
                                user.team, user.level
                            )
                        ),
                        parse_mode=telegram.ParseMode.MARKDOWN
                    )

                
                #Exists user but no registered.
                if user.validation_type == ValidationType.NONE:

                    logging.debug("User NOT None and not registered")

                    commit_user(
                        user_id,
                        validation_data.trainer_name,
                        validation_data.team,
                        ValidationType.PIKACHU,
                        validation_data.level
                        )
                    user = get_user(user_id)
                    bot.sendMessage(
                        chat_id=chat_id,
                        text=(
                            "👌 Has completado el proceso de validación correctamente.\n"
                            "*{0}*, se te ha asignado el equipo *{1}* y el nivel *{2}*.\n\n"
                            "A partir de ahora aparecerán tu nivel y equipo reflejados "
                            "en tu Ficha de Entrenador.\n\nSi subes de "
                            "nivel en el juego y quieres que se refleje en tu Ficha, "
                            "puedes enviarme en cualquier momento otra captura de tu "
                            "perfil del juego, no es necesario que cambies tu Pokémon "
                            "acompañante.".format(
                                ensure_escaped(user.trainer_name),
                                user.team, user.level
                            )
                        ),
                        parse_mode=telegram.ParseMode.MARKDOWN
                    )

                # Already registered with same trainer name. Maybe update?
                elif user.trainer_name == validation_data.trainer_name:
                    if user.level < validation_data.level:
                        logging.debug("update level")
                        update_level(validation_data.level, user_id)
                        logging.debug("level updated")
                        user = get_user(user_id)
                        bot.sendMessage(
                            chat_id=chat_id,
                            text=(
                                "👌 ¡De acuerdo {}! Ya veo que has subido de "
                                "nivel. Voy a actualizar mis datos.\n\nA "
                                "partir de ahora aparecerá tu nuevo nivel "
                                "cuando alguien pregunte por ti en los grupos"
                                .format(ensure_escaped(user.trainer_name))
                            ),
                            parse_mode=telegram.ParseMode.MARKDOWN)
                    else:
                        bot.sendMessage(
                            chat_id=chat_id,
                            text=(
                                "❌ Lo siento, pero no he detectado ninguna "
                                "subida de nivel."
                            ),
                            parse_mode=telegram.ParseMode.MARKDOWN)
                elif user.trainer_name != validation_data.trainer_name:
                    logging.debug("OLD USER, new account")
                    commit_user(
                        user_id,
                        validation_data.trainer_name,
                        validation_data.team,
                        ValidationType.PIKACHU,
                        validation_data.level
                        )
                    user = get_user(user_id)
                    bot.sendMessage(
                        chat_id=chat_id,
                        text=(
                            "👌 Has completado el proceso de validación correctamente.\n"
                            "{0}, se te ha asignado el equipo *{1}* y el nivel *{2}*.\n\n"
                            "A partir de ahora aparecerán tu nivel y equipo reflejados "
                            "en tu Ficha de Entrenador.\n\nSi subes de "
                            "nivel en el juego y quieres que se refleje en tu Ficha, "
                            "puedes enviarme en cualquier momento otra captura de tu "
                            "perfil del juego, no es necesario que cambies tu Pokémon "
                            "acompañante.".format(
                                ensure_escaped(user.trainer_name),
                                user.team, user.level
                            )
                        ),
                        parse_mode=telegram.ParseMode.MARKDOWN)

                validate = active_validation(user_id)
                if validate is not None:
                    expire_validation(replied_userid)

            # User exists but something went wrong
            else:
                logging.debug(
                    "Something went wrong: user object:%s - validation data: %s",
                    user, validation_data
                )
                bot.sendMessage(
                    chat_id=chat_id,
                    text=(
                        "❌ Parece que tu cuenta aún no está completamente "
                        "validada con Pikachu. No puedo aceptar "
                        "tu nivel y equipo hasta que te valides."
                    ),
                    parse_mode=telegram.ParseMode.MARKDOWN
                )

        else:
            bot.sendMessage(
                chat_id=chat_id,
                text=(
                    "❌ Ese mensaje es demasiado antiguo. ¡Debes reenviarme"
                    " un mensaje más reciente!"
                ),
                parse_mode=telegram.ParseMode.MARKDOWN
            )

    else:
        bot.sendMessage(
            chat_id=chat_id,
            text="❌ No he reconocido el bot que envía ese mensaje. ",
            parse_mode=telegram.ParseMode.MARKDOWN
        )

#------------END Process Text Messages------------#

#---------------START Photo Process---------------#

@run_async
def process_photo_private(bot, update):
    logging.debug("Photo received from %s", update.message.chat.id)

    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    config = get_config()

    if are_banned(chat_id, user_id):
        return

    validation = waiting_picture(user_id)
    if validation is not None and validation.step == ValidationStep.WAITING_SCREENSHOT:
        logging.info("Sending trainer photo")
        process_trainer_photo(bot, update, validation)
        return
    
    if validation and validation.step == ValidationStep.FAILED:
        return

    user = get_user(user_id)

    if user and user.validation_type != ValidationType.NONE:
        photo = bot.get_file(update.message.photo[-1]["file_id"])
        logging.debug("Downloading file %s", photo)

        filename = os.path.join(
            os.path.expanduser(config['general']['photos']),
            "profile-{}-update_level-{}.jpg".format(
            user_id, int(time.time())
            )
        )
        urllib.request.urlretrieve(photo["file_path"], filename)
        logging.debug("Storing received file in %s", filename)

        store_base_filename = '{}_{}'.format(
            time.time(),
            random.randrange(100, 999)
        )
        try:
            profile = parse_profile_image(
                filename,
                None,
                store_path=config['general']['inspectdir'],
                store_base_name=store_base_filename
            )
            output = None
            if profile.matched_model is None:
                output = (
                    "❌ La captura de pantalla no parece válida. Asegúrate de "
                    "enviar una captura de pantalla completa del juego en un "
                    "teléfono móvil. No son válidas las capturas en tablets ni"
                    " otros dispositivos ni capturas recortadas o alteradas. "
                    "Puedes volver a intentarlo enviando otra captura. Si no "
                    "consigues que la reconozca, <a href='%s'>entra al "
                    "grupo de asistencia a subidas de niveles</a>"
                    "y nuestro equipo de administradores revisará tu caso manualmente."
                    % (config["telegram"]["helpgroup"])
                )

            elif not profile.check_name(user.trainer_name):
                output = (
                    "❌ No he reconocido correctamente el <b>nombre del "
                    "entrenador</b>. Si no consigues que la reconozca, <a href='%s'>entra al "
                    "grupo de asistencia a subidas de niveles</a>"
                    " y nuestro equipo de administradores revisará tu caso manualmente."
                    % (config["telegram"]["helpgroup"])
                )

            elif profile.level is None:
                output = (
                    "❌ No he reconocido correctamente el <b>nivel</b>. Si no "
                    "consigues que la reconozca, <a href='%s'>entra al "
                    "grupo de asistencia a subidas de niveles</a>"
                    " y nuestro equipo de administradores revisará tu caso manualmente."
                    % (config["telegram"]["helpgroup"])
                )

            elif user.level == profile.level:
                output = (
                    "❌ En la captura pone que eres <b>nivel {0}</b>, pero yo ya "
                    "sabía que tenías ese nivel.".format(user.level)
                )

            elif user.level > profile.level:
                output = (
                    "❌ En la captura pone que eres <b>nivel {0}</b>, pero ya eras "
                    "<b>nivel {1}</b>. ¿Has estado jugando con Celebi? ".format(
                        profile.level, user.level
                    )
                )

            elif user.team != profile.team:
                output = (
                    "❌ No he reconocido correctamente el <b>equipo</b>. Si no "
                    "consigues que la reconozca, <a href='%s'>entra al "
                    "grupo de asistencia a subidas de niveles</a>"
                    "y nuestro equipo de administradores revisará tu caso manualmente."
                    % (config["telegram"]["helpgroup"])
                )

            if output is not None:
                bot.sendMessage(
                    chat_id=chat_id,
                    text=output,
                    parse_mode=telegram.ParseMode.HTML
                )
                return

            # Level UP ok!
            update_level(profile.level, user_id)

            output = (
                "👌 Así que has subido al nivel *{0}*. ¡Felicidades! \n\n Si "
                "vuelves a subir de nivel en el juego y quieres que se refleje"
                " en tu Ficha de Entrenador puedes enviarme, en cualquier "
                "momento, otra captura de tu perfil de juego. Me encantará "
                "saber sobre tus progresos".format(profile.level))
            bot.sendMessage(
                chat_id=chat_id,
                text=output,
                parse_mode=telegram.ParseMode.MARKDOWN)

            keyboard = [[
                InlineKeyboardButton("🛫 ¡Fly!", callback_data='es_fly'),
                InlineKeyboardButton("🛬 No fly", callback_data='es_nofly')]]
            bot.send_photo(
                chat_id=config["telegram"]["user_validation"],
                photo=open(filename, 'rb'),
                caption="👩‍⚕Trainer ID:{}\nLevel UP:{} {} {}".format(user_id, user.trainer_name, profile.level, user.team), 
                parse_mode=telegram.ParseMode.HTML, 
                reply_markup=InlineKeyboardMarkup(keyboard))

        except Exception as ex:
            logging.error(ex)
            bot.sendMessage(
                chat_id=chat_id,
                text=(
                    '¡Oh! Ha habido algún error verificando la imagen que '
                    'has enviado. Por favor, asegúrate de haber enviado una '
                    'captura de tu perfil. Si el error persiste, por favor '
                    '<a href="{}">entra al grupo de asistencia a subidas de '
                    'nivel</a> para que Chansey y yo te ayudemos'.format(
                        config["telegram"]["helpgroup"])
                ),
                parse_mode=telegram.ParseMode.HTML
            )


@run_async
def process_trainer_photo(bot, update, validation):
    logging.info("%s", update.message.text)

    user_id = update.message.chat.id
    config = get_config()

    photo = bot.get_file(update.message.photo[-1]["file_id"])
    filename = os.path.join(
        os.path.expanduser(config['general']['photos']),
        'profile-{}-{}-{}.jpg'.format(
            user_id, validation.id, int(time.time())
        )
    )

    urllib.request.urlretrieve(photo["file_path"], filename)
    logging.debug("Storing received file in %s", filename)

    store_base_filename = '{}_{}'.format(
        time.time(),
        random.randrange(100, 999)
    )

    logging.debug("%s", validation)
    try:
        profile = parse_profile_image(
            filename,
            validation.pokemon,
            store_path=config['general']['inspectdir'],
            store_base_name=store_base_filename
        )

        logging.debug("End parse_profile_image")

    except ProfileParseException as ex:
        logging.error(ex)
        bot.sendMessage(
            chat_id=user_id,
            text=(
                '¡Oh! Ha habido algún error verificando la imagen que '
                'has enviado. Por favor, asegúrate de haber enviado una '
                'captura de tu perfil. Si el error persiste, por favor '
                '<a href="{}">entra al grupo de asistencia a validaciones'
                '</a> para que Chansey y yo te ayudemos'.format(
                    config["telegram"]["helpgroup"])
            ),
            parse_mode=telegram.ParseMode.HTML
        )
        return False

    logging.debug("BUDDY_NAME OK?:%s", profile.check_buddy_name(validation.pokemon_name))
    logging.debug("TRAINER_NAME OK?: %s", profile.check_name(validation.trainer_name))

    output = None

    if profile.matched_model is None:
        output = (
            "❌ La captura de pantalla no parece válida. Asegúrate "
            "de enviar una captura de pantalla completa del juego "
            "en un teléfono móvil. No son válidas las capturas en "
            "tablets ni otros dispositivos ni capturas recortadas "
            "o alteradas. Puedes volver a intentarlo enviando otra"
            " captura. Si no consigues que la reconozca, "
            "[entra al grupo de asistencia de validaciones]({0}) in"
            "dicando tu alias de Telegram y tu nombre de entrenador"
            " para que revisemos el caso manualmente."
        )

    elif profile.level is None:
        output = (
            "❌ No he reconocido correctamente el *nivel*. Puedes "
            "volver a intentar completar la validación enviando "
            "otra captura. Si no consigues que la reconozca, "
            "[entra al grupo de asistencia de validaciones]({0})"
            " indicando tu alias de Telegram y "
            "tu nombre de entrenador para que revisemos el caso "
            "manualmente."
        )

    elif profile.team is None:
        output = (
            "❌ No he reconocido correctamente el *equipo*. Puedes "
            "volver a intentar completar la validación enviando "
            "otra captura. Si no consigues que la reconozca, "
            "[entra al grupo de asistencia de validaciones]({0})"
            " indicando tu alias de Telegram y tu"
            " nombre de entrenador para que revisemos el caso "
            "manualmente."
        )

    elif profile.buddy != validation.pokemon:
        output = (
            "❌ No he reconocido correctamente el *Pokémon*. "
            "¿Has puesto de compañero a *{2}* como te dije? Puedes"
            " volver a intentarlo enviando otra captura. Si no "
            "consigues que la reconozca, "
            "[entra al grupo de asistencia de validaciones]({0}) "
            "y nuestro equipo de validadores se encargará de ello."
        )
    
    elif not profile.check_buddy_name(validation.pokemon_name):
        output = (
            "❌ No he reconocido correctamente el *nombre del "
            "Pokémon*. ¿Le has cambiado el nombre a *{1}* como te "
            "dije? Puedes volver a intentar completar la "
            "validación enviando otra captura. Si no consigues que"
            " la reconozca, "
            "[entra al grupo de asistencia de validaciones]({0}) in"
            "dicando tu alias de Telegram y tu nombre de entrenador"
            "para que revisemos el caso manualmente."
        )

    elif not profile.check_name(validation.trainer_name):
        output = (
            "❌ No he reconocido correctamente el *nombre del "
            "entrenador*. ¿Seguro que lo has escrito bien? Puedes "
            "volver a enviar otra captura. Si te has equivocado, "
            "espera 6 horas a que caduque la validación y vuelve a"
            " comenzar de nuevo. Si lo has escrito bien y no "
            "consigues que lo reconozca, "
            "[entra al grupo de asistencia de validaciones]({0}) "
            "indicando tu alias de Telegram y tu nombre de "
            "entrenador para que revisemos el caso manualmente."
        )

    user = get_user(user_id)

    if output is not None:
        output = output.format(
            config['telegram']['helpgroup'],
            validation.pokemon_name,
            validation.pokemon
        )
        bot.sendMessage(
            chat_id=update.message.chat.id,
            text=output,
            parse_mode=telegram.ParseMode.MARKDOWN
        )
        if increase_validation(user_id):
            bot.sendMessage(
                chat_id=update.message.chat.id,
                text="❌ Has excedido el número máximo de intentos",
                parse_mode=telegram.ParseMode.MARKDOWN
            )
        return

    # Validation ok!
    
    commit_user(
        user_id,
        validation.trainer_name,
        profile.team,
        ValidationType.PIKACHU,
        profile.level
        )
    complete_validation(
        profile.level, 
        profile.team, 
        user_id
        )

    output = (
        "👌 Has completado el proceso de validación correctamente."
        "Se te ha asignado el equipo *{0}* y el nivel *{1}*.\n\nA "
        "partir de ahora aparecerán tu nivel y equipo reflejados "
        "en tu Ficha de Entrenador.\n\nSi subes de "
        "nivel en el juego y quieres que se refleje en tu Ficha, "
        "puedes enviarme en cualquier momento otra captura de tu "
        "perfil del juego, no es necesario que cambies tu Pokémon "
        "acompañante."
    )

    bot.sendMessage(
        chat_id=update.message.chat.id,
        text=output.format(
            profile.team, profile.level
        ),
        parse_mode=telegram.ParseMode.MARKDOWN
    )
    keyboard = [[
        InlineKeyboardButton("🛫 ¡Fly!", callback_data='es_fly'),
        InlineKeyboardButton("🛬 No fly", callback_data='es_nofly')]]
    bot.send_photo(
        chat_id=config["telegram"]["user_validation"],
        photo=open(filename, 'rb'),
        caption="👩‍⚕Trainer ID:{}\nNew Register:{} {} {}".format(user_id, validation.trainer_name, profile.level, profile.team), 
        parse_mode=telegram.ParseMode.HTML, 
        reply_markup=InlineKeyboardMarkup(keyboard))

#---------------END Photo Process---------------#

@run_async
def set_friend_id(bot, update, args=None):
    logging.debug("%s", update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    if are_banned(chat_id, user_id):
        return

    fc = None

    if len(args) == 3:
        fc = ''.join(args)

    elif len(args) == 1:
        fc = args[0]

    logging.debug("FC:%s",fc)
    user = get_user(user_id)

    if user is None:
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "❌ Esto… no encuentro tu Ficha de Entrenador.¿Qu"
                "ién eres? ¿Te has registrado conmigo? En caso "
                "afirmativo, pide ayuda en @enfermerajoyayuda . "
                "De no ser así, registrate antes de usar este comando"
            ),
            parse_mode=telegram.ParseMode.MARKDOWN
        )
        return

    if fc is None and user.friend_id is None:
        bot.sendMessage(
            chat_id=chat_id,
            text="❌ Vaya, no he reconocido ese ID de entrenador... "
        )
        return

    elif fc is None and user.friend_id is not None:
        bot.sendMessage(
            chat_id=chat_id,
            text="❌ ID de entrenador eliminado correctamente"
        )
        set_fc(None, user_id)
        return

    friendcode = re.match(r'^[0-9]{12}$', fc)

    if friendcode is None and user.friend_id is None:
        bot.sendMessage(
            chat_id=chat_id,
            text="❌ Vaya, no he reconocido ese ID de entrenador... {}".format(fc)
        )
        return

    else:
        set_fc(fc, user_id)
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "👌 Ya he registrado tu ID de entrenador como `{}`. "
                "\n Disfruta ahora interactuando con tus compañeros "
                "de grupo, mandándo regalos e intercambiando Pokémon"
                " por todo el mundo. \n\n *Y recuerda, la suplantac"
                "ión de entrenadores es motivo de baneo*".format(fc)
            ),
            parse_mode=telegram.ParseMode.MARKDOWN
        )


@run_async
def set_switch_id(bot, update, args=None):
    logging.debug("%s", update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    if are_banned(chat_id, user_id):
        return

    fc = None

    if len(args) == 3:
        fc = ''.join(args)

    elif len(args) == 1:
        fc = args[0]

    logging.debug("FC:%s",fc)
    user = get_user(user_id)

    if user is None:
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "❌ Esto… no encuentro tu Ficha de Entrenador.¿Qu"
                "ién eres? ¿Te has registrado conmigo? En caso "
                "afirmativo, pide ayuda en @enfermerajoyayuda . "
                "De no ser así, registrate antes de usar este comando"
            ),
            parse_mode=telegram.ParseMode.MARKDOWN
        )
        return

    if fc is None and user.friend_id is None:
        bot.sendMessage(
            chat_id=chat_id,
            text="❌ Vaya, no he reconocido ese ID de Nintendo Switch... "
        )
        return

    elif fc is None and user.friend_id is not None:
        bot.sendMessage(
            chat_id=chat_id,
            text="❌ ID de Nintendo Switch eliminado correctamente"
        )
        set_swichfc(None, user_id)
        return

    friendcode = re.match(r'^[0-9]{12}$', fc)

    if friendcode is None and user.friend_id is None:
        bot.sendMessage(
            chat_id=chat_id,
            text="❌ Vaya, no he reconocido ese ID de Nintendo Switch... {}".format(fc)
        )
        return

    else:
        set_swichfc(fc, user_id)
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "👌 Ya he registrado tu ID de Nintendo Switch como `{}`. "
                "\n Disfruta ahora interactuando con tus compañeros "
                "de grupo, intercambiando Pokémon por todo el mundo.".format(fc)
            ),
            parse_mode=telegram.ParseMode.MARKDOWN
        )


@run_async
def set_ds_id(bot, update, args=None):
    logging.debug("%s", update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    if are_banned(chat_id, user_id):
        return

    fc = None

    if len(args) == 3:
        fc = ''.join(args)

    elif len(args) == 1:
        fc = args[0]

    logging.debug("FC:%s",fc)
    user = get_user(user_id)

    if user is None:
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "❌ Esto… no encuentro tu Ficha de Entrenador.¿Qu"
                "ién eres? ¿Te has registrado conmigo? En caso "
                "afirmativo, pide ayuda en @enfermerajoyayuda . "
                "De no ser así, registrate antes de usar este comando"
            ),
            parse_mode=telegram.ParseMode.MARKDOWN
        )
        return

    if fc is None and user.friend_id is None:
        bot.sendMessage(
            chat_id=chat_id,
            text="❌ Vaya, no he reconocido ese ID de N3DS... "
        )
        return

    elif fc is None and user.friend_id is not None:
        bot.sendMessage(
            chat_id=chat_id,
            text="❌ ID de N3DS eliminado correctamente"
        )
        set_dsfc(None, user_id)
        return

    friendcode = re.match(r'^[0-9]{12}$', fc)

    if friendcode is None and user.friend_id is None:
        bot.sendMessage(
            chat_id=chat_id,
            text="❌ Vaya, no he reconocido ese ID de N3DS... {}".format(fc)
        )
        return

    else:
        set_dsfc(fc, user_id)
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "👌 Ya he registrado tu ID de N3DS como `{}`. "
                "\n Disfruta ahora interactuando con tus compañeros "
                "de grupo, mandándo regalos e intercambiando Pokémon"
                " por todo el mundo.".format(fc)
            ),
            parse_mode=telegram.ParseMode.MARKDOWN
        )


@run_async
def user_privacity(bot, update):
    logging.debug("%s", update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    if are_banned(chat_id, user_id):
        return

    user = get_user(user_id)

    if user is None or user.validation_type == ValidationType.NONE:
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "❌ Esto… no encuentro tu Ficha de Entrenador.¿Qu"
                "ién eres? ¿Te has registrado conmigo? En caso "
                "afirmativo, pide ayuda en @enfermerajoyayuda . "
                "De no ser así, registrate antes de usar este comando"
            ),
            parse_mode=telegram.ParseMode.MARKDOWN
        )
        return


    button_list = [
        #[InlineKeyboardButton(text="🤖 Bots", callback_data='privacity_bots_goo')],
        [InlineKeyboardButton(text="🗣 Menciones", callback_data='privacity_men_goo')],
        [InlineKeyboardButton(text="👥 Friend Code", callback_data='privacity_fc_goo')],
        [InlineKeyboardButton("Terminado", callback_data='privacity_')]
    ]
    reply_markup = InlineKeyboardMarkup(button_list)
    bot.send_message(
        chat_id=chat_id,
        text=(
            "¡Bienvenido al apartado de privacidad para tu cuenta en"
            " @NurseJoyBot. Selecciona una de las opciones para conti"
            "nuar.\n\n👥 Friend Code: Pulsa en el botón para activar"
            " o desactivar para cambiar tu configuración.\n"
            #"🤖 Bots: Pulsa en el nombre del bot para aceptar o rech"
            #"azar la transferencia de datos."
            ),
        parse_mode=telegram.ParseMode.MARKDOWN,
        reply_markup=reply_markup
    )


@run_async
def privacity_btn(bot, update):
    query = update.callback_query
    data = query.data
    user = update.effective_user
    user_id = query.from_user.id
    text = query.message.text
    chat_id = query.message.chat.id
    message_id = query.message.message_id

    if are_banned(user_id, chat_id):
        return

    if data == "privacity_":
        delete_message(chat_id, message_id, bot)
    
    match = REGPRIV.match(query.data)
    keyboard = []
    if match:
        check = match.group(1)
        option = match.group(2)        
    else:
        return

    if check == "fc":
        if option != "goo":
            update_fclist(user_id)
        user = get_user(user_id)
        if user.fclists:
            text = "✅ Listados de codigos"
        else:
            text = "❌ Listados de codigos"

        button_list = [
            [InlineKeyboardButton(text=text, callback_data='privacity_fc_togle')],
            [InlineKeyboardButton("« Menú principal", callback_data='privacity_back_xxx')]
        ]
        reply_markup = InlineKeyboardMarkup(button_list)

    elif check == "men":
        if option != "goo":
            if option == "ad":
                admin = True
            else:
                admin = False
            update_mentions(user_id, admin)

        user = get_user(user_id)
        if user.adm_alerts:
            text_admin = "✅ Menciones de @admin"
        else:
            text_admin = "◾️ Menciones de @admin"
        if user.usr_alerts:
            text_normal = "✅ Menciones de tu @usuario"
        else:
            text_normal = "◾️ Menciones de tu @usuario"
        button_list = [
            [InlineKeyboardButton(text=text_admin, callback_data='privacity_men_ad')],
            [InlineKeyboardButton(text=text_normal, callback_data='privacity_men_us')],
            [InlineKeyboardButton("« Menú principal", callback_data='privacity_back_xxx')]
        ]
        reply_markup = InlineKeyboardMarkup(button_list)

    elif check == "back":
        button_list = [
            #[InlineKeyboardButton(text="🤖 Bots", callback_data='privacity_bots_go')],
            [InlineKeyboardButton(text="🗣 Menciones", callback_data='privacity_men_goo')],
            [InlineKeyboardButton(text="👥 Friend Code", callback_data='privacity_fc_goo')],
            [InlineKeyboardButton("Terminado", callback_data='privacity_')]
        ]
        reply_markup = InlineKeyboardMarkup(button_list)

    bot.edit_message_reply_markup(
        chat_id=chat_id,
        message_id=message_id,
        reply_markup=reply_markup
    )

'''
    if check == "bot":
        if option != 'go':
            bot_edit_privacity(user_id, option)
        bots = get_bidoof_bots()
        for bot in bots:
            status = get_bot_status(bot.id)
            if status = Verified.NONE
                status = ""
            elif status = Verified.REQUESTED
                status = ""
            elif status = Verified.ACCEPTED
                status = ""
            elif status = Verified.DENIED
                status = ""
            text = "{} {}".format(status, bot.name)
            keyboard.append([InlineKeyboardButton(text, callback_data='privacity_bot_{}'.format())])

        keyboard.append([InlineKeyboardButton("« Menú principal", callback_data='privacity_back_xxx'])
'''