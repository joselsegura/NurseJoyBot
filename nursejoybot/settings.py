#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import re
import logging
from datetime import datetime

import telegram
from telegram.ext.dispatcher import run_async
from pytz import timezone

from nursejoybot.db import get_session
from nursejoybot.model import (
    Group,
    User
)
from nursejoybot.supportmethods import (
    is_admin,
    extract_update_info,
    ensure_escaped,
    delete_message,
    update_settings_message,
    get_unique_from_query,
    get_unified_timezone,
    get_welcome_type
)
from nursejoybot.register import (
    start_gorochu,
    start_register,
    refresh_gorochu
)
from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup
)
from nursejoybot.model import GroupType
from nursejoybot.nests import list_nests_callback
from nursejoybot.storagemethods import (
    get_group,
    are_banned,
    remove_warn,
    add_type,
    rv_link,
    is_news_subscribed,
    rm_news_subscription,
    set_news_subscription,
    set_custom_welcome,
    set_welc_preference,
    set_general_settings,
    set_join_settings,
    set_nests_settings,
    set_welcome_settings,
    set_nanny_settings,
    set_admin_settings,
    set_ladmin_settings,
    set_max_members,
    set_welcome_cooldown
)

@run_async
def settings(bot, update, args=None): 
    logging.debug("nursejoybot:settings: %s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    chat_title = message.chat.title
    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id, chat_id):
        return

    group = get_group(chat_id)
    if group is None:
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "¿Qué? Lo siento, no conozco ese grupo. ¿He saludado "
                "al entrar? Prueba a echarme y a meterme de nuevo. \n"
                "Si has promocionado el grupo a supergrupo después de"
                " que yo entrase entonces esto es normal, no te preoc"
                "upes, echarme y vuelveme a meter, por favor. \n Si e"
                "staba funcionando hasta ahora y he dejado de hacerlo"
                " avisa en @enfermerajoyayuda ."
            ),
            parse_mode=telegram.ParseMode.MARKDOWN
        )
        return

    message = bot.sendMessage(
        chat_id=chat_id,
        text="Oído cocina!..."
    )

    #set_group_info(chat_id, group_alias, chat_title, message.message_id) PENDING FIX!!!
    update_settings_message(chat_id, bot, message.message_id, keyboard = "main")

@run_async
def set_welcome(bot, update):
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    chat = update.effective_chat  # type: Optional[Chat]
    user = update.effective_user  # type: Optional[User]
    msg = update.effective_message  # type: Optional[Message]

    text, data_type, content, buttons = get_welcome_type(msg)

    if not is_admin(chat_id, user_id, bot):
        return

    if data_type is None:
        set_welc_preference(chat_id, False)
        msg.reply_text("Mensaje de bienvenida desactivado correctamente")
        return

    set_welc_preference(chat_id, True)
    set_custom_welcome(chat.id, content or text, data_type, buttons)
    msg.reply_text("Mensaje de bienvenida guardado correctamente")

@run_async
def set_zone(bot, update, args=None): 
    logging.debug("nursejoybot:settimezone: %s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    chat_title = message.chat.title
    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id, chat_id):
        return

    if args is None or len(args) != 1 or len(args[0]) < 3 or len(args[0]) > 60:
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "❌ Me siento un poco perdida ahora mismo. Debes pasarme "
                "un nombre de zona horaria en inglés, por ejemplo, `Ameri"
                "ca/Montevideo` o `Europe/Madrid`."
            ),
            parse_mode=telegram.ParseMode.MARKDOWN)
        return

    tz = get_unified_timezone(args[0])

    if len(tz) == 1:
        group = get_group(chat_id)
        group.timezone = tz[0]
        group.title = chat_title

        bot.sendMessage(
            chat_id=chat_id,
            text="👌 Perfecto! Ya se que hora es.*{}*.".format(group.timezone),
            parse_mode=telegram.ParseMode.MARKDOWN
        )
        now = datetime.now(timezone(group.timezone)).strftime("%H:%M")
        bot.sendMessage(
            chat_id=chat_id,
            text="🕒 Por favor, comprueba que la hora sea correcta: {}".format(now),
            parse_mode=telegram.ParseMode.MARKDOWN)

    elif len(tz) == 0:
        bot.sendMessage(
            chat_id=chat_id,
            text="❌ Uy, no he encontrado ninguna zona horaria con ese nombre",
            parse_mode=telegram.ParseMode.MARKDOWN
        )

    else:
        bot.sendMessage(
            chat_id=chat_id,
            text=("❌ Has sido demasiado genérico con el nombre de la zona "
                  "horaria. Intenta concretar más."),
            parse_mode=telegram.ParseMode.MARKDOWN
        )

@run_async
def set_maxmembers(bot, update, args=None): 
    logging.debug("nursejoybot:settimezone: %s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    chat_title = message.chat.title
    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id, chat_id):
        return

    if args is None or len(args) != 1 or not args[0].isdigit() or int(args[0])<0:
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "❌ No he reconocido el parámetro introducido. Por favor, revisa"
                " el comando e intenta de nuevo."
            ),
            parse_mode=telegram.ParseMode.MARKDOWN)
        return

    if int(args[0]) is 0:
        set_max_members(chat_id, None)
        output = "Número máximo de miembros en el grupo desactivado correctamente."
    else:
        set_max_members(chat_id, int(args[0]))
        output = "Número máximo de miembros en el grupo establecido a {}".format(args[0])
    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN)
    return

@run_async
def set_cooldown(bot, update, args=None): 
    logging.debug("nursejoybot:settimezone: %s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    chat_title = message.chat.title
    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id, chat_id):
        return

    if args is None or len(args) != 1 or not args[0].isdigit() or int(args[0])<0:
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "❌ No he reconocido el parámetro introducido. Por favor, revisa"
                " el comando e intenta de nuevo."
            ),
            parse_mode=telegram.ParseMode.MARKDOWN)
        return

    if int(args[0]) is 0:
        set_welcome_cooldown(chat_id, None)
        output = "El mensaje de bienvenida no se eliminará automáticamente."
    else:
        set_welcome_cooldown(chat_id, int(args[0]))
        output = "El mensaje de bienvenida se eliminará automáticamente en {} segundos".format(args[0])
    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN)
    return

'''
@run_async
def set_stops(bot, update, args=None): 
    logging.debug("nursejoybot:setspreadsheet: %s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    chat_title = message.chat.title
    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id, chat_id):
        return

    if args is None or len(args) != 1:
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "❌ Debes pasarme la URL de la Google Spreadsheet como un ú"
                "nico parámetro."
            )
        )
        return

    m = re.search(
        'docs.google.com/.*spreadsheets/d/([a-zA-Z0-9_-]+)',
        args[0], flags=re.IGNORECASE
    )

    if m is None:
        bot.sendMessage(
            chat_id=chat_id,
            text="❌ Perdona, no consigo reconocer esa URL... %s" % args[0]
        )

    else:
        spreadsheet_id = m.group(1)
        if group is None:  
            bot.sendMessage(
                chat_id=chat_id,
                text=(
                    "¿Qué? Lo siento, no conozco ese grupo. ¿He saludado a"
                    "l entrar? Prueba a echarme y a meterme de nuevo. \n S"
                    "i has promocionado el grupo a supergrupo después de q"
                    "ue yo entrase entonces esto es normal, no te preocupe"
                    "s, echarme y vuélveme a meter, por favor. \n Si estab"
                    "a funcionando hasta ahora y he dejado de hacerlo avis"
                    "a en @enfermerajoyayuda ."
                ),
                parse_mode=telegram.ParseMode.MARKDOWN
            )
            return

        group.title = chat_title
        group.spreadsheet = spreadsheet_id
        bot.sendMessage(
            chat_id=chat_id,
            text=(
                "👌 Establecido hoja de cálculo con identificador `{}`.\n\n"
                "Debes usar `/refresh` ahora para hacer la carga inicial de "
                "las pokeparadas y nidos, y cada vez que modifiques el"
                "documento para recargarlos.".format(ensure_escaped(spreadsheet_id))
            ),
            parse_mode=telegram.ParseMode.MARKDOWN
        )

'''
@run_async
def miscbuttons(bot, update):
    query = update.callback_query
    data = query.data
    user = update.effective_user
    user_id = query.from_user.id
    user_username = query.from_user.username
    chat_id = query.message.chat.id
    message_id = query.message.message_id

    are_banned(user_id, chat_id)
    logging.debug("%s: %s %s", data, bot, update)

    # Nests
    if data == "send_nests":
        list_nests_callback(bot, query.from_user, chat_id, query.message)
        return

    # Gorochu goto
    if data == "gorochu_start":        
        delete_message(chat_id, message_id, bot)
        start_gorochu(bot, user_id, user_username)
        return

    elif data == "validation_start":       
        delete_message(chat_id, message_id, bot)
        start_register(bot, user_id, user_username)
        return

    elif data == "gorochu_refresh":       
        delete_message(chat_id, message_id, bot)
        refresh_gorochu(bot, user_id, user_username)
        return

    elif data == "gorochu_cancel":       
        delete_message(chat_id, message_id, bot)
        return

    match = re.match(r"rm_warn\((.+?)\)", query.data)
    if match:
        if not is_admin(chat_id, user_id, bot):
            bot.answerCallbackQuery(
                text="Solo los administradores del grupo pueden retirar warns",
                callback_query_id=update.callback_query.id,
                show_alert="true"
            )
            return
        user_id = match.group(1)
        res = remove_warn(user_id, chat_id)
        if res:
            text = "Aviso eliminado por @{}.".format(user_username)
        else:
            bot.answerCallbackQuery(
                text="En estos momentos no puedo eliminar el warn, prueba mas tarde",
                callback_query_id=update.callback_query.id,
                show_alert="true"
            )
            return
        logging.debug("%s %s", res, text)
        bot.edit_message_text(
            text=text,
            chat_id=chat_id,
            message_id=message_id,
            parse_mode=telegram.ParseMode.HTML,
            disable_web_page_preview=True
        )
        return

    match = re.match(r"rm_ban\((.+?)\)", query.data)
    if match:
        if not is_admin(chat_id, user_id, bot):
            bot.answerCallbackQuery(
                text="Solo los administradores del grupo pueden retirar bans",
                callback_query_id=update.callback_query.id,
                show_alert="true"
            )
            return
        us_id = match.group(1)
        bot.unbanChatMember(chat_id, us_id)
        text="Ban retirado por @{}.".format(user_username)
        bot.edit_message_text(
            text=text,
            chat_id=chat_id,
            message_id=message_id,
            parse_mode=telegram.ParseMode.HTML,
            disable_web_page_preview=True
        )
        return

    match = re.match(r"type_([a-z]{1,8})_([0-9\-]{2,20})", query.data)
    if match:
        if not is_admin(chat_id, user_id, bot):
            bot.answerCallbackQuery(
                text="Solo los administradores del grupo pueden vincular grupos",
                callback_query_id=update.callback_query.id,
                show_alert="true"
            )
            return

        group_type = match.group(1)
        group_id = match.group(2)
        text = "El grupo ha sido vinculado"

        if group_type == "raids":
            add_type(group_id, GroupType.RAID)
        elif group_type == "trade":
            add_type(group_id, GroupType.TRADE)
        elif group_type == "talk":
            add_type(group_id, GroupType.GENERAL)
        elif group_type == "friends":
            add_type(group_id, GroupType.FRIENDS)
        elif group_type == "ex":
            add_type(group_id, GroupType.EX)
        elif group_type == "alerts":
            add_type(group_id, GroupType.ALERTS)
        elif group_type == "quests":
            add_type(group_id, GroupType.QUESTS)
        elif group_type == "others":
            add_type(group_id, GroupType.OTHER)
        elif group_type == "r":
            add_type(group_id, GroupType.RED)
        elif group_type == "y":
            add_type(group_id, GroupType.YELLOW)
        elif group_type == "b":
            add_type(group_id, GroupType.BLUE)
        elif group_type == "cancel":
            rv_link(group_id)
            text = "El proceso ha sido cancelado"

        bot.edit_message_text(
            text=text,
            chat_id=chat_id,
            message_id=message_id,
            parse_mode=telegram.ParseMode.HTML,
            disable_web_page_preview=True
        )
        return

    if re.match("^es_.+$", data) != None:
        text = re.sub(r"\n . @%s: .*es #?fly" % user_username, "", query.message.caption)
        if data == "es_fly":
            text = text + "\n 🛫 @%s: es #fly" % user_username
        elif data == "es_nofly":
            text = text + "\n 🛬 @%s: no es fly" % user_username
        keyboard = [[
            InlineKeyboardButton("🛫 ¡Fly!", callback_data='es_fly'),
            InlineKeyboardButton("🛬 No fly", callback_data='es_nofly')
        ]]
        bot.edit_message_caption(
            caption=text,
            chat_id=chat_id,
            message_id=message_id,
            reply_markup=InlineKeyboardMarkup(keyboard),
            parse_mode=telegram.ParseMode.HTML,
            disable_web_page_preview=True
        )
        return

    else:
        logging.error(
            'nursejoybot:miscbuttons:%s is not a expected button command',
            data
        )

@run_async
def settingsbuttons(bot, update):
    query = update.callback_query
    data = query.data
    user = update.effective_user
    user_id = query.from_user.id
    user_username = query.from_user.username
    chat_id = query.message.chat.id
    message_id = query.message.message_id

    are_banned(user_id, chat_id)
    logging.debug("%s: %s %s", data, bot, update)

    # Settings and admin stuff
    settings_goto = {
        "settings_goto_general": "general",
        "settings_goto_join_joy": "join_joy",
        "settings_goto_join_pikachu": "join_pikachu",
        "settings_goto_nests": "nests",
        "settings_goto_news": "news",
        "settings_goto_welcome": "welcome",
        "settings_goto_nanny": "nanny",
        "settings_goto_safari": "safari",
        "settings_goto_ladmin": "ladmin",
        "settings_goto_main": "main"
    }
    settings_general = {
        "settings_general_pvp": "pvp",
        "settings_general_jokes": "jokes",
        "settings_general_games": "games",
        "settings_general_hard": "hard",
        "settings_general_trades": "trades",
        "settings_general_notes": "notes",
        "settings_general_reply": "reply",
        "settings_general_cmd": "command",
        "settings_general_warn": "warn"
    }
    settings_joy = {
        "settings_joy_joy": "joy",
        "settings_joy_silence": "silence",
        "settings_joy_mute": "mute",
        "settings_joy_level": "level",
        "settings_joy_val": "val"
    }
    settings_pika = {
        "settings_pika_pika": "pika",
        "settings_pika_valpika": "valpika",
        "settings_pika_pikamute": "pikamute"
    }
    settings_nests = {
        "settings_nests_nests": "nests",
        "settings_nests_alerts": "alerts",
        "settings_nests_rankings": "rankings",
        "settings_nests_text": "text",
        "settings_nests_location": "location",
        "settings_nests_mindays": "mindays",
        "settings_nests_minmessages": "minmessages"
    }
    settings_welcome = {
        "settings_welcome_welcome": "should_welcome"
    }
    settings_nanny = {
        "settings_nanny_command": "cmd",
        "settings_nanny_animation": "animation",
        "settings_nanny_contact": "contact",
        "settings_nanny_photo": "photo",
        "settings_nanny_games": "games",
        "settings_nanny_text": "text",
        "settings_nanny_sticker": "sticker",
        "settings_nanny_location": "location",
        "settings_nanny_url": "url",
        "settings_nanny_video": "video",
        "settings_nanny_warn": "warn",
        "settings_nanny_admin_too": "admin_too"
    }
    settings_ladmin = {
        "settings_ladmin_welcome": "welcome",
        "settings_ladmin_goodbye": "goodbye",
        "settings_ladmin_nests": "nests",
        "settings_ladmin_gejections": "globalEjections",
        "settings_ladmin_admin": "admin",
        "settings_ladmin_ejections": "ejections"
    }
    settings_admin = {
        "settings_admin_welcome": "welcome",
        "settings_admin_goodbye": "goodbye",
        "settings_admin_nests": "nests",
        "settings_admin_gejections": "globalEjections",
        "settings_admin_admin": "admin",
        "settings_admin_ejections": "ejections"
    }
    #---
    if re.match("^settings_.+$", data) is not None:
        match = re.match(r"settings_news_([-0-9]*)", query.data)
        # First, checks if the user is an admin. If not, show an error and return
        if not is_admin(chat_id, user_id, bot):
            bot.answerCallbackQuery(
                text="Solo los administradores del grupo pueden configurar el bot",
                callback_query_id=update.callback_query.id,
                show_alert="true"
            )
            return
        if data in settings_goto:
            update_settings_message(
                chat_id, bot, message_id, keyboard=settings_goto[data]
            )
            return
        elif data == "settings_done":
            delete_message(chat_id, message_id, bot)
            return
        elif data in settings_general:
            set_general_settings(chat_id, settings_general[data])
            update_settings_message(
                chat_id, bot, message_id, keyboard="general"
            )
            return            
        elif data in settings_joy:
            set_join_settings(chat_id, settings_joy[data])
            update_settings_message(
                chat_id, bot, message_id, keyboard="join_joy"
            )
            return        
        elif data in settings_pika:
            set_join_settings(chat_id, settings_pika[data])
            update_settings_message(
                chat_id, bot, message_id, keyboard="join_pikachu"
            )
            return 
        elif data in settings_nests:
            set_nests_settings(chat_id, settings_nests[data])
            update_settings_message(
                chat_id, bot, message_id, keyboard="nests"
            )
            return 
        elif data in settings_welcome:
            set_welcome_settings(chat_id, settings_welcome[data])
            update_settings_message(
                chat_id, bot, message_id, keyboard="welcome"
            )
            return 
        elif data in settings_nanny:
            set_nanny_settings(chat_id, settings_nanny[data])
            update_settings_message(
                chat_id, bot, message_id, keyboard="nanny"
            )
            return
        elif data in settings_admin:
            set_admin_settings(chat_id, settings_admin[data])
            update_settings_message(
                chat_id, bot, message_id, keyboard="admin"
            )
            return 
        elif data in settings_ladmin:
            set_ladmin_settings(chat_id, settings_ladmin[data])
            update_settings_message(
                chat_id, bot, message_id, keyboard="ladmin"
            )
            return 
        elif data is "settings_admin_spy":    
            button_list = [[
                InlineKeyboardButton(text="Adelante!", callback_data='settings_admin_on'),
                InlineKeyboardButton(text="Volver atrás", callback_data='settings_admin_back')
            ]]
            out = (
                "Has seleccionado activar el Modo Incognito. Si continuas, deberás añadir a"
                " @NurseJoyAdminBot al grupo y yo me saldré de este. Mi hermana *NO tiene *"
                "*acceso a ningún mensaje*, incluido los comandos, por ende no sabrá nada d"
                "e lo que pase por el grupo y así preservaréis vuestra intimidad.\n\nPara d"
                "esactivar este modo en un futuro, simplemente deberás añadirme a mi de nue"
                "vo al grupo."
            )
            bot.edit_message_text(
                text=out,
                chat_id=chat_id,
                reply_markup=InlineKeyboardMarkup(button_list),
                parse_mode=telegram.ParseMode.MARKDOWN
            )
            return 
        elif data is "settings_admin_on":
            set_admin_settings(chat_id, "spy_mode")
            delete_message(chat_id, message_id, bot)
            out = "Me voy del grupo, no olvides añadir a mi hermana @NurseJoyAdminBot"
            bot.sendMessage(chat_id, out)
            bot.leaveChat(chat_id)
            return 
        elif data is "settings_admin_back":
            update_settings_message(chat_id, bot, message_id, keyboard='admin')
            return 
        elif match:
            news_id = match.group(1)
            if is_news_subscribed(chat_id, news_id):
                rm_news_subscription(chat_id, news_id)
            else:
                set_news_subscription(chat_id, news_id)
            update_settings_message(
                chat_id, bot, message_id, keyboard="news"
            )
            return

    else:
        logging.error(
            'nursejoybot:settingsbuttons:%s is not a expected settings command',
            data
        )
