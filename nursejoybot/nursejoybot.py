#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import os
import re
import sys
import json
import logging
import telegram

from uuid import uuid4
from threading import Thread
from datetime import datetime
from nursejoybot.rules import send_rules
from nursejoybot.config import get_config
from telegram.ext.dispatcher import run_async
from nursejoybot.model import Team, ValidationType

from nursejoybot.supportmethods import (
    extract_update_info,
    delete_message_timed,
    delete_message,
    is_admin,
    get_gamemaster_data,
    get_move_data,
    get_trsl_data,
    get_move_trsl,
    parse_pokemon
)
from nursejoybot.storagemethods import (
    are_banned,
    rgpd_user,
    get_user,
    get_user_by_name,
    has_fc,
    has_rules,
    get_users_from_group,
    get_group_settings,
    has_ds_fc,
    has_switch_fc
)

REGFINDCP = re.compile(
        r'^([0-9]{1,3}) - ([a-zA-Z0-9\- ]{3,10})$',
        flags=re.IGNORECASE
)
REGWHOIS = re.compile(
    r'^qui[eé]n [eé]s (@|)([a-zA-Z0-9_]{3,32})$',
    flags=re.IGNORECASE
)

@run_async
def start(bot, update, args=None):
    logging.debug("%s %s", bot, update)

    config = get_config()
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    delete_message(chat_id, message.message_id, bot)

    if are_banned(user_id, chat_id):
        profile(bot, update)
        return

    if chat_type == "private" and args is not None and len(args)!=0:
        send_rules(bot, update, args[0])
        return

    if chat_type != "private":
        group = get_group_settings(chat_id)
        if group.reply_on_group:
            dest_id = chat_id
        else:
            dest_id = user_id
    else:
        dest_id = user_id

    sent_message = bot.sendMessage(
        chat_id=dest_id,
        text=(
            "📖 ¡Bienvenido al centro Pokémon de la Enfermera Joy! Tómate"
            " tu tiempo en leer <a href='%s'>la guía de entrenadores</a>.\n\n"
            "<b>Lee con detenimiento la </b><a href='%s'>politica de privacidad</a>"
            "<b> antes de registrarte.</b>\n\n"
            "💙💛❤️<b>Registrar nivel/equipo</b>\nEscríbeme por privado en @%s el "
            "comando <code>/register</code>."
            "\n\n🔔 <b>Subida de nivel</b>\nPara subir de nivel,"
            " unicamente debes enviarme una captura de pantalla de tu perfil"
            " de Pokémon GO por privado y yo haré el resto.\n\n" % (
                config["telegram"]["bothelp"],
                config["telegram"]["botrgpd"],
                config["telegram"]["botalias"])
        ),
        parse_mode=telegram.ParseMode.HTML,
        disable_web_page_preview=True
    )

    if chat_type != "private" and group.reply_on_group:
        Thread(target=delete_message_timed,
               args=(chat_id, sent_message.message_id, 40, bot)).start()

@run_async
def joyping(bot, update):
    logging.debug("nursejoybot:joyping: %s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    sent_dt = message.date
    now_dt = datetime.now()
    timediff = now_dt - sent_dt

    if chat_type != "private":
        try:
            bot.deleteMessage(chat_id=chat_id, message_id=message.message_id)

        except Exception:
            pass

    if chat_type != "private":
        group = get_group_settings(chat_id)
        if group and group.reply_on_group:
            dest_id = chat_id
        else:
            dest_id = user_id
    else:
        dest_id = user_id

    sent_message = bot.sendMessage(
        chat_id=dest_id,
        text=(
            "¿Ves como no era para tanto el pinchazo? Fueron solo %d seg"
            "undos 🤗" % (timediff.seconds)
        ),
        parse_mode=telegram.ParseMode.HTML,
        disable_web_page_preview=True
    )

    if chat_type != "private" and group and group.reply_on_group:
        Thread(target=delete_message_timed,
               args=(chat_id, sent_message.message_id, 10, bot)).start()

@run_async
def whois(bot, update, args=None):
    logging.debug("%s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    delete_message(chat_id, message.message_id, bot)

    if are_banned(user_id, chat_id):
        return

    match = REGWHOIS.match(text)

    logging.debug("Pass banned")
    if message.reply_to_message is not None:
        if message.reply_to_message.forward_from is not None:
            user = get_user(message.reply_to_message.forward_from.id)
            if user is not None:
                replied_id = user.id
        elif message.reply_to_message.from_user is not None:
            user = get_user(message.reply_to_message.from_user.id)
            if user is not None:
                replied_id = user.id
    elif args is not None and len(args)== 1:
        logging.debug("Getting user")
        user = get_user_by_name(args[0])
        if user is not None:
            replied_id = user.id
        logging.debug("User: %s", user)
    elif match:
        logging.debug("Getting user")
        user = get_user_by_name(match.group(2))
        if user is not None:
            replied_id = user.id
        logging.debug("User: %s", user)  
    else:
        return

    logging.debug("Pass get user")

    if user is None or user.level is 0:
        output = "❌ No tengo información sobre este entrenador."
        bot.sendMessage(
            chat_id=user_id,
            text=output,
            parse_mode=telegram.ParseMode.MARKDOWN
        )
        return

    if user.team is Team.NONE:
        text_team = "_Desconocido_"
    elif user.team is Team.BLUE:
        text_team = "del equipo *Sabiduría*"
    elif user.team is Team.RED:
        text_team = "del equipo *Valor*"
    elif user.team is Team.YELLOW:
        text_team = "del equipo *Instinto*"

    text_trainername = ("{}".format(user.trainer_name)
                        if user.trainer_name is not None
                        else "Desconocido")
    
    text_level = ("*{}*".format(user.level)
                  if user.level is not None
                  else "_Desconocido_")

    text_friend_id = ("Su ID de Entrenador: `{}`".format(user.friend_id)
                      if has_fc(user_id) and user.friend_id is not None
                      else "")

    text_ds_id = ("\nSu ID de N3DS: `{}`".format(user.ds_id)
                      if has_ds_fc(user_id) and user.ds_id is not None
                      else "")

    text_switch_id = ("\nSu ID de Nintendo Switch: `{}`".format(user.switch_id)
                      if has_switch_fc(user_id) and user.switch_id is not None
                      else "")

    if user.banned == 1:
        text_validationstatus = "⛔️"

    elif user.validation_type in [
            ValidationType.INTERNAL, ValidationType.PIKACHU, ValidationType.GOROCHU]:
        text_validationstatus = "✅"

    else:
        text_validationstatus = "⚠️"

    if user.admin == 1:
        text_admin = "👩‍⚕️"

    else:
        text_admin = ""

    flag_text = ("{}".format(user.flag)
                  if user.flag is not None
                  else " ")

    output = "[{0}](tg://user?id={1}), es {2} nivel {3} {4} {5} {7}\n{6}{8}{9}".format(
        text_trainername, 
        replied_id,
        text_team,
        text_level,
        text_validationstatus,
        text_admin,
        text_friend_id,
        flag_text,
        text_ds_id,
        text_switch_id
    )

    if chat_type != "private":
        group = get_group_settings(chat_id)
        if group.reply_on_group:
            dest_id = chat_id
        else:
            dest_id = user_id
    else:
        dest_id = user_id

    bot.sendMessage(
        chat_id=dest_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )

@run_async
def whois_id(bot, update, args=None):
    logging.debug("%s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id, chat_id):
        return

    if message.reply_to_message is not None:
        if message.reply_to_message.forward_from is not None:
            user = get_user(message.reply_to_message.forward_from.id)
            if user is not None:
                replied_id = user.id
        elif message.reply_to_message.from_user is not None:
            user = get_user(message.reply_to_message.from_user.id)
            if user is not None:
                replied_id = user.id
    elif args is not None and len(args)== 1:
        args[0] = re.sub("@", "", args[0])
        user = get_user_by_name(args[0])
        if user is not None:
            replied_id = user.id
    else:
        return
    telegram_user = bot.get_chat_member(chat_id, replied_id)
    text_alias = "@{}".format(telegram_user.user.username)

    if user is None and user.level is 0:
        output = "❌ No puedo darte información sobre este entrenador."
        bot.sendMessage(
            chat_id=user_id,
            text=output,
            parse_mode=telegram.ParseMode.MARKDOWN
        )
        return

    if user is None or user.team is Team.NONE:
        text_team = "_Desconocido_"
    elif user and user.team is Team.BLUE:
        text_team = "_Sabiduría_"
    elif user and user.team is Team.RED:
        text_team = "_Valor_"
    elif user and user.team is Team.YELLOW:
        text_team = "_Instinto_"

    text_trainername = ("_{}_".format(user.trainer_name)
                        if user and user.trainer_name is not None
                        else "_Desconocido_")
    
    text_level = ("_{}_".format(user.level)
                  if user and user.level is not None
                  else "_??_")

    if user is None or user.banned == 1:
        text_validationstatus = "⛔️"

    elif user is None or user.validation_type in [
            ValidationType.INTERNAL, ValidationType.PIKACHU, ValidationType.GOROCHU]:
        text_validationstatus = "✅"

    else:
        text_validationstatus = "⚠️"

    flag_text = ("\nFlags:{}".format(user.flag)
                  if user and (user.flag is not None and user.flag is not " " and user.flag is not "")
                  else "")

    output = "*ID:* `{}`\n*Alias:* {}\n*Nick:* {}\n*Nivel:* {}\n*Equipo:* {}\n*Estado:* {}{}".format(
        replied_id,
        text_alias,
        text_trainername, 
        text_level,
        text_team,
        text_validationstatus,
        flag_text
    )

    if chat_type != "private":
        group = get_group_settings(chat_id)

        if group.reply_on_group:
            dest_id = chat_id
        else:
            dest_id = user_id
    else:
        dest_id = user_id

    bot.sendMessage(
        chat_id=dest_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )

@run_async
def profile(bot, update):
    logging.debug("%s %s", bot, update)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)

    user = get_user(user_id)
    if user is not None:
        if user.level == 0 and user.banned:
            return
            
        if user.team is Team.NONE:
            text_team = "_Desconocido_"
        elif user.team is Team.BLUE:
            text_team = "del equipo *Sabiduría*"
        elif user.team is Team.RED:
            text_team = "del equipo *Valor*"
        elif user.team is Team.YELLOW:
            text_team = "del equipo *Instinto*"

        text_trainername = ("*{}*".format(user.trainer_name)
                            if user.trainer_name is not None
                            else "_Desconocido_")
        
        text_level = ("*{}*".format(user.level)
                      if user.level is not None
                      else "_Desconocido_")

        text_friend_id = ("*Tu ID de Entrenador:{}*".format(user.friend_id)
                          if user.friend_id is not None
                          else " ")

        if user.banned == 1:
            text_validationstatus = "⛔️"

        elif user.validation_type in [
                ValidationType.INTERNAL, ValidationType.PIKACHU, ValidationType.GOROCHU]:
            text_validationstatus = "✅"

        else:
            text_validationstatus = "⚠️"

        if user.admin == 1:
            text_admin = "👩‍⚕️"

        else:
            text_admin = ""

        flag_text = ("{}".format(user.flag)
                          if user.flag is not None
                          else " ")

        output = ("{0}, eres {1} nivel {2} {3} {4} {6}\n {5}".format(
            text_trainername, text_team, text_level, text_validationstatus,
            text_admin, text_friend_id, flag_text)
        )
        if user.banned == 1:
            config = get_config()
            output = output + "\n\nSi deseas más información sobre el motivo de tu baneo, pregunta en [el grupo de información para baneados]({}).".format(config["telegram"]["bangroup"])

    else:
        output = "❌ No tengo información sobre ti."

    bot.sendMessage(
        chat_id=user_id,
        text=output,
        parse_mode=telegram.ParseMode.MARKDOWN
    )

@run_async
def tabla(bot, update, args=None): 
    logging.debug("%s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    delete_message(chat_id, message.message_id, bot)

    if are_banned(user_id, chat_id):
        return

    if args is None or len(args)!=1:
        output =(
            "❌ No he encontrado la tabla que me has pedido. Puedes consultar las tablas disponibles en mi "
            "<a href='https://nursejoybot.com/#tablas'>glosario de tablas</a>"
        )
        bot.sendMessage(
            chat_id=user_id,
            text=output,
            parse_mode=telegram.ParseMode.HTML
        )
        return

    else:
        path = "/var/local/nursejoy/tablas"
        dirs = os.listdir(path)
        filename = None
        for file in dirs:
            m = re.match("%s.jpg" % args[0].lower(), file.lower(), flags=re.IGNORECASE)
            logging.debug("%s.jpg --- %s", args[0], file)
            if m is not None:
                filename = args[0]
                logging.debug("%s", filename)
                break

    if chat_type != "private":
        group = get_group_settings(chat_id)

        if group.reply_on_group:
            dest_id = chat_id
        else:
            dest_id = user_id
    else:
        dest_id = user_id

    if filename is not None:
        photo_path = '{0}/{1}.jpg'.format(path, filename.lower())
        logging.debug("%s", photo_path)

        try:
            bot.send_photo(chat_id=dest_id, photo=open(photo_path, 'rb')) 
            if chat_type != "private" and group.reply_on_group is False:
                sent_message = bot.sendMessage(
                chat_id=chat_id,
                text=("Tabla enviada por privado!"),
                )
                Thread(target=delete_message_timed, args=(chat_id, sent_message.message_id, 20, bot)).start()
            return
        except Exception:
            sent_message = bot.sendMessage(
                chat_id=chat_id,
                text=("❌ No he podido enviarte la tabla. Asegurate de tener una conversación iniciada conmigo."),
            )
            Thread(target=delete_message_timed, args=(chat_id, sent_message.message_id, 20, bot)).start()
            pass

    else:
        output =(
            "❌ No he encontrado la tabla que me has pedido. Puedes consultar las tablas disponibles en mi "
            "<a href='https://nursejoybot.com/#tablas'>glosario de tablas</a>"
        )
        sent_message = bot.sendMessage(
            chat_id=chat_id,
            text=output,
            parse_mode=telegram.ParseMode.HTML
        )
        Thread(target=delete_message_timed, args=(chat_id, sent_message.message_id, 20, bot)).start()

@run_async
def pkdx(bot, update, args=None): 
    logging.debug("%s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)

    if are_banned(user_id, chat_id):
        return

    if args is None or len(args)==0:
        output = "❌ No he encontrado los datos del Pokémon en mis archivos"
        bot.sendMessage(
            chat_id=chat_id,
            text=output,
            parse_mode=telegram.ParseMode.HTML
        )
        return

    form = None
    if len(args)>1 and args[1] not in ['m','f','h','s','shiny','variocolor','macho','hembra','male','female']:
        args[0], form = parse_pokemon(args[0], args[1])


    pkmn_id, pkmn_info, spawn_info = get_gamemaster_data(args[0], form)
    if pkmn_id is None:
        output = "❌ No he encontrado los datos del Pokémon en mis archivos"
        bot.sendMessage(
            chat_id=chat_id,
            text=output,
            parse_mode=telegram.ParseMode.HTML
        )
        return

    lang = "es_ES"
    #lang = get_lang(user_id)
    name, desc, category = get_trsl_data(pkmn_id, lang)
    for k in pkmn_info["quickMoves"]:
        move_id, move_data = get_move_data(k)
        move_name = get_move_trsl(move_id, lang)
    for k in pkmn_info["cinematicMoves"]:
        move_id, move_data = get_move_data(k)
        move_name = get_move_trsl(move_id, lang)

    basic_out = ("")
    types_out = ("")
    gnder_out = ("")
    moves_out = ("")
    stats_out = ("")
    evolv_out = ("")
    third_out = ("")
    extra_out = ("")
    '''
    Entrada Pokédex de Pikachu (#0025):
    ℹ️: Cada vez que un Pikachu se encuentra con algo nuevo, le lanza una descarga eléctrica. Cuando se ve alguna baya chamuscada, es muy probable que sea obra de un Pikachu, ya que a veces no controlan la intensidad de la descarga.

    Tipo principal: Eléctrico ⚡️
    🐾: 1 km
    🧪: Pokémon Ratón
    🌧: Bonus Climático

    **Estadisticas base:**
    ⚔️100 - 🛡100 - ❤️100

    **Evolución:**
    50 🍬 ➡️ Raichu 

    **Ataques rapidos:**
    ⚡️ Ataque Rapido
    💥8 - ⏳2,3s
    ⚡️ Impactrueno
    💥110 - ⏳2,3s
    **Ataques cargados:**
    💧Surf
    💥65 - ⏳2,3s - 🥖 x2
    ⚡️Chispazo
    💥65 - ⏳2,3s - 🥖 x3
    ⚡️ Voltio cruel 
    💥90 - ⏳2,3s - 🥖 x2

    Coste Segundo movimiento:
    🍬: 25 - 💫: 50.000
    Ratio de captura y de fuga:
    👌: 23% - 💨: 9%
    '''

    output = "MIERDIPKDX:{}{}".format(spawn_info, pkmn_info)
    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.HTML
    )

@run_async
def artwork(bot, update, args=None): 
    logging.debug("%s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    delete_message(chat_id, message.message_id, bot)

    if are_banned(user_id, chat_id):
        return

    if args is None or len(args)==0:
        output =(
            "❌ No he encontrado los datos del Pokémon en mis archivos"
        )

@run_async
def max_cp(bot, update, args=None): 
    logging.debug("%s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    delete_message(chat_id, message.message_id, bot)

    if are_banned(user_id, chat_id):
        return

    if args is None or len(args)!=1:
        output =(
            "❌ No he encontrado los datos del Pokémon en mis archivos"
        )
        sent_message = bot.sendMessage(
            chat_id=chat_id,
            text=("Información enviada por privado!")
            )
        Thread(target=delete_message_timed, args=(chat_id, sent_message.message_id, 20, bot)).start()
        return

    else:
        path = "/var/local/nursejoy/maxcp"
        dirs = os.listdir(path)
        filename = None
        for file in dirs:
            match = REGFINDCP.match(file)
            if match and (match.group(1) == args[0] or match.group(2).lower() == args[0].lower()):
                filename = file
                logging.debug("%s", filename)
                break
    if chat_type != "private":
        group = get_group_settings(chat_id)

        if group.reply_on_group:
            dest_id = chat_id
        else:
            dest_id = user_id
    else:
        dest_id = user_id

    if filename is not None:
        cp_path = '{0}/{1}'.format(path, filename)
        logging.debug("%s", cp_path)
        file = open(cp_path, 'r')
        data = file.read()
        data = '{}'.format(data)
        data = data.replace('cp','pc')
        try:
            bot.sendMessage(
                chat_id=dest_id,
                text=data
            )
            if chat_type != "private" and not group.reply_on_group:
                sent_message = bot.sendMessage(
                chat_id=chat_id,
                text=("Información enviada por privado!")
                )
                Thread(target=delete_message_timed, args=(chat_id, sent_message.message_id, 20, bot)).start()
        except Exception:
            sent_message = bot.sendMessage(
                chat_id=chat_id,
                text=("❌ No he podido enviarte la información. Asegurate de tener una conversación iniciada conmigo."),
            )
            Thread(target=delete_message_timed, args=(chat_id, sent_message.message_id, 20, bot)).start()
            pass

    else:
        output =(
            "❌ No he encontrado los datos del Pokémon en mis archivos"
        )
        sent_message = bot.sendMessage(
            chat_id=chat_id,
            text=output,
            parse_mode=telegram.ParseMode.HTML
        )
        Thread(target=delete_message_timed, args=(chat_id, sent_message.message_id, 20, bot)).start()

@run_async
def min_cp(bot, update, args=None): 
    logging.debug("%s %s %s", bot, update, args)
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    delete_message(chat_id, message.message_id, bot)

    if are_banned(user_id, chat_id):
        return

    if args is None or len(args)!=1:
        output =(
            "❌ No he encontrado los datos del Pokémon en mis archivos"
        )
        sent_message = bot.sendMessage(
            chat_id=chat_id,
            text=("Información enviada por privado!")
            )
        Thread(target=delete_message_timed, args=(chat_id, sent_message.message_id, 20, bot)).start()
        return

    else:
        path = "/var/local/nursejoy/mincp"
        dirs = os.listdir(path)
        filename = None
        for file in dirs:
            match = REGFINDCP.match(file)
            if match and (match.group(1) == args[0] or match.group(2).lower() == args[0].lower()):
                filename = file
                logging.debug("%s", filename)
                break
    if chat_type != "private":
        group = get_group_settings(chat_id)

        if group.reply_on_group:
            dest_id = chat_id
        else:
            dest_id = user_id
    else:
        dest_id = user_id

    if filename is not None:
        cp_path = '{0}/{1}'.format(path, filename)
        logging.debug("%s", cp_path)
        file = open(cp_path, 'r')
        data = file.read()
        data = '{}'.format(data)
        data = data.replace('cp','pc')
        try:
            bot.sendMessage(
                chat_id=dest_id,
                text=data
            )
            if chat_type != "private" and not group.reply_on_group:
                sent_message = bot.sendMessage(
                chat_id=chat_id,
                text=("Información enviada por privado!")
                )
                Thread(target=delete_message_timed, args=(chat_id, sent_message.message_id, 20, bot)).start()
        except Exception:
            sent_message = bot.sendMessage(
                chat_id=chat_id,
                text=("❌ No he podido enviarte la información. Asegurate de tener una conversación iniciada conmigo."),
            )
            Thread(target=delete_message_timed, args=(chat_id, sent_message.message_id, 20, bot)).start()
            pass

    else:
        output =(
            "❌ No he encontrado los datos del Pokémon en mis archivos"
        )
        sent_message = bot.sendMessage(
            chat_id=chat_id,
            text=output,
            parse_mode=telegram.ParseMode.HTML
        )
        Thread(target=delete_message_timed, args=(chat_id, sent_message.message_id, 20, bot)).start()

@run_async
def info_rgpd(bot, update):
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    if are_banned(user_id, chat_id):
        return

    output = (
        "Entrenador, bienvenido al departamento ARCO de las instalaciones"
        " de @NurseJoyBot. Antes que nada, asegúrese de que se encuentra "
        "informado correctamente en relación a la legislación vigente.\n"
        "\nEn este caso, procederé a eliminar los siguientes datos:\n-Ali"
        "as\n-Nombre de entrenador\n-Nivel\n-Equipo\n-Nidos Registrados\n"
        "-ID de grupos compartidos con @NurseJoyBot e información relativ"
        "a a la actividad en dichos grupos\n-Nidos registrados por usted\n"
        "-Archivos multimedia enviados al bot\n\nSe conservará el ID de T"
        "elegram dado que es información pública obtenida a través de los"
        " grupos que compartes y/o has compartido con @NurseJoyBot y con "
        "fines de defensa de reclamaciones.\n\n"
        "<a href='https://www.aepd.es/media/guias/guia-ciudadano.pdf'>Fuente Legal</a>"
        "\n\nAdemás, será <b>expulsado</b> de todos los grupos en los que @Nu"
        "rseJoyBot y usted se encuentren. En caso de ingresar de nuevo en"
        " algún grupo en el que se encuentre @NurseJoyBot los datos ID de"
        "l Grupo, fecha de entrada, fecha de último mensaje y número de m"
        "ensajes se volverán a recoger.\n\nPara eliminar sus datos, simpl"
        "emente dígame por privado <code>B0rr4r t0d05 m15 d4t05</code>.\n"
        "<b>Una vez hecho eso, su cuenta aparecerá como si jamás hubiera "
        "existido e internamente se marcará como baneada con el motivo el"
        "iminada.</b>"
    )
    bot.sendMessage(
        chat_id=chat_id,
        text=output,
        parse_mode=telegram.ParseMode.HTML
    )

@run_async    
def fuck_rgpd(bot, update):
    chat_id, chat_type, user_id, text, message = extract_update_info(update)

    if are_banned(user_id, chat_id):
        return

    groups = rgpd_user(user_id)

    for k in groups:
        try:
            bot.kickChatMember(k.group_id, user_id)
            bot.unbanChatMember(k.group_id, user_id)
        except:
            pass

    bot.sendMessage(
        chat_id=user_id,
        text="Hasta nunca! :)",
        parse_mode=telegram.ParseMode.HTML
    )

@run_async
def fc_list(bot, update):
    chat_id, chat_type, user_id, text, message = extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)

    if are_banned(user_id, chat_id) or chat_type == "private":
        return

    main = get_user(user_id)
    count = 0
    text = "**Listado de Friend Codes:**"

    if main is None or not main.fclists or main.friend_id is None:
        text = "❌ No cumples los requisitos para solicitar el listado."
        bot.sendMessage(
            chat_id=user_id,
            text=text,
            parse_mode=telegram.ParseMode.MARKDOWN
        )
        return

    users = get_users_from_group(chat_id)
    for user_data in users:
        try:
            data = bot.get_chat_member(chat_id, user_data.user_id)
        except:
            data = None
            pass
        if data is None or (data and data.status not in ['kicked','left'] and not data.user.is_bot):
            user = get_user(user_data.user_id)
            if user and not user.banned and user.friend_id and user.fclists:
                if user.team is Team.BLUE:
                    text_team = "💙"
                elif user.team is Team.RED:
                    text_team = "❤️"
                elif user.team is Team.YELLOW:
                    text_team = "💛"

                text = text + "\n{0}[{1}](tg://user?id={2}) `{3}`".format(
                    text_team,
                    user.trainer_name,
                    user.id,
                    user.friend_id
                )
                count += 1
                if count == 100:
                    bot.sendMessage(
                        chat_id=user_id,
                        text=text,
                        parse_mode=telegram.ParseMode.MARKDOWN
                    )
                    count = 0

    bot.sendMessage(
        chat_id=user_id,
        text=text,
        parse_mode=telegram.ParseMode.MARKDOWN
    )

