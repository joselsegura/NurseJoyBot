#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2019 Marc Rodriguez Garcia <marc@qwert1.es>
#
#
# Command list for @botfather
# help - Muestra la ayuda
# tabla - Muestra la tabla solicitada
# whois - Muestra info del perfil de alguien (en privado)
# maxiv - Envía una cadena con los CP de un Pokémon 100IV

import os
import re
import logging
import random
import time
import urllib.request
import threading
from datetime import datetime, timedelta, date

import telegram
from telegram.utils.helpers import escape_markdown
from telegram.ext.dispatcher import run_async
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from nursejoybot.supportmethods import is_admin, ensure_escaped, \
    is_staff, extract_update_info, delete_message, \
    update_settings_message

from nursejoybot.group import replace
from nursejoybot.model import Team, GroupType
from nursejoybot.storagemethods import are_banned, warn_user, \
    get_admin, get_group, get_warn_limit, set_admin, rm_admin, \
    get_all_groups, add_tlink, new_link, get_groups, set_tag, rm_link, \
    exists_user_group, set_user_group, get_users_from_group, \
    get_user_by_name, get_user, mock_team_link, get_linked_admin, \
    get_admin_from_linked, get_group_settings, get_particular_admin

REGREST = re.compile(r'restrict_([a-z]{3})_([0-9]{3,10})')

uv_last_check = {}
kickuv_last_check = {}
kicklvl_last_check = {}
kickmsg_last_check = {}
kickold_last_check = {}


# Check Last run
def last_run(group_id, cmdtype):
    if cmdtype is 'uv':
        if str(group_id) in uv_last_check:
            lastpress = uv_last_check[str(group_id)]
        else:
            lastpress = None
        uv_last_check[str(group_id)] = datetime.now()
    elif cmdtype is 'kickuv':

        if str(group_id) in kickuv_last_check:
            lastpress = kickuv_last_check[str(group_id)]
        else:
            lastpress = None
        kickuv_last_check[str(group_id)] = datetime.now()
    elif cmdtype is 'kicklvl':

        if str(group_id) in kicklvl_last_check:
            lastpress = kicklvl_last_check[str(group_id)]
        else:
            lastpress = None
        kicklvl_last_check[str(group_id)] = datetime.now()
    elif cmdtype is 'kickmsg':

        if str(group_id) in kickmsg_last_check:
            lastpress = kickmsg_last_check[str(group_id)]
        else:
            lastpress = None
        kickmsg_last_check[str(group_id)] = datetime.now()
    elif cmdtype is 'kickold':

        if str(group_id) in kickold_last_check:
            lastpress = kickold_last_check[str(group_id)]
        else:
            lastpress = None
        kickold_last_check[str(group_id)] = datetime.now()

    if lastpress is not None:
        difftime = datetime.now() - lastpress
        if difftime.days == 0:
            return True

    return False
# ---------KICK MASSIVE---------#
@run_async
def kick_lvl(bot, update, args=None):
    logging.debug('%s %s', bot, update)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)
    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id) or args is None or len(args) != 1 \
        or not args[0].isdigit():
        return

    if user_id != 529767166 and last_run(chat_id, 'kicklvl'):
        bot.sendMessage(chat_id=chat_id,
                        text="Hace menos de un dia que se usó el comando"
                        , parse_mode=telegram.ParseMode.MARKDOWN)
        return

    users = get_users_from_group(chat_id)
    level = int(args[0])
    level += 1
    kick = 0

    for user in users:
        time.sleep(0.05)
        try:
            data = bot.get_chat_member(chat_id, user.user_id)
        except Exception:
            data = None
            pass
        if data and data.status not in ['kicked', 'left'] \
            and not data.user.is_bot:
            info = get_user(user.user_id)
            if info is None or info.team is Team.NONE or info.level \
                < level:
                try:
                    bot.kickChatMember(chat_id, user.user_id)
                    bot.unbanChatMember(chat_id, user.user_id)
                    kick += 1
                except:
                    pass

    bot.sendMessage(chat_id=chat_id,
                    text='Han sido expulsados {} usuarios'.format(kick),
                    parse_mode=telegram.ParseMode.MARKDOWN)

@run_async
def kick_msg(bot, update, args=None):
    logging.debug('%s %s', bot, update)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id) or args is None or len(args) != 1 \
        or not args[0].isdigit():
        return

    if user_id != 529767166 and last_run(chat_id, 'kickmsg'):
        bot.sendMessage(chat_id=chat_id,
                        text="Hace menos de un dia que se usó el comando"
                        , parse_mode=telegram.ParseMode.MARKDOWN)
        return

    kick = 0
    msg_number = int(args[0])
    users = get_users_from_group(chat_id)
    for user in users:
        time.sleep(0.05)
        try:
            data = bot.get_chat_member(chat_id, user.user_id)
        except:
            data = None
            pass
        if data and data.status not in ['kicked', 'left'] \
            and not data.user.is_bot and user.total_messages \
            < msg_number:
            try:
                bot.kickChatMember(chat_id, user.user_id)
                bot.unbanChatMember(chat_id, user.user_id)
                kick += 1
            except:
                pass

    bot.sendMessage(chat_id=chat_id,
                    text='Han sido expulsados {} usuarios'.format(kick),
                    parse_mode=telegram.ParseMode.MARKDOWN)

@run_async
def kick_old(bot, update, args=None):
    logging.debug('%s %s', bot, update)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)

    
    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id) or args is None or len(args) != 1 \
        or not args[0].isdigit():
        return

    if user_id != 529767166 and last_run(chat_id, 'kickold'):
        bot.sendMessage(chat_id=chat_id,
                        text="Hace menos de un dia que se usó el comando"
                        , parse_mode=telegram.ParseMode.MARKDOWN)
        return


    kick = 0
    days = int(args[0])
    start = datetime.utcnow()
    users = get_users_from_group(chat_id)
    for user in users:
        time.sleep(0.05)
        try:
            data = bot.get_chat_member(chat_id, user.user_id)
        except:
            data = None
            pass
        if data and data.status not in ['kicked', 'left'] \
            and not data.user.is_bot:
            date = start - user.last_message
            if date.days >= days:
                try:
                    bot.kickChatMember(chat_id, user.user_id)
                    bot.unbanChatMember(chat_id, user.user_id)
                    kick += 1
                except:
                    pass

    bot.sendMessage(chat_id=chat_id,
                    text='Han sido expulsados {} usuarios'.format(kick),
                    parse_mode=telegram.ParseMode.MARKDOWN)
#-------END KICK MASSIVE-------#
#-------------WARN-------------#
@run_async
def warn(bot, update, args=None):
    logging.debug('%s %s\nArgs:%s', bot, update, args)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)

    if are_banned(user_id, chat_id) or not is_admin(chat_id, user_id,
            bot):
        return

    reason = ''
    name = ''
    if message.reply_to_message is not None:
        if message.reply_to_message.forward_from is not None:
            if args is not None and len(args)!=0 and args[0] is "f":
                replied_user = message.reply_to_message.from_user.id
                name = message.reply_to_message.from_user.first_name
                del args[0]
            else:
                replied_user = message.reply_to_message.forward_from.id
                name = message.reply_to_message.forward_from.first_name
        elif message.reply_to_message.from_user is not None:
            replied_user = message.reply_to_message.from_user.id
            name = message.reply_to_message.from_user.first_name
        logging.debug('%s', args)
        if args and len(args) > 0:
            reason = ' '.join(args)
    elif args is not None and len(args) > 0:
        if len(args) > 1 and args[0][0] is '-':
            if get_linked_admin(chat_id, args[0]):
                chat_id = args[0]
                del args[0]
            else:
                output = "❌ Ese grupo no esta vinculado a este"
                bot.sendMessage(chat_id=chat_id, text=output,
                                parse_mode=telegram.ParseMode.MARKDOWN)
                return
        if args[0].isdigit():
            user = get_user(args[0])
            if user and user.trainer_name:
                name = user.trainer_name
            elif user and user.username:
                name = user.username
            else:
                name = args[0]
            replied_user = args[0]
            del args[0]
        else:
            args[0] = re.sub("@", "", args[0])
            user = get_user_by_name(args[0])
            del args[0]
            if user:
                name = user.trainer_name
                replied_user = user.id
            else:
                return
    else:
        return

    if args is not None and len(args) > 0:
        reason = ' '.join(args)

    if get_admin(chat_id) is not None:
        groups = get_all_groups(chat_id)
    else:
        groups = None

    if groups is None:
        if not exists_user_group(replied_user, chat_id):
            set_user_group(replied_user, chat_id)

        warning = warn_user(chat_id, replied_user, 1)
        group = get_group_settings(chat_id)
        if warning < group.warn:
            output = \
                "👌 Entrenador [{0}](tg://user?id={1}) advertido correctamente! {2}/{3}".format(name,
                    replied_user, warning, group.warn)
            if reason is not '':
                output = output + '\nMotivo: {}'.format(reason)
            keyboard = \
                InlineKeyboardMarkup([[InlineKeyboardButton('Eliminar warn'
                    ,
                    callback_data='rm_warn({})'.format(replied_user))]])
            bot.send_message(chat_id=chat_id, text=output,
                             parse_mode=telegram.ParseMode.MARKDOWN,
                             reply_markup=keyboard)
        else:
            output = \
                '👌 Entrenador {} baneado correctamente!\nAlcanzado número máximo de advertencias.'.format(name)
            if reason is not '':
                output = output + '\nMotivo: {}'.format(reason)

            warn_user(chat_id, replied_user, 0)
            try:
                bot.kickChatMember(chat_id, replied_user)
                if group.hard is False:
                    bot.unbanChatMember(chat_id, replied_user)
                bot.sendMessage(chat_id=chat_id, text=output,
                                parse_mode=telegram.ParseMode.MARKDOWN)
            except:
                output = "❌ No tengo los permisos necesarios"
                bot.sendMessage(chat_id=chat_id, text=output,
                                parse_mode=telegram.ParseMode.MARKDOWN)
                return

        ladmin = get_particular_admin(chat_id)
        if ladmin is not None and ladmin.ejections:
            admin = get_admin_from_linked(chat_id)
            if admin is not None and admin.ejections is True:
                replace_pogo = replace(replied_user, name)
                message_text = \
                    "ℹ️ {}\n👤 {} ha sido advertido {}/{}".format(message.chat.title,
                        replace_pogo, warning, group.warn)
                if reason is not '':
                    message_text = message_text + '\n❔ Motivo: {}'.format(reason)
                bot.sendMessage(chat_id=admin.id, text=message_text,
                                parse_mode=telegram.ParseMode.MARKDOWN)
    else:
        admin_chat_id = chat_id
        replace_pogo = replace(replied_user, name)
        warnText = f"👤 {replace_pogo}"
        if reason is not '':
            warnText = warnText + '\n❔ Motivo: {}'.format(reason)
        warnText = warnText + "\nℹ️ Entrenador advertido correctamente de:"
        successBool = False
        errorBool = False
        for group in groups:
            chat_id = group.linked_id
            group_title = group.title
            group = get_group(chat_id)
            if not exists_user_group(replied_user, chat_id):
                set_user_group(replied_user, chat_id)
            warning = warn_user(chat_id, replied_user, 1)
            group = get_group_settings(chat_id)
            if warning < group.warn:
                warnText = warnText + "\n- {0} | {1}/{2}".format(group_title, warning, group.warn)
            else:
                warnText = warnText + "\n- {0} | {1}/{2}".format(group_title, warning, group.warn)
                success = f"👤 {replace_pogo} \n❔ Motivo: Alcanzado el número máximo de advertencias.\nℹ️ Entrenador baneado correctamente de:"
                error = f"👤 {replace_pogo}\n❌ No he podido banear al entrenador de:"

                warn_user(chat_id, replied_user, 0)
                try:
                    bot.kickChatMember(chat_id, replied_user)
                    if group.hard is False:
                        bot.unbanChatMember(chat_id, replied_user)
                    successBool = True
                    success = success + "\n- {}".format(group_title)
                except:
                    errorBool = True
                    error = error + "\n- {}".format(group_title)
                    continue

        bot.send_message(chat_id=admin_chat_id, text=warnText,
                                parse_mode=telegram.ParseMode.MARKDOWN)

        if successBool == True:
            bot.send_message(chat_id=admin_chat_id, text=success,
                                parse_mode=telegram.ParseMode.MARKDOWN)

        if errorBool == True:
            error = error + "\n\nPuede que sea administrador o que yo no tenga los permisos de administración necesarios. (Borrar y expulsar)"
            bot.send_message(chat_id=admin_chat_id, text=error,
                                parse_mode=telegram.ParseMode.MARKDOWN)

#-----------END WARN-----------#
#-------------KICK-------------#
@run_async
def kick(bot, update, args=None):
    logging.debug('%s %s', bot, update)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)

    if are_banned(user_id, chat_id) or not is_admin(chat_id, user_id,
            bot):
        return

    if delete_message(chat_id, message.message_id, bot) is False:
        bot.sendMessage(chat_id=chat_id,
                        text="❌ No soy administradora del grupo con los permisos necesarios. (Borrar y expulsar)"
                        , parse_mode=telegram.ParseMode.MARKDOWN)
        return

    reason = ''
    name = ''
    if message.reply_to_message is not None:
        if message.reply_to_message.forward_from is not None:
            if args is not None and len(args)!=0 and args[0] is "f":
                replied_user = message.reply_to_message.from_user.id
                name = message.reply_to_message.from_user.first_name
                del args[0]
            else:
                replied_user = message.reply_to_message.forward_from.id
                name = message.reply_to_message.forward_from.first_name
        elif message.reply_to_message.from_user is not None:
            replied_user = message.reply_to_message.from_user.id
            name = message.reply_to_message.from_user.first_name
        logging.debug('%s', args)
        if args and len(args) > 0:
            reason = ' '.join(args)
    elif args is not None and len(args) > 0:
        if len(args) > 1 and args[0][0] is '-':
            if get_linked_admin(chat_id, args[0]):
                chat_id = args[0]
                del args[0]
            else:
                output = "❌ Ese grupo no esta vinculado a este"
                bot.sendMessage(chat_id=chat_id, text=output,
                                parse_mode=telegram.ParseMode.MARKDOWN)
                return
        if args[0].isdigit():
            user = get_user(args[0])
            if user and user.trainer_name:
                name = user.trainer_name
            elif user and user.username:
                name = user.username
            else:
                name = args[0]
            replied_user = args[0]
            del args[0]
        else:
            args[0] = re.sub("@", "", args[0])
            user = get_user_by_name(args[0])
            del args[0]
            if user:
                name = user.trainer_name
                replied_user = user.id
            else:
                return
    else:
        return

    if args is not None and len(args) > 0:
        reason = ' '.join(args)

    if get_admin(chat_id) is not None:
        groups = get_all_groups(chat_id)
    else:
        groups = None

    if groups is None:
        try:
            bot.kickChatMember(chat_id, replied_user)
            bot.unbanChatMember(chat_id, replied_user)
            output = \
                "👌 Entrenador {} expulsado correctamente!".format(name)
            if reason is not '':
                output = output + '\nMotivo: {}'.format(reason)
            bot.sendMessage(chat_id=chat_id, text=output,
                parse_mode=telegram.ParseMode.MARKDOWN)
        except:
            output = \
                "❌ No he podido expulsar al entrenador. Puede que sea administrador o ya no forme parte del grupo."
            bot.sendMessage(chat_id=chat_id, text=output,
                            parse_mode=telegram.ParseMode.MARKDOWN)
            return

        ladmin = get_particular_admin(chat_id)
        if ladmin is not None and ladmin.ejections:
            admin = get_admin_from_linked(chat_id)
            if admin is not None and admin.ejections is True:
                replace_pogo = replace(replied_user, name)
                message_text = \
                    "ℹ️ {}\n👤 {} ha sido expulsado".format(message.chat.title,
                        replace_pogo)
                if reason is not '':
                    message_text = message_text + '\n❔ Motivo: {}'.format(reason)
                bot.sendMessage(chat_id=admin.id, text=message_text,
                                parse_mode=telegram.ParseMode.MARKDOWN)
    else:
        admin_chat_id = chat_id
        replace_pogo = replace(replied_user, name)
        success = f"👤 {replace_pogo}"
        if reason is not '':
            success = success + '\n❔ Motivo: {}'.format(reason)
        success = success + "\nℹ️ Entrenador expulsado correctamente de:"
        successBool = False
        error = f"👤 {replace_pogo}\n❌ No he podido expulsar al entrenador de:"
        errorBool = False
        for group in groups:
            chat_id = group.linked_id
            group = get_group(chat_id)
            try:
                bot.kickChatMember(chat_id, replied_user)
                bot.unbanChatMember(chat_id, replied_user)
                successBool = True
                success = success + "\n- {}".format(group.title)
            except:
                errorBool = True
                error = error + "\n- {}".format(group.title)
                continue

        if successBool == True:
            bot.send_message(chat_id=admin_chat_id, text=success,
                                parse_mode=telegram.ParseMode.MARKDOWN)

        if errorBool == True:
            error = error + "\n\nPuede que sea administrador o que yo no tenga los permisos de administración necesarios. (Borrar y expulsar)"
            bot.send_message(chat_id=admin_chat_id, text=error,
                                parse_mode=telegram.ParseMode.MARKDOWN)
#-----------END KICK-----------#
#-------------BAN--------------#
@run_async
def ban(bot, update, args=None):
    logging.debug('%s %s', bot, update)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)

    if are_banned(user_id, chat_id) or not is_admin(chat_id, user_id,
            bot):
        return

    if delete_message(chat_id, message.message_id, bot) is False:
        bot.sendMessage(chat_id=chat_id,
                        text="❌ No soy administradora del grupo con los permisos necesarios. (Borrar y expulsar)"
                        , parse_mode=telegram.ParseMode.MARKDOWN)
        return

    reason = ''
    name = ''
    if message.reply_to_message is not None:
        if message.reply_to_message.forward_from is not None:
            if args is not None and len(args)!=0 and args[0] is "f":
                replied_user = message.reply_to_message.from_user.id
                name = message.reply_to_message.from_user.first_name
                del args[0]
            else:
                replied_user = message.reply_to_message.forward_from.id
                name = message.reply_to_message.forward_from.first_name
        elif message.reply_to_message.from_user is not None:
            replied_user = message.reply_to_message.from_user.id
            name = message.reply_to_message.from_user.first_name
        logging.debug('%s', args)
        if args and len(args) > 0:
            reason = ' '.join(args)
    elif args is not None and len(args) > 0:
        if len(args) > 1 and args[0][0] is '-':
            if get_linked_admin(chat_id, args[0]):
                chat_id = args[0]
                del args[0]
            else:
                output = "❌ Ese grupo no esta vinculado a este"
                bot.sendMessage(chat_id=chat_id, text=output,
                                parse_mode=telegram.ParseMode.MARKDOWN)
                return
        if args[0].isdigit():
            user = get_user(args[0])
            if user and user.trainer_name:
                name = user.trainer_name
            elif user and user.username:
                name = user.username
            else:
                name = args[0]
            replied_user = args[0]
            del args[0]
        else:
            args[0] = re.sub("@", "", args[0])
            user = get_user_by_name(args[0])
            del args[0]
            if user:
                name = user.trainer_name
                replied_user = user.id
            else:
                return
    else:
        return

    if args is not None and len(args) > 0:
        reason = ' '.join(args)

    if get_admin(chat_id) is not None:
        groups = get_all_groups(chat_id)
    else:
        groups = None

    if groups is None:
        try:
            bot.kickChatMember(chat_id, replied_user)
            output = \
                "👌 Entrenador {} baneado correctamente!".format(name)
            if reason is not '':
                output = output + '\nMotivo: {}'.format(reason)
            keyboard = \
                InlineKeyboardMarkup([[InlineKeyboardButton('Eliminar Ban'
                    ,
                    callback_data='rm_ban({})'.format(replied_user))]])
            bot.send_message(chat_id=chat_id, text=output,
                             parse_mode=telegram.ParseMode.MARKDOWN,
                             reply_markup=keyboard)
        except:
            output = \
                "❌ No he podido banear al entrenador. Puede que sea administrador."
            bot.sendMessage(chat_id=chat_id, text=output,
                            parse_mode=telegram.ParseMode.MARKDOWN)
            return
            
        ladmin = get_particular_admin(chat_id)
        if ladmin is not None and ladmin.ejections:
            admin = get_admin_from_linked(chat_id)
            if admin is not None and admin.ejections is True:
                replace_pogo = replace(replied_user, name)
                message_text = \
                "ℹ️ {}\n👤 {} ha sido baneado".format(message.chat.title,
                    replace_pogo)
                if reason is not '':
                    message_text = message_text + '\n❔ Motivo: {}'.format(reason)
                bot.sendMessage(chat_id=admin.id, text=message_text,
                                parse_mode=telegram.ParseMode.MARKDOWN)
    else:
        admin_chat_id = chat_id
        replace_pogo = replace(replied_user, name)
        success = f"👤 {replace_pogo}"
        if reason is not '':
            success = success + '\n❔ Motivo: {}'.format(reason)
        success = success + "\nℹ️ Entrenador baneado correctamente de:"
        successBool = False
        error = f"👤 {replace_pogo}\n❌ No he podido banear al entrenador de:"
        errorBool = False
        for group in groups:
            chat_id = group.linked_id
            group = get_group(chat_id)
            try:
                bot.kickChatMember(chat_id, replied_user)
                successBool = True
                success = success + "\n- {}".format(group.title)
            except:
                errorBool = True
                error = error + "\n- {}".format(group.title)
                continue

        if successBool == True:
            bot.send_message(chat_id=admin_chat_id, text=success,
                                parse_mode=telegram.ParseMode.MARKDOWN)

        if errorBool == True:
            error = error + "\n\nPuede que sea administrador o que yo no tenga los permisos de administración necesarios. (Borrar y expulsar)"
            bot.send_message(chat_id=admin_chat_id, text=error,
                                parse_mode=telegram.ParseMode.MARKDOWN)

@run_async
def unban(bot, update, args=None):
    logging.debug('%s %s', bot, update)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)

    if are_banned(user_id, chat_id) or not is_admin(chat_id, user_id,
            bot):
        return

    reason = ''

    if delete_message(chat_id, message.message_id, bot) is False:
        bot.sendMessage(chat_id=chat_id,
                        text="❌ No soy administradora del grupo con los permisos necesarios. (Borrar y expulsar)"
                        , parse_mode=telegram.ParseMode.MARKDOWN)
    if message.reply_to_message is not None:
        if message.reply_to_message.forward_from is not None:
            if args is not None and len(args)!=0 and args[0] is "f":
                replied_user = message.reply_to_message.from_user.id
                name = message.reply_to_message.from_user.first_name
                del args[0]
        elif message.reply_to_message.from_user is not None:

            replied_user = message.reply_to_message.from_user.id
            name = message.reply_to_message.from_user.first_name
        logging.debug('%s', args)
        if args and len(args) > 0:
            reason = ' '.join(args)
    elif args is not None and len(args) > 0:
        if len(args) > 1 and args[0][0] is '-':
            if get_linked_admin(chat_id, args[0]):
                chat_id = args[0]
                del args[0]
            else:
                output = "❌ Ese grupo no esta vinculado a este"
                bot.sendMessage(chat_id=chat_id, text=output,
                                parse_mode=telegram.ParseMode.MARKDOWN)
                return
        if args[0].isdigit():
            user = get_user(args[0])
            if user and user.trainer_name:
                name = user.trainer_name
            elif user and user.username:
                name = user.username
            else:
                name = args[0]
            replied_user = args[0]
            del args[0]
        else:
            args[0] = re.sub("@", "", args[0])
            user = get_user_by_name(args[0])
            del args[0]
            if user:
                name = user.trainer_name
                replied_user = user.id
            else:
                return
        if len(args) > 0:
            reason = ' '.join(args)
    else:
        return
    if get_admin(chat_id) is not None:
        groups = get_all_groups(chat_id)
    else:
        groups = None

    if groups is None:
        try:
            bot.unbanChatMember(chat_id, replied_user)
            output = \
                "👌 Entrenador {} desbaneado correctamente!".format(name)
            if reason is not '':
                output = output + '\nMotivo: {}'.format(reason)
            bot.sendMessage(chat_id=chat_id, text=output,
                            parse_mode=telegram.ParseMode.MARKDOWN)
        except:
            output = "❌ No he podido desbanear al entrenador"
            bot.sendMessage(chat_id=chat_id, text=output,
                            parse_mode=telegram.ParseMode.MARKDOWN)
            return
            
        ladmin = get_particular_admin(chat_id)
        if ladmin is not None and ladmin.ejections:
            admin = get_admin_from_linked(chat_id)
            if admin is not None and admin.ejections is True:
                replace_pogo = replace(replied_user, name)
                message_text = \
                    "ℹ️ {}\n👤 {} ha sido desbaneado".format(message.chat.title,
                        replace_pogo)
                if reason is not '':
                    message_text = message_text + '\n❔ Motivo: {}'.format(reason)
                bot.sendMessage(chat_id=admin.id, text=message_text,
                                parse_mode=telegram.ParseMode.MARKDOWN)
    else:
        admin_chat_id = chat_id
        replace_pogo = replace(replied_user, name)
        success = f"👤 {replace_pogo}"
        if reason is not '':
            success = success + '\n❔ Motivo: {}'.format(reason)
        success = success + "\nℹ️ Entrenador desbaneado correctamente de:"
        successBool = False
        error = f"👤 {replace_pogo}\n❌ No he podido desbanear al entrenador de:"
        errorBool = False
        for group in groups:
            chat_id = group.linked_id
            group = get_group(chat_id)
            try:
                bot.unbanChatMember(chat_id, replied_user)
                successBool = True
                success = success + "\n- {}".format(group.title)
            except:
                errorBool = True
                error = error + "\n- {}".format(group.title)
                continue

        if successBool == True:
            bot.send_message(chat_id=admin_chat_id, text=success,
                                parse_mode=telegram.ParseMode.MARKDOWN)

        if errorBool == True:
            error = error + "\n\nPuede que sea administrador o que yo no tenga los permisos de administración necesarios. (Borrar y expulsar)"
            bot.send_message(chat_id=admin_chat_id, text=error,
                                parse_mode=telegram.ParseMode.MARKDOWN)
#-----------END BAN------------#
#---------MASSIVE BANS---------#
#-------END MASSIVE BANS-------#
#--------------UV--------------#
def request_list_uv(bot, update, args=None):
    logging.debug('%s %s %s', bot, update, args)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)

    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    uv = r = b = y = outn = 0
    print_r = print_y = print_b = print_uv = False

    if args is not None and len(args) > 0:
        for arg in args:
            arg = arg.lower()
            if arg.lower() == 'l':
                print_uv = True
            elif arg.lower() == 'r':
                print_r = True
            elif arg.lower() == 'y':
                print_y = True
            elif arg.lower() == 'b':
                print_b = True
            elif arg.lower() == 'all':
                print_uv = print_r = print_b = print_y = True
                break
    else:
        bot.sendMessage(chat_id=chat_id,
                        text="Debes introducir un parametro entre `all, l, b, r, y`"
                        , parse_mode=telegram.ParseMode.HTML)
        return     

    if user_id != 529767166 and last_run(chat_id, 'uv'):
        bot.sendMessage(chat_id=chat_id,
                        text="Hace menos de un dia que se usó el comando"
                        , parse_mode=telegram.ParseMode.HTML)
        return

    logging.debug('%s %s %s %s', print_uv, print_r, print_b, print_y)

    output = 'Listado de usuarios:\n'

    users = get_users_from_group(chat_id)
    tlgrm = bot.get_chat_members_count(chat_id)
    for user in users:
        try:
            data = bot.get_chat_member(chat_id, user.user_id)
        except:
            data = None
            pass
        if data and data.status not in ['kicked', 'left'] \
            and not data.user.is_bot:
            info = get_user(user.user_id)
            if info is None or info.team is Team.NONE:
                uv += 1
                if print_uv:
                    output = output \
                        + ' <a href="tg://user?id={1}">🖤{0}</a>'.format(escape_markdown(data.user.first_name),
                            user.user_id)
                    outn += 1
            elif info.team is Team.BLUE:
                b += 1
                if print_b:
                    output = output \
                        + ' <a href="tg://user?id={1}">💙{0}</a>'.format(info.trainer_name,
                            user.user_id)
                    outn += 1
            elif info.team is Team.RED:
                r += 1
                if print_r:
                    output = output \
                        + ' <a href="tg://user?id={1}">❤️{0}</a>'.format(info.trainer_name,
                            user.user_id)
                    outn += 1
            elif info.team is Team.YELLOW:
                y += 1
                if print_y:
                    output = output \
                        + ' <a href="tg://user?id={1}">💛{0}</a>'.format(info.trainer_name,
                            user.user_id)
                    outn += 1

            if outn == 100:
                bot.sendMessage(chat_id=chat_id, text=output,
                                parse_mode=telegram.ParseMode.HTML)
                output = ''
                outn = 0

            time.sleep(0.01)

    logging.debug('%s', output)
    total = uv + b + r + y
    tlgrm = tlgrm - total - 1
    output = \
        """

💙 Blanche: {}
💛 Spark: {}
❤️ Candela: {}
🖤 Unvalidated: {}
❓ Unknown: {}
""".format(b,
            y, r, uv, tlgrm) + output
    bot.sendMessage(chat_id=chat_id, text=output,
                    parse_mode=telegram.ParseMode.HTML)


def request_kick_uv(bot, update, args=None):
    logging.debug('%s %s %s', bot, update, args)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)

    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    if user_id != 529767166 and last_run(chat_id, 'kickuv'):
        bot.sendMessage(chat_id=chat_id,
                        text="Hace menos de un dia que se usó el comando"
                        , parse_mode=telegram.ParseMode.HTML)
        return

    uv = errors = print_uv = outn = 0
    output = ''

    if args is not None and len(args) > 0:
        arg = args[0].lower()
        if arg is 'l':
            print_uv = 1

    users = get_users_from_group(chat_id)
    for user in users:
        try:
            data = bot.get_chat_member(chat_id, user.user_id)
        except:
            data = None
            pass
        if data and data.status not in ['kicked', 'left'] \
            and not data.user.is_bot:
            info = get_user(user.user_id)
            if info is None or info.team is Team.NONE:
                try:
                    bot.kickChatMember(chat_id, user.user_id)
                    bot.unbanChatMember(chat_id, user.user_id)
                    uv += 1
                except:
                    pass
                if print_uv and info is None:
                    output = output \
                        + " <a href='tg://user?id={0}'>🖤{0}</a>".format(user.user_id)
                    outn += 1
                elif print_uv:
                    output = output \
                        + " <a href='tg://user?id={0}'>🖤{1}</a>".format(info.alias
                            or user.user_id, user.user_id)
                    outn += 1
                if outn == 100:
                    bot.sendMessage(chat_id=chat_id, text=output,
                                    parse_mode=telegram.ParseMode.HTML)
                    output = ''
                    outn = 0

                time.sleep(0.01)

    output = 'Han sido expulsados {} usuarios'.format(uv) + output

    bot.sendMessage(chat_id=chat_id, text=output,
                    parse_mode=telegram.ParseMode.MARKDOWN)


# ------------END UV------------#
# -----------SETTINGS-----------#

@run_async
def set_teamgroup(bot, update, args=None):
    logging.debug('%s %s', bot, update)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)

    if args is None or len(args) != 2:
        bot.sendMessage(chat_id=chat_id,
                        text='Lo siento, no he reoconocido correctamente los parametros'
                        , parse_mode=telegram.ParseMode.MARKDOWN)
        return

    if get_admin(chat_id) is None:
        bot.sendMessage(chat_id=chat_id,
                        text="Este comando es exclusivo para grupos de administración"
                        , parse_mode=telegram.ParseMode.MARKDOWN)
        return

    if args[0].lower() in ['red', 'r', 'rojo']:
        team = GroupType.RED
    elif args[0].lower() in ['azul', 'b', 'blue']:
        team = GroupType.BLUE
    elif args[0].lower() in ['yellow', 'y', 'amarillo']:
        team = GroupType.YELLOW
    else:
        bot.sendMessage(chat_id=chat_id,
                        text='No he reconocido el equipo',
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return

    del args[0]

    if get_team_linked(chat_id, team):
        bot.sendMessage(chat_id=chat_id,
                        text='Ya tienes vinculado un grupo de este equipo'
                        , parse_mode=telegram.ParseMode.MARKDOWN)
        return

    if args is None or len(args) != 1 or args[0] != '-' \
        and (len(args[0]) < 3 or len(args[0]) > 60
             or re.match("@?[a-zA-Z]([a-zA-Z0-9_]+)$|https://t\.me/joinchat/[a-zA-Z0-9_-]+$"
             , args[0]) is None):
        bot.sendMessage(chat_id=chat_id,
                        text='\xe2\x9d\x8c Necesito que me pases el alias del grupo o un enlace de `t.me` de un grupo privado. Por ejemplo: `@pkmngobadajoz` o `https://t.me/joinchat/XXXXERK2ZfB3ntXXSiWUx`.'
                        , parse_mode=telegram.ParseMode.MARKDOWN)
        return

    group_id = abs(random.getrandbits(60))

    if args[0] == '-':
        mock_team_link(chat_id, None, team, None)
        output = 'Grupo borrado correctamente'
    else:
        mock_team_link(chat_id, group_id, team, args[0])
        output = 'Grupo creado correctamente'

    bot.sendMessage(chat_id=chat_id,
                    text='\xe2\x9d\x8c Necesito que me pases el alias del grupo o un enlace de `t.me` de un grupo privado. Por ejemplo: `@pkmngobadajoz` o `https://t.me/joinchat/XXXXERK2ZfB3ntXXSiWUx`.'
                    , parse_mode=telegram.ParseMode.MARKDOWN)


@run_async
def settings_admin(bot, update):
    logging.debug('%s %s', bot, update)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)
    chat_title = message.chat.title

    delete_message(chat_id, message.message_id, bot)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    group = get_admin(chat_id)
    if group is None:
        bot.sendMessage(chat_id=chat_id,
                        text='No he reconocido este grupo como un grupo de administracion'
                        , parse_mode=telegram.ParseMode.MARKDOWN)
        return
    message = bot.sendMessage(chat_id=chat_id, text="Oído cocina!...")
    update_settings_message(chat_id, bot, message.message_id,
                            keyboard='admin')


@run_async
def create_adm(bot, update):
    logging.debug('%s %s', bot, update)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    if get_admin(chat_id) is None:
        set_admin(chat_id)
        output = \
            'Grupo configuado exitosamente. Continua vinculando grupos con este ID: `{}`\nNo olvides activar las alertas con `/settings_admin`'.format(chat_id)
    elif get_admin(chat_id) is not None:

        output = \
            "Ya está configurado este grupo como administrativo\nAquí tienes tu ID: `{}`".format(chat_id)
    else:

        output = \
            "Este grupo no puede ser un grupo administrativo ya que está vinculado a otro grupo."

    bot.sendMessage(chat_id=chat_id, text=output,
                    parse_mode=telegram.ParseMode.MARKDOWN)
    return


@run_async
def rm_adm(bot, update):
    logging.debug('%s %s', bot, update)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    if get_admin(chat_id) is not None:
        rm_admin(chat_id)
    else:
        return

    bot.sendMessage(chat_id=chat_id,
                    text='Todos los grupos desvinculados',
                    parse_mode=telegram.ParseMode.MARKDOWN)
    return


@run_async
def groups(bot, update):
    logging.debug('%s %s', bot, update)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)
    delete_message(chat_id, message.message_id, bot)

    if are_banned(user_id, chat_id):
        return

    groups = get_all_groups(chat_id)
    output = '*Listado de grupos vinculados:*'

    if groups is None:
        output = 'No he encontrado grupos vinculados a este'
    elif is_admin(chat_id, user_id, bot):

        for k in groups:
            if k.link is not None \
                and re.match('@?[a-zA-Z]([a-zA-Z0-9_]+)$', k.link) \
                is None:
                output = output + "\n🏥 [{}]({}) - `{}`".format(k.label
                        or k.title, k.link, k.linked_id)
            elif k.link is not None \
                and re.match('@?[a-zA-Z]([a-zA-Z0-9_]+)$', k.link) \
                is not None:
                output = output + "\n🏥 {} - {} - `{}`".format(k.label
                        or k.title, k.link, k.linked_id)
            else:
                output = output + "\n🏥 {} - `{}`".format(k.label
                        or k.title, k.linked_id)
    else:
        for k in groups:
            if k.group_type not in [GroupType.RED, GroupType.BLUE,
                                    GroupType.YELLOW]:
                if k.link is not None \
                    and re.match('@?[a-zA-Z]([a-zA-Z0-9_]+)$', k.link) \
                    is None:
                    output = output + "\n🏥 [{}]({})".format(k.label
                            or k.title, k.link)
                elif k.link is not None \
                    and re.match('@?[a-zA-Z]([a-zA-Z0-9_]+)$', k.link) \
                    is not None:
                    output = output + "\n🏥 {} - {}".format(k.label
                            or k.title, k.link)
                else:
                    output = output + "\n🏥 {}".format(k.label
                            or k.title)

    if chat_type != "private":
        group = get_group_settings(chat_id)
        if group.reply_on_group:
            dest_id = chat_id
        else:
            dest_id = user_id
    else:
        dest_id = user_id

    bot.sendMessage(chat_id=user_id, text=output,
                    parse_mode=telegram.ParseMode.MARKDOWN,
                    disable_web_page_preview=True)
    return


@run_async
def add_url(bot, update, args=None):
    logging.debug('%s %s %s', bot, update, args)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)
    chat_title = message.chat.title
    group_alias = None

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    if args is None or len(args) != 1 or args[0] != '-' \
        and (len(args[0]) < 3 or len(args[0]) > 60
             or re.match("@?[a-zA-Z]([a-zA-Z0-9_]+)$|https://t\.me/joinchat/[a-zA-Z0-9_-]+$"
             , args[0]) is None):
        bot.sendMessage(chat_id=chat_id,
                        text='\xe2\x9d\x8c Necesito que me pases el alias del grupo o un enlace de `t.me` de un grupo privado. Por ejemplo: `@pkmngobadajoz` o `https://t.me/joinchat/XXXXERK2ZfB3ntXXSiWUx`.'
                        , parse_mode=telegram.ParseMode.MARKDOWN)

    if args[0] != '-':
        enlace = args[0]
        add_tlink(chat_id, enlace)
        if re.match('@?[a-zA-Z]([a-zA-Z0-9_]+)$', args[0]) is not None:
            bot.sendMessage(chat_id=chat_id,
                            text="👌 Establecido el link del grupo a {}.".format(ensure_escaped(enlace)),
                            parse_mode=telegram.ParseMode.MARKDOWN)
        else:

            bot.sendMessage(chat_id=chat_id,
                            text="👌 Establecido el link del grupo a {}.".format(ensure_escaped(enlace)),
                            parse_mode=telegram.ParseMode.MARKDOWN)
    else:
        add_tlink(chat_id, None)
        bot.sendMessage(chat_id=chat_id,
                        text="👌 Esto... Creo que acabo de olvidar el enlace del grupo..."
                        , parse_mode=telegram.ParseMode.MARKDOWN)


@run_async
def link(bot, update, args=None):
    logging.debug('%s %s %s', bot, update, args)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)
    chat_title = message.chat.title

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    if args is None or len(args) != 1 or get_admin(args[0]) is None:
        output = 'Parece ser que has introducido un ID incorrecto'
        bot.sendMessage(chat_id=chat_id, text=output,
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return

    new_link(args[0], chat_id, chat_title)
    output = \
        'El grupo ha sido registrado, continua el proceso desde el grupo de administracion'
    bot.sendMessage(chat_id=chat_id, text=output,
                    parse_mode=telegram.ParseMode.MARKDOWN)
    button_list = [[InlineKeyboardButton(text="💥 Raids",
                   callback_data='type_raids_{}'.format(chat_id)),
                   InlineKeyboardButton(text="🔁 Intercambios",
                   callback_data='type_trade_{}'.format(chat_id)),
                   InlineKeyboardButton(text="👫 Amigos",
                   callback_data='type_friends_{}'.format(chat_id))],
                   [InlineKeyboardButton(text="🌟 EX",
                   callback_data='type_ex_{}'.format(chat_id)),
                   InlineKeyboardButton(text="👀 Alertas",
                   callback_data='type_alerts_{}'.format(chat_id)),
                   InlineKeyboardButton(text="🔍 Misiones",
                   callback_data='type_quests_{}'.format(chat_id))],
                   [InlineKeyboardButton(text="❤️ Valor",
                   callback_data='type_r_{}'.format(chat_id)),
                   InlineKeyboardButton(text="💛 Instinto",
                   callback_data='type_y_{}'.format(chat_id)),
                   InlineKeyboardButton(text="💙 Sabiduria",
                   callback_data='type_b_{}'.format(chat_id))],
                   [InlineKeyboardButton(text="🗣 Charla",
                   callback_data='type_talk_{}'.format(chat_id)),
                   InlineKeyboardButton(text="👾 Otros",
                   callback_data='type_others_{}'.format(chat_id)),
                   InlineKeyboardButton(text="❌ Cancelar",
                   callback_data='type_cancel_{}'.format(chat_id))]]
    reply_markup = InlineKeyboardMarkup(button_list)
    output = \
        'Continua con el registro del grupo {}'.format(ensure_escaped(chat_title))
    bot.send_message(chat_id=args[0], text=output,
                     parse_mode=telegram.ParseMode.MARKDOWN,
                     reply_markup=reply_markup)


@run_async
def tag(bot, update, args=None):
    logging.debug('%s %s %s', bot, update, args)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    if get_groups(chat_id) is not None:
        message = ' '.join(args)
    else:
        bot.sendMessage(chat_id=chat_id,
                        text='Este grupo no esta vinculado',
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return

    if len(message) < 51:
        output = 'Listo'
        if message is '-':
            message = None
        set_tag(chat_id, message)
    else:
        output = 'Este mensaje es demasiado largo'

    bot.sendMessage(chat_id=chat_id, text=output,
                    parse_mode=telegram.ParseMode.MARKDOWN)


@run_async
def unlink(bot, update, args=None):
    logging.debug('%s %s %s', bot, update, args)
    (chat_id, chat_type, user_id, text, message) = \
        extract_update_info(update)
    chat_title = message.chat.title

    if not is_admin(chat_id, user_id, bot) or are_banned(user_id,
            chat_id):
        return

    rm_link(chat_id)
    output = 'El grupo ha sido desvinculado'
    bot.sendMessage(chat_id=chat_id, text=output,
                    parse_mode=telegram.ParseMode.MARKDOWN)

# ---------END SETTINGS---------#
